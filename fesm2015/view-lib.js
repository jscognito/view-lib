import { Injectable, NgModule, Component, Input, Output, EventEmitter, ContentChildren, defineInjectable } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ViewLibService {
    constructor() { }
}
ViewLibService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
ViewLibService.ctorParameters = () => [];
/** @nocollapse */ ViewLibService.ngInjectableDef = defineInjectable({ factory: function ViewLibService_Factory() { return new ViewLibService(); }, token: ViewLibService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BannerComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
BannerComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-banner',
                template: `<div class="banner">
  <ng-content></ng-content>
</div>
`,
                styles: [`.banner{width:auto;height:auto;border-radius:6px;background-color:#fff;border:1.5px solid #ffd200;padding:15px;overflow:hidden}`]
            },] },
];
/** @nocollapse */
BannerComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ButtonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-button',
                template: `<button class="btn {{ type }}">
  {{ label }}
</button>
`,
                styles: [`.btn{width:144px;height:40px;border-radius:20px;font-size:14px;font-weight:700;border:0;text-transform:uppercase}.btn.primary-gray{background-color:#58595b;color:#fff}.btn.primary-gray:hover{background-color:#363636}.btn.primary-gray:focus{background-color:#58595b}.btn.primary-yellow{background-color:#ffd200;color:#454648}.btn.primary-yellow:hover{background-color:#ffb500}.btn.primary-yellow:focus{background-color:#ffd200}.btn.primary-red{background-color:#fa5e5b;color:#fff}.btn.primary-red:hover{background-color:#d14d4a}.btn.primary-red:focus{background-color:#fa5e5b}.btn.white{background-color:#fff;color:#454648}.btn.white:hover{background-color:transparent;border:1px solid #fff}.btn.white:focus{background-color:#fff;color:#454648}.btn.secondary{border:1px solid #00448d;background-color:#fff;color:#00448d}.btn.secondary:hover{border-width:2px}.btn.secondary:focus{background-color:#e6e7e8}.btn.flat{border-radius:0;background-color:transparent;color:#333}.btn.flat:hover{color:#ffd200}.btn.flat:focus{color:#58595b}`]
            },] },
];
/** @nocollapse */
ButtonComponent.ctorParameters = () => [];
ButtonComponent.propDecorators = {
    type: [{ type: Input }],
    label: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CardComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CardComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-card',
                template: `<div class="card">
  <ng-content></ng-content>
</div>
`,
                styles: [`.card{background-color:#fff;border-radius:8px;border:1px solid #f1f1f1;box-shadow:0 2px 6px 0 rgba(0,0,0,.07);width:100%;height:100%;overflow:hidden}`]
            },] },
];
/** @nocollapse */
CardComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CarouselBannerComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CarouselBannerComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-carousel-banner',
                template: `<div class="main-container">
  <div class="carousel-banner">
    <div class="banner-container">
      <a href=""><img class="img-fluid" src="../../../assets/images/bitmap.png"></a>
    </div>
  </div>
</div>
`,
                styles: [`.main-container{width:100%}.main-container .carousel-banner{background-color:#e6e7e8;height:auto}.main-container .carousel-banner .banner-container{max-width:942px;margin:auto;text-align:right}@media (max-width:768px){.carousel-banner{background-color:transparent!important}.carousel-banner .banner-container{max-width:100%;padding:0 26px!important;text-align:center!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.banner-container{padding:0 15px}}`]
            },] },
];
/** @nocollapse */
CarouselBannerComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class FooterComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FooterComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-footer',
                template: `<footer>
  <div class="contact-information text-center">
    <div>
      <i class="footer-icon fal fa-phone"></i>
      Sucursal Telefónica : 507-306-4700
    </div>
    <div>Copyright Banistmo SA. 2018</div>
  </div>
</footer>



`,
                styles: [`footer{width:100%;background-color:#e6e7e8;padding:25px 0}footer .contact-information{font-size:.8em}footer .contact-information i{-webkit-transform:scaleX(-1);transform:scaleX(-1)}`]
            },] },
];
/** @nocollapse */
FooterComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class FooterExternalComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FooterExternalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-footer-external',
                template: `<footer>
  <div class="container">
    <div class="row justify-content-md-center text-center">
      <div class="col col-lg-2">
        <a href="" id="info_schedules">Horarios</a>
      </div>
      <div class="col col-lg-2">
        <a href="" id="info_commissions">Comisiones</a>
      </div>
      <div class="col col-lg-2">
        <a href="" id="info_security">Seguridad</a>
      </div>
    </div>
  </div>
</footer>
`,
                styles: [`footer{width:100%;background-color:#e6e7e8;padding:25px 0 0}footer a{color:#454648;font-size:.8em;line-height:34px}footer a:hover{font-weight:700;text-decoration:none}`]
            },] },
];
/** @nocollapse */
FooterExternalComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class HyperlinkComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
HyperlinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-hyperlink',
                template: `<a href="{{ url }}">
  {{ label }}
</a>
`,
                styles: [`a{font-size:.85em;color:#00448d;text-decoration:underline}a:hover{font-weight:700}`]
            },] },
];
/** @nocollapse */
HyperlinkComponent.ctorParameters = () => [];
HyperlinkComponent.propDecorators = {
    label: [{ type: Input }],
    url: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LoadingComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
LoadingComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-loading',
                template: `<div class="blocking-background"></div>
<div class="container-loader" id="{{ id }}">
  <div class="loader">
    <div class="spinner">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
</div>
`,
                styles: [`.blocking-background{background-color:rgba(255,255,255,.4);position:fixed;top:0;left:0;width:100%;height:100%;z-index:400}.container-loader{background-color:rgba(255,255,255,.7);position:absolute;top:0;left:0;width:100%;height:100%;z-index:400;border-radius:8px}.container-loader .loader{display:block;position:relative;left:50%;top:50%;z-index:500}.container-loader[loaded] .spinner{-webkit-animation:2s ease-in fadeout;animation:2s ease-in fadeout;opacity:0}.container-loader .spinner{position:relative;-webkit-animation:2s ease-in fadein;animation:2s ease-in fadein}.container-loader .spinner div{position:absolute;width:20px;height:20px;border-radius:50%;opacity:.7;-webkit-transform:translateX(-30px);transform:translateX(-30px)}.container-loader .spinner div:nth-child(1){background-color:#ffd200;-webkit-animation:1.8s ease-in-out 1.2s infinite spin;animation:1.8s ease-in-out 1.2s infinite spin;height:24px;width:24px}.container-loader .spinner div:nth-child(2){background-color:#58595b;-webkit-animation:1.8s ease-in-out .6s infinite spin;animation:1.8s ease-in-out .6s infinite spin;height:20px;weight:20px}.container-loader .spinner div:nth-child(3){background-color:#00448d;-webkit-animation:1.8s ease-in-out infinite spin;animation:1.8s ease-in-out infinite spin;width:10px;height:10px}@-webkit-keyframes spin{0%{-webkit-transform:translateX(-30px);transform:translateX(-30px)}33.3%{-webkit-transform:translateX(30px);transform:translateX(30px)}66.7%{-webkit-transform:translateX(0) translateY(-45px);transform:translateX(0) translateY(-45px)}100%{-webkit-transform:translateX(-30px) translateY(0);transform:translateX(-30px) translateY(0)}}@keyframes spin{0%{-webkit-transform:translateX(-30px);transform:translateX(-30px)}33.3%{-webkit-transform:translateX(30px);transform:translateX(30px)}66.7%{-webkit-transform:translateX(0) translateY(-45px);transform:translateX(0) translateY(-45px)}100%{-webkit-transform:translateX(-30px) translateY(0);transform:translateX(-30px) translateY(0)}}@-webkit-keyframes fadein{from{opacity:0}to{opacity:1}}@keyframes fadein{from{opacity:0}to{opacity:1}}@-webkit-keyframes fadeout{from{opacity:1}to{opacity:0}}@keyframes fadeout{from{opacity:1}to{opacity:0}}`]
            },] },
];
/** @nocollapse */
LoadingComponent.ctorParameters = () => [];
LoadingComponent.propDecorators = {
    id: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ModalComponent {
    /**
     * @return {?}
     */
    openModal() {
        this.display = 'block';
        document.body.style.overflow = 'hidden';
    }
    /**
     * @return {?}
     */
    closeModal() {
        this.display = 'none';
        document.body.style.overflow = 'auto';
    }
}
ModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-modal',
                template: `<div class="modal-container" [style.display]=display id="{{ id }}">
  <div class="centered">
    <div class="modal-content">
      <div class="row modal-header align-items-center">
        <div class="col-12 text-center">
          <h2>{{ titleModal }}</h2>
        </div>
        <i routerLink="/login" class="fal fa-times close" (click)="closeModal()" id="btn_close"></i>
      </div>
      <div class="modal-body text-center">
        <i class="icon-exit"></i>
        <p class="session-message">{{ messageContent }}</p>
        <div class="row justify-content-center actions">
          <div class="col-5">
            <app-button id="btn_accept" (click)="closeModal()" routerLink="/login" type="primary-gray" label="{{ labelActionBtn }}"></app-button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
`,
                styles: [`body{overflow:hidden}.modal-container{display:none;position:fixed;z-index:100;left:0;top:0;width:100%;height:100%;-webkit-backdrop-filter:blur(3px);backdrop-filter:blur(3px);background-color:rgba(247,247,247,.8)}.modal-container .centered{display:table;margin:12% auto}.modal-content{min-width:500px;border-radius:8px;box-shadow:0 2px 6px 0 rgba(0,0,0,.07);background-color:#fff;border:1px solid #f1f1f1;align-items:center}.modal-content .modal-header{width:100%;padding:15px;border-bottom:1px solid rgba(151,151,151,.15);color:#454648;margin:0;position:relative}.modal-content .modal-header .close{position:absolute;right:15px;cursor:pointer;font-size:1.5em;color:#58595b;opacity:.5}.modal-content .modal-header h2{font-size:1.12em;font-weight:700;margin-bottom:0}.modal-content .modal-body{padding:30px 0}.modal-content .modal-body .icon-exit{padding-bottom:30px;content:url(../../../assets/icons/exit.svg)}.modal-content .modal-body .session-message{padding-bottom:30px}@media (max-width:576px){.icon{position:absolute;right:10px;top:-32px}.modal-content{min-width:320px!important}.centered{margin:35% auto}}`]
            },] },
];
ModalComponent.propDecorators = {
    id: [{ type: Input }],
    titleModal: [{ type: Input }],
    messageContent: [{ type: Input }],
    labelActionBtn: [{ type: Input }],
    iconModalBtn: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NavbarComponent {
    constructor() {
        this.date = new Date();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
NavbarComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-navbar',
                template: `<div class="navbar">
  <nav class="main-navbar">
    <div class="nav-container">
      <div class="row">
        <div class="col-sm-6">
          <img src="../../../assets/images/logoBanistmo.svg">
        </div>
        <div class="col-sm-6">
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  </nav>
  <nav class="secondary-navbar">
    <div class="nav-container">
      <div class="row">
        <div class="col-sm-6 welcome-text">
          Bienvenido a la Sucursal Virtual Personas
        </div>
        <div class="col-sm-6 date">
          
        </div>
      </div>
    </div>
  </nav>
</div>
`,
                styles: [`.navbar{width:100%}.navbar .main-navbar{padding:13px 0;background-color:#fff}.navbar .secondary-navbar{padding:4px 0;background-color:#ffd200;min-height:30px;line-height:25px}.navbar .secondary-navbar .welcome-text{font-weight:700;font-size:.88em}.navbar .secondary-navbar .date{font-size:12px;text-align:right}.navbar .nav-container{max-width:942px;margin:auto}.navbar img{height:34px}@media (max-width:576px){.date,.main-navbar,.secondary-navbar{text-align:center!important}}@media (max-width:768px){.nav-container{max-width:100%;padding:0 26px!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.nav-container{padding:0 15px}}`]
            },] },
];
/** @nocollapse */
NavbarComponent.ctorParameters = () => [];
NavbarComponent.propDecorators = {
    content: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NotificationComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
NotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-notification',
                template: `<div class="notification {{ type }}">
  <div class="icon">
    <i class="{{ icon }}"></i>
  </div>
  <div class="content">
    {{ message }}
  </div>
</div>
`,
                styles: [`.notification{width:auto;height:auto;border-radius:8px;padding:15px;margin-bottom:15px}.notification .icon{float:left;margin-right:15px;font-size:18px}.notification .content{text-align:center}.notification.error{background-color:#fa5e5b;color:#fff}.notification.info{background-color:#00448d;color:#fff}.notification.success{background-color:#16c98d;color:#fff}.notification.warning{background-color:#ffd200;color:#454648}`]
            },] },
];
/** @nocollapse */
NotificationComponent.ctorParameters = () => [];
NotificationComponent.propDecorators = {
    type: [{ type: Input }],
    icon: [{ type: Input }],
    message: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class OutlineButtonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
OutlineButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-outline-button',
                template: `<button class="btn-outline" id="{{ id }}">
  <i class="icon-left {{ icon }}"></i>
  <ng-content></ng-content>
</button>
`,
                styles: [`.btn-outline{width:287px;height:59px;border-radius:29.5px;border:1px solid #3a3b3b;background-color:transparent;font-size:.85em;font-weight:700;padding:0 15px;margin-top:10px;cursor:pointer}.btn-outline:hover{border:2px solid #3a3b3b}.btn-outline .icon-left{font-size:1.8em;width:30px;vertical-align:bottom;line-height:10px}`]
            },] },
];
/** @nocollapse */
OutlineButtonComponent.ctorParameters = () => [];
OutlineButtonComponent.propDecorators = {
    id: [{ type: Input }],
    icon: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class PasswordFieldComponent {
    constructor() {
        this.valuePassword = new EventEmitter();
        this.class = '';
    }
    /**
     * @return {?}
     */
    getValuePassword() {
        this.valuePassword.emit(this.valueInputPassword);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.type = 'password';
    }
    /**
     * @return {?}
     */
    validateField() {
        this.getValuePassword();
        (this.valueInputPassword !== null && this.valueInputPassword !== '') ? this.emptyInput = 'status' : this.emptyInput = '';
    }
    /**
     * @return {?}
     */
    clearField() {
        this.valueInputPassword = '';
        this.state = '';
        this.message = '';
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onSetStateChange(event) {
        this.state = event.state;
        if (event.state !== '') {
            this.message = 'No cumple con la longitud mínima';
        }
        else {
            this.message = '';
        }
    }
}
PasswordFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-password-field',
                template: `<div class="text-field {{ state }}">
  <div class="content">
    <label>
      <input type="{{ type }}" id="{{ id }}" class="{{ emptyInput }}" [(ngModel)]="valueInputPassword" name="text-field" required (change)="validateField()"
        autocomplete="off" onpaste="return false" onCopy="return false" input-directive [minLength]=8 [maxLength]=16 [pattern]="pattern"
        (setState)="onSetStateChange($event)">
      <span>{{ label }}</span>
      <div class="icon">
        <i [class]="'fal fa-eye' + class" (mousedown)="class='-slash';type='text'" (mouseup)="class='';type='password'" ></i>
        <i *ngIf="state !== 'error'" class="fal fa-info-circle"></i>
        <i *ngIf="state === 'error'" class="fal fa-times error" (click)="clearField()"></i>
      </div>
    </label>
  </div>
  <div class="message">
    <p>{{ message }}</p>
  </div>
</div>
`,
                styles: [`.text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon .error,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:8px;cursor:pointer}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}`]
            },] },
];
/** @nocollapse */
PasswordFieldComponent.ctorParameters = () => [];
PasswordFieldComponent.propDecorators = {
    valuePassword: [{ type: Output }],
    label: [{ type: Input }],
    state: [{ type: Input }],
    message: [{ type: Input }],
    type: [{ type: Input }],
    pattern: [{ type: Input }],
    icon: [{ type: Input }],
    id: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SecondaryLinkComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SecondaryLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-secondary-link',
                template: `<div class="secondary-link">
  <i class="icon-left {{ icon }}">
  </i>
  <a href="">{{ labelLink }}</a>
  <i class="fas fa-plus icon-right">
  </i>
</div>
`,
                styles: [`.secondary-link{margin-bottom:30px}.secondary-link .icon-left{font-size:2em;float:left;margin-right:20px;width:35px;text-align:center}.secondary-link a{color:#3a3b3b;font-size:.9em;font-weight:700;line-height:32px}.secondary-link a:hover{color:#ffd200;font-weight:700;text-decoration:none}.secondary-link a:focus{color:#3a3b3b}.secondary-link .icon-right{font-weight:700;font-size:.55em;float:right;line-height:32px}`]
            },] },
];
/** @nocollapse */
SecondaryLinkComponent.ctorParameters = () => [];
SecondaryLinkComponent.propDecorators = {
    labelLink: [{ type: Input }],
    icon: [{ type: Input }],
    id: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SelectComponent {
    constructor() {
        this.valueSelect = 0;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    validateField() {
        (this.valueSelect !== 0) ? this.emptySelect = 'status' : this.emptySelect = '';
    }
}
SelectComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-select',
                template: `<div class="text-field {{ state }}">
  <div class="content">
    <label>
      <select class="{{ emptySelect }}" [(ngModel)]="valueSelect" name="select" required
              (change)="validateField()" [disabled]="(state == 'disabled')">

        <ng-content></ng-content>
      </select>
      <span>Seleccione</span>
      <div class="icon">
        <i class="{{ icon }}"></i>
      </div>
    </label>
  </div>
  <div class="message">
    <p>{{ message }}</p>
  </div>
</div>
`,
                styles: [`.text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content select{position:absolute;top:15px;z-index:2;width:100%;font-size:16px;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px;-moz-appearance:none;-webkit-appearance:none;appearance:none}.text-field .content select:focus{border-bottom:2px solid #ffd200}.text-field .content select:focus+span,.text-field .content select:valid+span{top:6px;cursor:inherit;font-size:13px;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:1;font-size:16px;transition:all .2s ease-in-out;width:100%;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{font-size:18px;position:absolute;z-index:3;right:15px;top:15px}.text-field .message{min-height:20px}.text-field .message p{padding:4px 0 0 15px;font-size:12px;color:#808285;margin:0}`]
            },] },
];
/** @nocollapse */
SelectComponent.ctorParameters = () => [];
SelectComponent.propDecorators = {
    label: [{ type: Input }],
    icon: [{ type: Input }],
    type: [{ type: Input }],
    state: [{ type: Input }],
    message: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class SubtitleComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SubtitleComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-subtitle',
                template: `<h2>
  <ng-content></ng-content>
</h2>
`,
                styles: [`h2{font-size:17px;font-weight:700;line-height:1.35}`]
            },] },
];
/** @nocollapse */
SubtitleComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabComponent {
    constructor() {
        this.active = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
TabComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-tab',
                template: `<div [hidden]="!active" class="pane">
  <ng-content></ng-content>
</div>`,
                styles: [`.pane{padding:30px 20px;justify-content:center}`]
            },] },
];
/** @nocollapse */
TabComponent.ctorParameters = () => [];
TabComponent.propDecorators = {
    title: [{ type: Input }],
    id: [{ type: Input }],
    active: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TabsComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        /** @type {?} */
        const activeTabs = this.tabs.filter((tab) => tab.active);
        // if there is no active tab set, activate the first
        if (activeTabs.length === 0) {
            this.selectTab(this.tabs.first);
        }
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    selectTab(tab) {
        // deactivate all tabs
        this.tabs.toArray().forEach(tab => tab.active = false);
        // activate the tab the user has clicked on.
        tab.active = true;
    }
}
TabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-tabs',
                template: `<div class="nav nav-tabs">
  <span *ngFor="let tab of tabs" id="{{tab.id}}" (click)="selectTab(tab)" [class.active]="tab.active" class="tab"
        ngClass="{{position}}">
    <span role="button" class="tab-title">{{tab.title}}</span>
  </span>
</div>
<ng-content></ng-content>
`,
                styles: [`.nav-tabs{display:flex;flex-wrap:nowrap;font-size:1em;font-weight:700;text-transform:uppercase;border-bottom:1px solid #e6e7e8}.nav-tabs .tab{width:50%;padding:18px 10px 10px;text-align:center}.nav-tabs .center{flex:1 1;display:flex}.nav-tabs .tab-title{padding:0 7px 5px;cursor:pointer}.nav-tabs .active{border-bottom:solid #ffd200}`]
            },] },
];
/** @nocollapse */
TabsComponent.ctorParameters = () => [];
TabsComponent.propDecorators = {
    position: [{ type: Input }],
    tabs: [{ type: ContentChildren, args: [TabComponent,] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TextFieldComponent {
    constructor() {
        this.valueUsername = new EventEmitter();
    }
    /**
     * @return {?}
     */
    getValueUsername() {
        this.valueUsername.emit({
            value: this.valueInput,
            state: this.inputStyle
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    validateField() {
        if (this.valueInput !== null && this.valueInput !== '') {
            this.inputStyle = 'status';
        }
        else {
            this.inputStyle = '';
        }
        this.getValueUsername();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onSetStateChange(event) {
        this.state = event.state;
        this.inputStyle = event.state;
        if (event.state !== '') {
            this.message = 'No cumple con la longitud mínima';
            this.iconSecondary = 'fal fa-times';
        }
        else {
            this.message = '';
            this.iconSecondary = '';
        }
        if (this.valueInput === '') {
            this.message = '';
            this.state = '';
            this.iconSecondary = '';
        }
        this.getValueUsername();
    }
}
TextFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-text-field',
                template: `<div class="text-field {{ state }}">
  <div class="content">
    <label>
      <input type="text" id="{{ id }}" class="{{ inputStyle }}" [(ngModel)]="valueInput" name="text-field" [required]="true"
      input-directive [minLength]=8 [maxLength]=16 [pattern]="pattern" (setState)="onSetStateChange($event)" (keyup)="validateField()" [disabled]="(state == 'disabled')">
      <span>{{ label }}</span>
      <div class="icon">
        <i class="{{ iconPrimary }}"></i>
        <i class="{{ iconSecondary }}"></i>
      </div>
    </label>
  </div>
  <div class="message">
    <p>{{ message }}</p>
  </div>
</div>
`,
                styles: [`.text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:15px;cursor:pointer}.text-field .content .icon i:nth-child(2){color:#fa5e5b}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}`],
            },] },
];
/** @nocollapse */
TextFieldComponent.ctorParameters = () => [];
TextFieldComponent.propDecorators = {
    valueUsername: [{ type: Output }],
    label: [{ type: Input }],
    iconPrimary: [{ type: Input }],
    iconSecondary: [{ type: Input }],
    state: [{ type: Input }],
    message: [{ type: Input }],
    pattern: [{ type: Input }],
    id: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TitleComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
TitleComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-title',
                template: `<h1>
  <ng-content></ng-content>
</h1>
`,
                styles: [`h1{font-size:24px;font-weight:700;line-height:1.17}`]
            },] },
];
/** @nocollapse */
TitleComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class TooltipComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
TooltipComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-tooltip',
                template: `<a  class="tooltip" title="{{ title}}">
  <i class="{{ icon }}"></i>
</a>`,
                styles: [`.tooltip{display:inline;position:relative}.tooltip:hover:after{background:rgba(0,0,0,.8);border-radius:5px;bottom:26px;color:#fff;content:attr(title);left:-600%;padding:5px 15px;position:absolute;z-index:98;width:220px}.tooltip:hover:before{border:solid;border-color:#333 transparent;border-width:6px 6px 0;bottom:20px;content:"";left:0;position:absolute;z-index:99}.tooltip i{font-size:1.2em;letter-spacing:15px;cursor:pointer}`]
            },] },
];
/** @nocollapse */
TooltipComponent.ctorParameters = () => [];
TooltipComponent.propDecorators = {
    title: [{ type: Input }],
    icon: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ViewLibModule {
}
ViewLibModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule
                ],
                declarations: [
                    BannerComponent,
                    ButtonComponent,
                    CardComponent,
                    CarouselBannerComponent,
                    FooterComponent,
                    FooterExternalComponent,
                    HyperlinkComponent,
                    LoadingComponent,
                    ModalComponent,
                    NavbarComponent,
                    NotificationComponent,
                    OutlineButtonComponent,
                    PasswordFieldComponent,
                    SecondaryLinkComponent,
                    SelectComponent,
                    SubtitleComponent,
                    TabComponent,
                    TabsComponent,
                    TextFieldComponent,
                    TitleComponent,
                    TooltipComponent
                ],
                exports: [
                    BannerComponent,
                    ButtonComponent,
                    CardComponent,
                    CarouselBannerComponent,
                    FooterComponent,
                    FooterExternalComponent,
                    HyperlinkComponent,
                    LoadingComponent,
                    ModalComponent,
                    NavbarComponent,
                    NotificationComponent,
                    OutlineButtonComponent,
                    PasswordFieldComponent,
                    SecondaryLinkComponent,
                    SelectComponent,
                    SubtitleComponent,
                    TabComponent,
                    TabsComponent,
                    TextFieldComponent,
                    TitleComponent,
                    TooltipComponent
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { BannerComponent, ButtonComponent, CardComponent, CarouselBannerComponent, FooterComponent, FooterExternalComponent, HyperlinkComponent, LoadingComponent, ModalComponent, NavbarComponent, NotificationComponent, OutlineButtonComponent, PasswordFieldComponent, SecondaryLinkComponent, SelectComponent, SubtitleComponent, TabComponent, TabsComponent, TextFieldComponent, TitleComponent, TooltipComponent, ViewLibService, ViewLibModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1saWIuanMubWFwIiwic291cmNlcyI6WyJuZzovL3ZpZXctbGliL2xpYi92aWV3LWxpYi5zZXJ2aWNlLnRzIiwibmc6Ly92aWV3LWxpYi9saWIvYmFubmVyL2Jhbm5lci5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9idXR0b24vYnV0dG9uLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL2NhcmQvY2FyZC5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9jYXJvdXNlbC1iYW5uZXIvY2Fyb3VzZWwtYmFubmVyLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvZm9vdGVyLWV4dGVybmFsL2Zvb3Rlci1leHRlcm5hbC5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9oeXBlcmxpbmsvaHlwZXJsaW5rLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL2xvYWRpbmcvbG9hZGluZy5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9tb2RhbC9tb2RhbC5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvb3V0bGluZS1idXR0b24vb3V0bGluZS1idXR0b24uY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvcGFzc3dvcmQtZmllbGQvcGFzc3dvcmQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvc2Vjb25kYXJ5LWxpbmsvc2Vjb25kYXJ5LWxpbmsuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvc2VsZWN0L3NlbGVjdC5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9zdWJ0aXRsZS9zdWJ0aXRsZS5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi90YWIvdGFiLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3RhYnMvdGFicy5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi90ZXh0LWZpZWxkL3RleHQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvdGl0bGUvdGl0bGUuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvdG9vbHRpcC90b29sdGlwLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3ZpZXctbGliLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWaWV3TGliU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWJhbm5lcicsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwiYmFubmVyXCI+XHJcbiAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLmJhbm5lcnt3aWR0aDphdXRvO2hlaWdodDphdXRvO2JvcmRlci1yYWRpdXM6NnB4O2JhY2tncm91bmQtY29sb3I6I2ZmZjtib3JkZXI6MS41cHggc29saWQgI2ZmZDIwMDtwYWRkaW5nOjE1cHg7b3ZlcmZsb3c6aGlkZGVufWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCYW5uZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtYnV0dG9uJyxcclxuICB0ZW1wbGF0ZTogYDxidXR0b24gY2xhc3M9XCJidG4ge3sgdHlwZSB9fVwiPlxyXG4gIHt7IGxhYmVsIH19XHJcbjwvYnV0dG9uPlxyXG5gLFxyXG4gIHN0eWxlczogW2AuYnRue3dpZHRoOjE0NHB4O2hlaWdodDo0MHB4O2JvcmRlci1yYWRpdXM6MjBweDtmb250LXNpemU6MTRweDtmb250LXdlaWdodDo3MDA7Ym9yZGVyOjA7dGV4dC10cmFuc2Zvcm06dXBwZXJjYXNlfS5idG4ucHJpbWFyeS1ncmF5e2JhY2tncm91bmQtY29sb3I6IzU4NTk1Yjtjb2xvcjojZmZmfS5idG4ucHJpbWFyeS1ncmF5OmhvdmVye2JhY2tncm91bmQtY29sb3I6IzM2MzYzNn0uYnRuLnByaW1hcnktZ3JheTpmb2N1c3tiYWNrZ3JvdW5kLWNvbG9yOiM1ODU5NWJ9LmJ0bi5wcmltYXJ5LXllbGxvd3tiYWNrZ3JvdW5kLWNvbG9yOiNmZmQyMDA7Y29sb3I6IzQ1NDY0OH0uYnRuLnByaW1hcnkteWVsbG93OmhvdmVye2JhY2tncm91bmQtY29sb3I6I2ZmYjUwMH0uYnRuLnByaW1hcnkteWVsbG93OmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2ZmZDIwMH0uYnRuLnByaW1hcnktcmVke2JhY2tncm91bmQtY29sb3I6I2ZhNWU1Yjtjb2xvcjojZmZmfS5idG4ucHJpbWFyeS1yZWQ6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjojZDE0ZDRhfS5idG4ucHJpbWFyeS1yZWQ6Zm9jdXN7YmFja2dyb3VuZC1jb2xvcjojZmE1ZTVifS5idG4ud2hpdGV7YmFja2dyb3VuZC1jb2xvcjojZmZmO2NvbG9yOiM0NTQ2NDh9LmJ0bi53aGl0ZTpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2JvcmRlcjoxcHggc29saWQgI2ZmZn0uYnRuLndoaXRlOmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2ZmZjtjb2xvcjojNDU0NjQ4fS5idG4uc2Vjb25kYXJ5e2JvcmRlcjoxcHggc29saWQgIzAwNDQ4ZDtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Y29sb3I6IzAwNDQ4ZH0uYnRuLnNlY29uZGFyeTpob3Zlcntib3JkZXItd2lkdGg6MnB4fS5idG4uc2Vjb25kYXJ5OmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2U2ZTdlOH0uYnRuLmZsYXR7Ym9yZGVyLXJhZGl1czowO2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Y29sb3I6IzMzM30uYnRuLmZsYXQ6aG92ZXJ7Y29sb3I6I2ZmZDIwMH0uYnRuLmZsYXQ6Zm9jdXN7Y29sb3I6IzU4NTk1Yn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWNhcmQnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AuY2FyZHtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Ym9yZGVyLXJhZGl1czo4cHg7Ym9yZGVyOjFweCBzb2xpZCAjZjFmMWYxO2JveC1zaGFkb3c6MCAycHggNnB4IDAgcmdiYSgwLDAsMCwuMDcpO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7b3ZlcmZsb3c6aGlkZGVufWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtY2Fyb3VzZWwtYmFubmVyJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJtYWluLWNvbnRhaW5lclwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjYXJvdXNlbC1iYW5uZXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJiYW5uZXItY29udGFpbmVyXCI+XHJcbiAgICAgIDxhIGhyZWY9XCJcIj48aW1nIGNsYXNzPVwiaW1nLWZsdWlkXCIgc3JjPVwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9iaXRtYXAucG5nXCI+PC9hPlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AubWFpbi1jb250YWluZXJ7d2lkdGg6MTAwJX0ubWFpbi1jb250YWluZXIgLmNhcm91c2VsLWJhbm5lcntiYWNrZ3JvdW5kLWNvbG9yOiNlNmU3ZTg7aGVpZ2h0OmF1dG99Lm1haW4tY29udGFpbmVyIC5jYXJvdXNlbC1iYW5uZXIgLmJhbm5lci1jb250YWluZXJ7bWF4LXdpZHRoOjk0MnB4O21hcmdpbjphdXRvO3RleHQtYWxpZ246cmlnaHR9QG1lZGlhIChtYXgtd2lkdGg6NzY4cHgpey5jYXJvdXNlbC1iYW5uZXJ7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudCFpbXBvcnRhbnR9LmNhcm91c2VsLWJhbm5lciAuYmFubmVyLWNvbnRhaW5lcnttYXgtd2lkdGg6MTAwJTtwYWRkaW5nOjAgMjZweCFpbXBvcnRhbnQ7dGV4dC1hbGlnbjpjZW50ZXIhaW1wb3J0YW50O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveH19QG1lZGlhIChtYXgtd2lkdGg6OTkycHgpey5iYW5uZXItY29udGFpbmVye3BhZGRpbmc6MCAxNXB4fX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2Fyb3VzZWxCYW5uZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtZm9vdGVyJyxcclxuICB0ZW1wbGF0ZTogYDxmb290ZXI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRhY3QtaW5mb3JtYXRpb24gdGV4dC1jZW50ZXJcIj5cclxuICAgIDxkaXY+XHJcbiAgICAgIDxpIGNsYXNzPVwiZm9vdGVyLWljb24gZmFsIGZhLXBob25lXCI+PC9pPlxyXG4gICAgICBTdWN1cnNhbCBUZWxlZsODwrNuaWNhIDogNTA3LTMwNi00NzAwXHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXY+Q29weXJpZ2h0IEJhbmlzdG1vIFNBLiAyMDE4PC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvZm9vdGVyPlxyXG5cclxuXHJcblxyXG5gLFxyXG4gIHN0eWxlczogW2Bmb290ZXJ7d2lkdGg6MTAwJTtiYWNrZ3JvdW5kLWNvbG9yOiNlNmU3ZTg7cGFkZGluZzoyNXB4IDB9Zm9vdGVyIC5jb250YWN0LWluZm9ybWF0aW9ue2ZvbnQtc2l6ZTouOGVtfWZvb3RlciAuY29udGFjdC1pbmZvcm1hdGlvbiBpey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlWCgtMSk7dHJhbnNmb3JtOnNjYWxlWCgtMSl9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvb3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWZvb3Rlci1leHRlcm5hbCcsXHJcbiAgdGVtcGxhdGU6IGA8Zm9vdGVyPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWluZXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJyb3cganVzdGlmeS1jb250ZW50LW1kLWNlbnRlciB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sIGNvbC1sZy0yXCI+XHJcbiAgICAgICAgPGEgaHJlZj1cIlwiIGlkPVwiaW5mb19zY2hlZHVsZXNcIj5Ib3JhcmlvczwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wgY29sLWxnLTJcIj5cclxuICAgICAgICA8YSBocmVmPVwiXCIgaWQ9XCJpbmZvX2NvbW1pc3Npb25zXCI+Q29taXNpb25lczwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wgY29sLWxnLTJcIj5cclxuICAgICAgICA8YSBocmVmPVwiXCIgaWQ9XCJpbmZvX3NlY3VyaXR5XCI+U2VndXJpZGFkPC9hPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Zvb3Rlcj5cclxuYCxcclxuICBzdHlsZXM6IFtgZm9vdGVye3dpZHRoOjEwMCU7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4O3BhZGRpbmc6MjVweCAwIDB9Zm9vdGVyIGF7Y29sb3I6IzQ1NDY0ODtmb250LXNpemU6LjhlbTtsaW5lLWhlaWdodDozNHB4fWZvb3RlciBhOmhvdmVye2ZvbnQtd2VpZ2h0OjcwMDt0ZXh0LWRlY29yYXRpb246bm9uZX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9vdGVyRXh0ZXJuYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWh5cGVybGluaycsXHJcbiAgdGVtcGxhdGU6IGA8YSBocmVmPVwie3sgdXJsIH19XCI+XHJcbiAge3sgbGFiZWwgfX1cclxuPC9hPlxyXG5gLFxyXG4gIHN0eWxlczogW2Bhe2ZvbnQtc2l6ZTouODVlbTtjb2xvcjojMDA0NDhkO3RleHQtZGVjb3JhdGlvbjp1bmRlcmxpbmV9YTpob3Zlcntmb250LXdlaWdodDo3MDB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEh5cGVybGlua0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuICBASW5wdXQoKSB1cmw6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWxvYWRpbmcnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cImJsb2NraW5nLWJhY2tncm91bmRcIj48L2Rpdj5cclxuPGRpdiBjbGFzcz1cImNvbnRhaW5lci1sb2FkZXJcIiBpZD1cInt7IGlkIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImxvYWRlclwiPlxyXG4gICAgPGRpdiBjbGFzcz1cInNwaW5uZXJcIj5cclxuICAgICAgPGRpdj48L2Rpdj5cclxuICAgICAgPGRpdj48L2Rpdj5cclxuICAgICAgPGRpdj48L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLmJsb2NraW5nLWJhY2tncm91bmR7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDI1NSwyNTUsMjU1LC40KTtwb3NpdGlvbjpmaXhlZDt0b3A6MDtsZWZ0OjA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTt6LWluZGV4OjQwMH0uY29udGFpbmVyLWxvYWRlcntiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMjU1LDI1NSwyNTUsLjcpO3Bvc2l0aW9uOmFic29sdXRlO3RvcDowO2xlZnQ6MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO3otaW5kZXg6NDAwO2JvcmRlci1yYWRpdXM6OHB4fS5jb250YWluZXItbG9hZGVyIC5sb2FkZXJ7ZGlzcGxheTpibG9jaztwb3NpdGlvbjpyZWxhdGl2ZTtsZWZ0OjUwJTt0b3A6NTAlO3otaW5kZXg6NTAwfS5jb250YWluZXItbG9hZGVyW2xvYWRlZF0gLnNwaW5uZXJ7LXdlYmtpdC1hbmltYXRpb246MnMgZWFzZS1pbiBmYWRlb3V0O2FuaW1hdGlvbjoycyBlYXNlLWluIGZhZGVvdXQ7b3BhY2l0eTowfS5jb250YWluZXItbG9hZGVyIC5zcGlubmVye3Bvc2l0aW9uOnJlbGF0aXZlOy13ZWJraXQtYW5pbWF0aW9uOjJzIGVhc2UtaW4gZmFkZWluO2FuaW1hdGlvbjoycyBlYXNlLWluIGZhZGVpbn0uY29udGFpbmVyLWxvYWRlciAuc3Bpbm5lciBkaXZ7cG9zaXRpb246YWJzb2x1dGU7d2lkdGg6MjBweDtoZWlnaHQ6MjBweDtib3JkZXItcmFkaXVzOjUwJTtvcGFjaXR5Oi43Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KX0uY29udGFpbmVyLWxvYWRlciAuc3Bpbm5lciBkaXY6bnRoLWNoaWxkKDEpe2JhY2tncm91bmQtY29sb3I6I2ZmZDIwMDstd2Via2l0LWFuaW1hdGlvbjoxLjhzIGVhc2UtaW4tb3V0IDEuMnMgaW5maW5pdGUgc3BpbjthbmltYXRpb246MS44cyBlYXNlLWluLW91dCAxLjJzIGluZmluaXRlIHNwaW47aGVpZ2h0OjI0cHg7d2lkdGg6MjRweH0uY29udGFpbmVyLWxvYWRlciAuc3Bpbm5lciBkaXY6bnRoLWNoaWxkKDIpe2JhY2tncm91bmQtY29sb3I6IzU4NTk1Yjstd2Via2l0LWFuaW1hdGlvbjoxLjhzIGVhc2UtaW4tb3V0IC42cyBpbmZpbml0ZSBzcGluO2FuaW1hdGlvbjoxLjhzIGVhc2UtaW4tb3V0IC42cyBpbmZpbml0ZSBzcGluO2hlaWdodDoyMHB4O3dlaWdodDoyMHB4fS5jb250YWluZXItbG9hZGVyIC5zcGlubmVyIGRpdjpudGgtY2hpbGQoMyl7YmFja2dyb3VuZC1jb2xvcjojMDA0NDhkOy13ZWJraXQtYW5pbWF0aW9uOjEuOHMgZWFzZS1pbi1vdXQgaW5maW5pdGUgc3BpbjthbmltYXRpb246MS44cyBlYXNlLWluLW91dCBpbmZpbml0ZSBzcGluO3dpZHRoOjEwcHg7aGVpZ2h0OjEwcHh9QC13ZWJraXQta2V5ZnJhbWVzIHNwaW57MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpfTMzLjMley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMzBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMzBweCl9NjYuNyV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgwKSB0cmFuc2xhdGVZKC00NXB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgwKSB0cmFuc2xhdGVZKC00NXB4KX0xMDAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpIHRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpIHRyYW5zbGF0ZVkoMCl9fUBrZXlmcmFtZXMgc3BpbnswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCl9MzMuMyV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgzMHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgzMHB4KX02Ni43JXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDApIHRyYW5zbGF0ZVkoLTQ1cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDApIHRyYW5zbGF0ZVkoLTQ1cHgpfTEwMCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCkgdHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCkgdHJhbnNsYXRlWSgwKX19QC13ZWJraXQta2V5ZnJhbWVzIGZhZGVpbntmcm9te29wYWNpdHk6MH10b3tvcGFjaXR5OjF9fUBrZXlmcmFtZXMgZmFkZWlue2Zyb217b3BhY2l0eTowfXRve29wYWNpdHk6MX19QC13ZWJraXQta2V5ZnJhbWVzIGZhZGVvdXR7ZnJvbXtvcGFjaXR5OjF9dG97b3BhY2l0eTowfX1Aa2V5ZnJhbWVzIGZhZGVvdXR7ZnJvbXtvcGFjaXR5OjF9dG97b3BhY2l0eTowfX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9hZGluZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbW9kYWwnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRhaW5lclwiIFtzdHlsZS5kaXNwbGF5XT1kaXNwbGF5IGlkPVwie3sgaWQgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwiY2VudGVyZWRcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJyb3cgbW9kYWwtaGVhZGVyIGFsaWduLWl0ZW1zLWNlbnRlclwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgICAgIDxoMj57eyB0aXRsZU1vZGFsIH19PC9oMj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aSByb3V0ZXJMaW5rPVwiL2xvZ2luXCIgY2xhc3M9XCJmYWwgZmEtdGltZXMgY2xvc2VcIiAoY2xpY2spPVwiY2xvc2VNb2RhbCgpXCIgaWQ9XCJidG5fY2xvc2VcIj48L2k+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keSB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwiaWNvbi1leGl0XCI+PC9pPlxyXG4gICAgICAgIDxwIGNsYXNzPVwic2Vzc2lvbi1tZXNzYWdlXCI+e3sgbWVzc2FnZUNvbnRlbnQgfX08L3A+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyIGFjdGlvbnNcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtNVwiPlxyXG4gICAgICAgICAgICA8YXBwLWJ1dHRvbiBpZD1cImJ0bl9hY2NlcHRcIiAoY2xpY2spPVwiY2xvc2VNb2RhbCgpXCIgcm91dGVyTGluaz1cIi9sb2dpblwiIHR5cGU9XCJwcmltYXJ5LWdyYXlcIiBsYWJlbD1cInt7IGxhYmVsQWN0aW9uQnRuIH19XCI+PC9hcHAtYnV0dG9uPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2Bib2R5e292ZXJmbG93OmhpZGRlbn0ubW9kYWwtY29udGFpbmVye2Rpc3BsYXk6bm9uZTtwb3NpdGlvbjpmaXhlZDt6LWluZGV4OjEwMDtsZWZ0OjA7dG9wOjA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTstd2Via2l0LWJhY2tkcm9wLWZpbHRlcjpibHVyKDNweCk7YmFja2Ryb3AtZmlsdGVyOmJsdXIoM3B4KTtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMjQ3LDI0NywyNDcsLjgpfS5tb2RhbC1jb250YWluZXIgLmNlbnRlcmVke2Rpc3BsYXk6dGFibGU7bWFyZ2luOjEyJSBhdXRvfS5tb2RhbC1jb250ZW50e21pbi13aWR0aDo1MDBweDtib3JkZXItcmFkaXVzOjhweDtib3gtc2hhZG93OjAgMnB4IDZweCAwIHJnYmEoMCwwLDAsLjA3KTtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Ym9yZGVyOjFweCBzb2xpZCAjZjFmMWYxO2FsaWduLWl0ZW1zOmNlbnRlcn0ubW9kYWwtY29udGVudCAubW9kYWwtaGVhZGVye3dpZHRoOjEwMCU7cGFkZGluZzoxNXB4O2JvcmRlci1ib3R0b206MXB4IHNvbGlkIHJnYmEoMTUxLDE1MSwxNTEsLjE1KTtjb2xvcjojNDU0NjQ4O21hcmdpbjowO3Bvc2l0aW9uOnJlbGF0aXZlfS5tb2RhbC1jb250ZW50IC5tb2RhbC1oZWFkZXIgLmNsb3Nle3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjE1cHg7Y3Vyc29yOnBvaW50ZXI7Zm9udC1zaXplOjEuNWVtO2NvbG9yOiM1ODU5NWI7b3BhY2l0eTouNX0ubW9kYWwtY29udGVudCAubW9kYWwtaGVhZGVyIGgye2ZvbnQtc2l6ZToxLjEyZW07Zm9udC13ZWlnaHQ6NzAwO21hcmdpbi1ib3R0b206MH0ubW9kYWwtY29udGVudCAubW9kYWwtYm9keXtwYWRkaW5nOjMwcHggMH0ubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuaWNvbi1leGl0e3BhZGRpbmctYm90dG9tOjMwcHg7Y29udGVudDp1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb25zL2V4aXQuc3ZnKX0ubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuc2Vzc2lvbi1tZXNzYWdle3BhZGRpbmctYm90dG9tOjMwcHh9QG1lZGlhIChtYXgtd2lkdGg6NTc2cHgpey5pY29ue3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjEwcHg7dG9wOi0zMnB4fS5tb2RhbC1jb250ZW50e21pbi13aWR0aDozMjBweCFpbXBvcnRhbnR9LmNlbnRlcmVke21hcmdpbjozNSUgYXV0b319YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE1vZGFsQ29tcG9uZW50IHtcclxuXHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBASW5wdXQoKSB0aXRsZU1vZGFsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbWVzc2FnZUNvbnRlbnQ6IHN0cmluZztcclxuICBASW5wdXQoKSBsYWJlbEFjdGlvbkJ0bjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb25Nb2RhbEJ0bjogc3RyaW5nO1xyXG5cclxuICBkaXNwbGF5OiBzdHJpbmc7XHJcblxyXG4gIG9wZW5Nb2RhbCgpIHtcclxuICAgIHRoaXMuZGlzcGxheSA9ICdibG9jayc7XHJcbiAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XHJcbiAgfVxyXG5cclxuICBjbG9zZU1vZGFsKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdhdXRvJztcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLW5hdmJhcicsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwibmF2YmFyXCI+XHJcbiAgPG5hdiBjbGFzcz1cIm1haW4tbmF2YmFyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibmF2LWNvbnRhaW5lclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02XCI+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvbG9nb0JhbmlzdG1vLnN2Z1wiPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNlwiPlxyXG4gICAgICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvbmF2PlxyXG4gIDxuYXYgY2xhc3M9XCJzZWNvbmRhcnktbmF2YmFyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibmF2LWNvbnRhaW5lclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02IHdlbGNvbWUtdGV4dFwiPlxyXG4gICAgICAgICAgQmllbnZlbmlkbyBhIGxhIFN1Y3Vyc2FsIFZpcnR1YWwgUGVyc29uYXNcclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTYgZGF0ZVwiPlxyXG4gICAgICAgICAgXHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9uYXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AubmF2YmFye3dpZHRoOjEwMCV9Lm5hdmJhciAubWFpbi1uYXZiYXJ7cGFkZGluZzoxM3B4IDA7YmFja2dyb3VuZC1jb2xvcjojZmZmfS5uYXZiYXIgLnNlY29uZGFyeS1uYXZiYXJ7cGFkZGluZzo0cHggMDtiYWNrZ3JvdW5kLWNvbG9yOiNmZmQyMDA7bWluLWhlaWdodDozMHB4O2xpbmUtaGVpZ2h0OjI1cHh9Lm5hdmJhciAuc2Vjb25kYXJ5LW5hdmJhciAud2VsY29tZS10ZXh0e2ZvbnQtd2VpZ2h0OjcwMDtmb250LXNpemU6Ljg4ZW19Lm5hdmJhciAuc2Vjb25kYXJ5LW5hdmJhciAuZGF0ZXtmb250LXNpemU6MTJweDt0ZXh0LWFsaWduOnJpZ2h0fS5uYXZiYXIgLm5hdi1jb250YWluZXJ7bWF4LXdpZHRoOjk0MnB4O21hcmdpbjphdXRvfS5uYXZiYXIgaW1ne2hlaWdodDozNHB4fUBtZWRpYSAobWF4LXdpZHRoOjU3NnB4KXsuZGF0ZSwubWFpbi1uYXZiYXIsLnNlY29uZGFyeS1uYXZiYXJ7dGV4dC1hbGlnbjpjZW50ZXIhaW1wb3J0YW50fX1AbWVkaWEgKG1heC13aWR0aDo3NjhweCl7Lm5hdi1jb250YWluZXJ7bWF4LXdpZHRoOjEwMCU7cGFkZGluZzowIDI2cHghaW1wb3J0YW50O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveH19QG1lZGlhIChtYXgtd2lkdGg6OTkycHgpey5uYXYtY29udGFpbmVye3BhZGRpbmc6MCAxNXB4fX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmF2YmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBkYXRlID0gbmV3IERhdGUoKTtcclxuXHJcbiAgQElucHV0KCkgY29udGVudDogYW55O1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLW5vdGlmaWNhdGlvbicsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwibm90aWZpY2F0aW9uIHt7IHR5cGUgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgPGkgY2xhc3M9XCJ7eyBpY29uIH19XCI+PC9pPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICB7eyBtZXNzYWdlIH19XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2Aubm90aWZpY2F0aW9ue3dpZHRoOmF1dG87aGVpZ2h0OmF1dG87Ym9yZGVyLXJhZGl1czo4cHg7cGFkZGluZzoxNXB4O21hcmdpbi1ib3R0b206MTVweH0ubm90aWZpY2F0aW9uIC5pY29ue2Zsb2F0OmxlZnQ7bWFyZ2luLXJpZ2h0OjE1cHg7Zm9udC1zaXplOjE4cHh9Lm5vdGlmaWNhdGlvbiAuY29udGVudHt0ZXh0LWFsaWduOmNlbnRlcn0ubm90aWZpY2F0aW9uLmVycm9ye2JhY2tncm91bmQtY29sb3I6I2ZhNWU1Yjtjb2xvcjojZmZmfS5ub3RpZmljYXRpb24uaW5mb3tiYWNrZ3JvdW5kLWNvbG9yOiMwMDQ0OGQ7Y29sb3I6I2ZmZn0ubm90aWZpY2F0aW9uLnN1Y2Nlc3N7YmFja2dyb3VuZC1jb2xvcjojMTZjOThkO2NvbG9yOiNmZmZ9Lm5vdGlmaWNhdGlvbi53YXJuaW5ne2JhY2tncm91bmQtY29sb3I6I2ZmZDIwMDtjb2xvcjojNDU0NjQ4fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1vdXRsaW5lLWJ1dHRvbicsXHJcbiAgdGVtcGxhdGU6IGA8YnV0dG9uIGNsYXNzPVwiYnRuLW91dGxpbmVcIiBpZD1cInt7IGlkIH19XCI+XHJcbiAgPGkgY2xhc3M9XCJpY29uLWxlZnQge3sgaWNvbiB9fVwiPjwvaT5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvYnV0dG9uPlxyXG5gLFxyXG4gIHN0eWxlczogW2AuYnRuLW91dGxpbmV7d2lkdGg6Mjg3cHg7aGVpZ2h0OjU5cHg7Ym9yZGVyLXJhZGl1czoyOS41cHg7Ym9yZGVyOjFweCBzb2xpZCAjM2EzYjNiO2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Zm9udC1zaXplOi44NWVtO2ZvbnQtd2VpZ2h0OjcwMDtwYWRkaW5nOjAgMTVweDttYXJnaW4tdG9wOjEwcHg7Y3Vyc29yOnBvaW50ZXJ9LmJ0bi1vdXRsaW5lOmhvdmVye2JvcmRlcjoycHggc29saWQgIzNhM2IzYn0uYnRuLW91dGxpbmUgLmljb24tbGVmdHtmb250LXNpemU6MS44ZW07d2lkdGg6MzBweDt2ZXJ0aWNhbC1hbGlnbjpib3R0b207bGluZS1oZWlnaHQ6MTBweH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgT3V0bGluZUJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1wYXNzd29yZC1maWVsZCcsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwidGV4dC1maWVsZCB7eyBzdGF0ZSB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICA8bGFiZWw+XHJcbiAgICAgIDxpbnB1dCB0eXBlPVwie3sgdHlwZSB9fVwiIGlkPVwie3sgaWQgfX1cIiBjbGFzcz1cInt7IGVtcHR5SW5wdXQgfX1cIiBbKG5nTW9kZWwpXT1cInZhbHVlSW5wdXRQYXNzd29yZFwiIG5hbWU9XCJ0ZXh0LWZpZWxkXCIgcmVxdWlyZWQgKGNoYW5nZSk9XCJ2YWxpZGF0ZUZpZWxkKClcIlxyXG4gICAgICAgIGF1dG9jb21wbGV0ZT1cIm9mZlwiIG9ucGFzdGU9XCJyZXR1cm4gZmFsc2VcIiBvbkNvcHk9XCJyZXR1cm4gZmFsc2VcIiBpbnB1dC1kaXJlY3RpdmUgW21pbkxlbmd0aF09OCBbbWF4TGVuZ3RoXT0xNiBbcGF0dGVybl09XCJwYXR0ZXJuXCJcclxuICAgICAgICAoc2V0U3RhdGUpPVwib25TZXRTdGF0ZUNoYW5nZSgkZXZlbnQpXCI+XHJcbiAgICAgIDxzcGFuPnt7IGxhYmVsIH19PC9zcGFuPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgIDxpIFtjbGFzc109XCInZmFsIGZhLWV5ZScgKyBjbGFzc1wiIChtb3VzZWRvd24pPVwiY2xhc3M9Jy1zbGFzaCc7dHlwZT0ndGV4dCdcIiAobW91c2V1cCk9XCJjbGFzcz0nJzt0eXBlPSdwYXNzd29yZCdcIiA+PC9pPlxyXG4gICAgICAgIDxpICpuZ0lmPVwic3RhdGUgIT09ICdlcnJvcidcIiBjbGFzcz1cImZhbCBmYS1pbmZvLWNpcmNsZVwiPjwvaT5cclxuICAgICAgICA8aSAqbmdJZj1cInN0YXRlID09PSAnZXJyb3InXCIgY2xhc3M9XCJmYWwgZmEtdGltZXMgZXJyb3JcIiAoY2xpY2spPVwiY2xlYXJGaWVsZCgpXCI+PC9pPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvbGFiZWw+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj5cclxuICAgIDxwPnt7IG1lc3NhZ2UgfX08L3A+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AudGV4dC1maWVsZHttYXJnaW4tYm90dG9tOjEwcHh9LnRleHQtZmllbGQuZGlzYWJsZWR7b3BhY2l0eTouM30udGV4dC1maWVsZC5kaXNhYmxlZCAuY29udGVudCwudGV4dC1maWVsZC5kaXNhYmxlZCBpbnB1dCwudGV4dC1maWVsZC5kaXNhYmxlZCBsYWJlbCwudGV4dC1maWVsZC5kaXNhYmxlZCBzcGFue2N1cnNvcjpub3QtYWxsb3dlZCFpbXBvcnRhbnR9LnRleHQtZmllbGQuZXJyb3IgLmljb24gLmVycm9yLC50ZXh0LWZpZWxkLmVycm9yIGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQuZXJyb3IgcCwudGV4dC1maWVsZC5lcnJvciBzcGFue2NvbG9yOiNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIGlucHV0e2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkIC5jb250ZW50e3dpZHRoOjEwMCU7Y29sb3I6IzQ1NDY0ODtiYWNrZ3JvdW5kLWNvbG9yOiNmN2Y3Zjc7Ym9yZGVyLXJhZGl1czo2cHggNnB4IDAgMH0udGV4dC1maWVsZCAuY29udGVudCBsYWJlbHtwb3NpdGlvbjpyZWxhdGl2ZTtkaXNwbGF5OmJsb2NrO3dpZHRoOjEwMCU7bWluLWhlaWdodDo1MHB4fS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0e3Bvc2l0aW9uOmFic29sdXRlO3RvcDoxNXB4O3otaW5kZXg6MTt3aWR0aDoxMDAlO2ZvbnQtc2l6ZToxZW07Ym9yZGVyOjA7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgIzgwODI4NTt0cmFuc2l0aW9uOmJvcmRlci1jb2xvciAuMnMgZWFzZS1pbi1vdXQ7b3V0bGluZTowO2hlaWdodDozNXB4O2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OmZvY3Vze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmZmQyMDB9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6Zm9jdXMrc3BhbiwudGV4dC1maWVsZCAuY29udGVudCBpbnB1dDp2YWxpZCtzcGFue3RvcDo2cHg7Y3Vyc29yOmluaGVyaXQ7Zm9udC1zaXplOi44ZW07Y29sb3I6IzgwODI4NTtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuc3RhdHVze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICMwMDQ0OGR9LnRleHQtZmllbGQgLmNvbnRlbnQgc3Bhbntwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO3RvcDoyMHB4O3otaW5kZXg6Mjtmb250LXNpemU6MWVtO3RyYW5zaXRpb246YWxsIC4ycyBlYXNlLWluLW91dDt3aWR0aDoxMDAlO2N1cnNvcjp0ZXh0O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuaWNvbnt6LWluZGV4OjM7bWFyZ2luOjE1cHggMCAwO3JpZ2h0OjA7cG9zaXRpb246YWJzb2x1dGV9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb24gaXtmb250LXNpemU6MS4yZW07bGV0dGVyLXNwYWNpbmc6OHB4O2N1cnNvcjpwb2ludGVyfS50ZXh0LWZpZWxkIC5tZXNzYWdle21pbi1oZWlnaHQ6MjBweH0udGV4dC1maWVsZCAubWVzc2FnZSBwe3BhZGRpbmc6MCAwIDAgMTVweDtmb250LXNpemU6LjhlbTtjb2xvcjojODA4Mjg1O21hcmdpbjotM3B4IDAgMH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFzc3dvcmRGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHZhbHVlSW5wdXRQYXNzd29yZDogYW55O1xyXG4gIGVtcHR5SW5wdXQ6IHN0cmluZztcclxuICBAT3V0cHV0KCkgcHVibGljIHZhbHVlUGFzc3dvcmQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuICBASW5wdXQoKSBzdGF0ZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgcGF0dGVybjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG5cclxuICBwdWJsaWMgY2xhc3MgPSAnJztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcblxyXG4gIH1cclxuICBnZXRWYWx1ZVBhc3N3b3JkKCkge1xyXG4gICAgdGhpcy52YWx1ZVBhc3N3b3JkLmVtaXQodGhpcy52YWx1ZUlucHV0UGFzc3dvcmQpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnR5cGUgPSAncGFzc3dvcmQnO1xyXG5cclxuICB9XHJcblxyXG4gIHZhbGlkYXRlRmllbGQoKSB7XHJcbiAgICB0aGlzLmdldFZhbHVlUGFzc3dvcmQoKTtcclxuICAgKHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkICE9PSBudWxsICYmIHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkICE9PSAnJykgPyB0aGlzLmVtcHR5SW5wdXQgPSAnc3RhdHVzJyA6IHRoaXMuZW1wdHlJbnB1dCA9ICcnO1xyXG4gIH1cclxuICBjbGVhckZpZWxkKCkge1xyXG4gICAgdGhpcy52YWx1ZUlucHV0UGFzc3dvcmQgPSAnJztcclxuICAgIHRoaXMuc3RhdGUgPSAnJztcclxuICAgIHRoaXMubWVzc2FnZSA9ICcnO1xyXG4gIH1cclxuXHJcbiAgb25TZXRTdGF0ZUNoYW5nZShldmVudCl7XHJcbiAgICB0aGlzLnN0YXRlPWV2ZW50LnN0YXRlO1xyXG4gICAgaWYoZXZlbnQuc3RhdGUgIT09ICcnKXtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJ05vIGN1bXBsZSBjb24gbGEgbG9uZ2l0dWQgbcODwq1uaW1hJ1xyXG4gICAgfSBlbHNle1xyXG4gICAgICB0aGlzLm1lc3NhZ2UgPSAnJztcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtc2Vjb25kYXJ5LWxpbmsnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cInNlY29uZGFyeS1saW5rXCI+XHJcbiAgPGkgY2xhc3M9XCJpY29uLWxlZnQge3sgaWNvbiB9fVwiPlxyXG4gIDwvaT5cclxuICA8YSBocmVmPVwiXCI+e3sgbGFiZWxMaW5rIH19PC9hPlxyXG4gIDxpIGNsYXNzPVwiZmFzIGZhLXBsdXMgaWNvbi1yaWdodFwiPlxyXG4gIDwvaT5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5zZWNvbmRhcnktbGlua3ttYXJnaW4tYm90dG9tOjMwcHh9LnNlY29uZGFyeS1saW5rIC5pY29uLWxlZnR7Zm9udC1zaXplOjJlbTtmbG9hdDpsZWZ0O21hcmdpbi1yaWdodDoyMHB4O3dpZHRoOjM1cHg7dGV4dC1hbGlnbjpjZW50ZXJ9LnNlY29uZGFyeS1saW5rIGF7Y29sb3I6IzNhM2IzYjtmb250LXNpemU6LjllbTtmb250LXdlaWdodDo3MDA7bGluZS1oZWlnaHQ6MzJweH0uc2Vjb25kYXJ5LWxpbmsgYTpob3Zlcntjb2xvcjojZmZkMjAwO2ZvbnQtd2VpZ2h0OjcwMDt0ZXh0LWRlY29yYXRpb246bm9uZX0uc2Vjb25kYXJ5LWxpbmsgYTpmb2N1c3tjb2xvcjojM2EzYjNifS5zZWNvbmRhcnktbGluayAuaWNvbi1yaWdodHtmb250LXdlaWdodDo3MDA7Zm9udC1zaXplOi41NWVtO2Zsb2F0OnJpZ2h0O2xpbmUtaGVpZ2h0OjMycHh9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlY29uZGFyeUxpbmtDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIGxhYmVsTGluazogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG4gIFxyXG4gIFxyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtc2VsZWN0JyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJ0ZXh0LWZpZWxkIHt7IHN0YXRlIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgIDxsYWJlbD5cclxuICAgICAgPHNlbGVjdCBjbGFzcz1cInt7IGVtcHR5U2VsZWN0IH19XCIgWyhuZ01vZGVsKV09XCJ2YWx1ZVNlbGVjdFwiIG5hbWU9XCJzZWxlY3RcIiByZXF1aXJlZFxyXG4gICAgICAgICAgICAgIChjaGFuZ2UpPVwidmFsaWRhdGVGaWVsZCgpXCIgW2Rpc2FibGVkXT1cIihzdGF0ZSA9PSAnZGlzYWJsZWQnKVwiPlxyXG5cclxuICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgIDwvc2VsZWN0PlxyXG4gICAgICA8c3Bhbj5TZWxlY2Npb25lPC9zcGFuPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwie3sgaWNvbiB9fVwiPjwvaT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2xhYmVsPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJtZXNzYWdlXCI+XHJcbiAgICA8cD57eyBtZXNzYWdlIH19PC9wPlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLnRleHQtZmllbGR7bWFyZ2luLWJvdHRvbToxMHB4fS50ZXh0LWZpZWxkLmRpc2FibGVke29wYWNpdHk6LjN9LnRleHQtZmllbGQuZGlzYWJsZWQgLmNvbnRlbnQsLnRleHQtZmllbGQuZGlzYWJsZWQgaW5wdXQsLnRleHQtZmllbGQuZGlzYWJsZWQgbGFiZWwsLnRleHQtZmllbGQuZGlzYWJsZWQgc3BhbntjdXJzb3I6bm90LWFsbG93ZWQhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIC5pY29uLC50ZXh0LWZpZWxkLmVycm9yIGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQuZXJyb3IgcCwudGV4dC1maWVsZC5lcnJvciBzcGFue2NvbG9yOiNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIGlucHV0e2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkIC5jb250ZW50e3dpZHRoOjEwMCU7Y29sb3I6IzQ1NDY0ODtiYWNrZ3JvdW5kLWNvbG9yOiNmN2Y3Zjc7Ym9yZGVyLXJhZGl1czo2cHggNnB4IDAgMH0udGV4dC1maWVsZCAuY29udGVudCBsYWJlbHtwb3NpdGlvbjpyZWxhdGl2ZTtkaXNwbGF5OmJsb2NrO3dpZHRoOjEwMCU7bWluLWhlaWdodDo1MHB4fS50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTVweDt6LWluZGV4OjI7d2lkdGg6MTAwJTtmb250LXNpemU6MTZweDtib3JkZXI6MDtib3JkZXItYm90dG9tOjFweCBzb2xpZCAjODA4Mjg1O3RyYW5zaXRpb246Ym9yZGVyLWNvbG9yIC4ycyBlYXNlLWluLW91dDtvdXRsaW5lOjA7aGVpZ2h0OjM1cHg7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHg7LW1vei1hcHBlYXJhbmNlOm5vbmU7LXdlYmtpdC1hcHBlYXJhbmNlOm5vbmU7YXBwZWFyYW5jZTpub25lfS50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdDpmb2N1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmZkMjAwfS50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdDp2YWxpZCtzcGFue3RvcDo2cHg7Y3Vyc29yOmluaGVyaXQ7Zm9udC1zaXplOjEzcHg7Y29sb3I6IzgwODI4NTtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuc3RhdHVze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICMwMDQ0OGR9LnRleHQtZmllbGQgLmNvbnRlbnQgc3Bhbntwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO3RvcDoyMHB4O3otaW5kZXg6MTtmb250LXNpemU6MTZweDt0cmFuc2l0aW9uOmFsbCAuMnMgZWFzZS1pbi1vdXQ7d2lkdGg6MTAwJTtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb257Zm9udC1zaXplOjE4cHg7cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDozO3JpZ2h0OjE1cHg7dG9wOjE1cHh9LnRleHQtZmllbGQgLm1lc3NhZ2V7bWluLWhlaWdodDoyMHB4fS50ZXh0LWZpZWxkIC5tZXNzYWdlIHB7cGFkZGluZzo0cHggMCAwIDE1cHg7Zm9udC1zaXplOjEycHg7Y29sb3I6IzgwODI4NTttYXJnaW46MH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgdmFsdWVTZWxlY3QgPSAwO1xyXG4gIGVtcHR5U2VsZWN0OiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHR5cGU6IHN0cmluZztcclxuICBASW5wdXQoKSBzdGF0ZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG4gIHZhbGlkYXRlRmllbGQoKSB7XHJcbiAgICAodGhpcy52YWx1ZVNlbGVjdCAhPT0gMCkgPyB0aGlzLmVtcHR5U2VsZWN0ID0gJ3N0YXR1cycgOiB0aGlzLmVtcHR5U2VsZWN0ID0gJyc7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtc3VidGl0bGUnLFxyXG4gIHRlbXBsYXRlOiBgPGgyPlxyXG4gIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuPC9oMj5cclxuYCxcclxuICBzdHlsZXM6IFtgaDJ7Zm9udC1zaXplOjE3cHg7Zm9udC13ZWlnaHQ6NzAwO2xpbmUtaGVpZ2h0OjEuMzV9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFN1YnRpdGxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRhYicsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IFtoaWRkZW5dPVwiIWFjdGl2ZVwiIGNsYXNzPVwicGFuZVwiPlxyXG4gIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuPC9kaXY+YCxcclxuICBzdHlsZXM6IFtgLnBhbmV7cGFkZGluZzozMHB4IDIwcHg7anVzdGlmeS1jb250ZW50OmNlbnRlcn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFiQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcblxyXG4gIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBASW5wdXQoKSBhY3RpdmUgPSBmYWxzZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgQ29udGVudENoaWxkcmVuLCBJbnB1dCwgT25Jbml0LCBBZnRlckNvbnRlbnRJbml0LCBRdWVyeUxpc3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHtUYWJDb21wb25lbnR9IGZyb20gJy4uL3RhYi90YWIuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRhYnMnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cIm5hdiBuYXYtdGFic1wiPlxyXG4gIDxzcGFuICpuZ0Zvcj1cImxldCB0YWIgb2YgdGFic1wiIGlkPVwie3t0YWIuaWR9fVwiIChjbGljayk9XCJzZWxlY3RUYWIodGFiKVwiIFtjbGFzcy5hY3RpdmVdPVwidGFiLmFjdGl2ZVwiIGNsYXNzPVwidGFiXCJcclxuICAgICAgICBuZ0NsYXNzPVwie3twb3NpdGlvbn19XCI+XHJcbiAgICA8c3BhbiByb2xlPVwiYnV0dG9uXCIgY2xhc3M9XCJ0YWItdGl0bGVcIj57e3RhYi50aXRsZX19PC9zcGFuPlxyXG4gIDwvc3Bhbj5cclxuPC9kaXY+XHJcbjxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuYCxcclxuICBzdHlsZXM6IFtgLm5hdi10YWJze2Rpc3BsYXk6ZmxleDtmbGV4LXdyYXA6bm93cmFwO2ZvbnQtc2l6ZToxZW07Zm9udC13ZWlnaHQ6NzAwO3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTtib3JkZXItYm90dG9tOjFweCBzb2xpZCAjZTZlN2U4fS5uYXYtdGFicyAudGFie3dpZHRoOjUwJTtwYWRkaW5nOjE4cHggMTBweCAxMHB4O3RleHQtYWxpZ246Y2VudGVyfS5uYXYtdGFicyAuY2VudGVye2ZsZXg6MSAxO2Rpc3BsYXk6ZmxleH0ubmF2LXRhYnMgLnRhYi10aXRsZXtwYWRkaW5nOjAgN3B4IDVweDtjdXJzb3I6cG9pbnRlcn0ubmF2LXRhYnMgLmFjdGl2ZXtib3JkZXItYm90dG9tOnNvbGlkICNmZmQyMDB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyQ29udGVudEluaXQgIHtcclxuXHJcbiAgQElucHV0KCkgcG9zaXRpb246IHN0cmluZztcclxuICBAQ29udGVudENoaWxkcmVuKFRhYkNvbXBvbmVudCkgdGFiczogUXVlcnlMaXN0PFRhYkNvbXBvbmVudD47XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbiAgbmdBZnRlckNvbnRlbnRJbml0KCkge1xyXG4gICAgLy8gZ2V0IGFsbCBhY3RpdmUgdGFic1xyXG4gICAgY29uc3QgYWN0aXZlVGFicyA9IHRoaXMudGFicy5maWx0ZXIoKHRhYikgPT4gdGFiLmFjdGl2ZSk7XHJcblxyXG4gICAgLy8gaWYgdGhlcmUgaXMgbm8gYWN0aXZlIHRhYiBzZXQsIGFjdGl2YXRlIHRoZSBmaXJzdFxyXG4gICAgaWYgKGFjdGl2ZVRhYnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0VGFiKHRoaXMudGFicy5maXJzdCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZWxlY3RUYWIodGFiOiBUYWJDb21wb25lbnQpIHtcclxuICAgIC8vIGRlYWN0aXZhdGUgYWxsIHRhYnNcclxuICAgIHRoaXMudGFicy50b0FycmF5KCkuZm9yRWFjaCggdGFiID0+IHRhYi5hY3RpdmUgPSBmYWxzZSk7XHJcblxyXG4gICAgLy8gYWN0aXZhdGUgdGhlIHRhYiB0aGUgdXNlciBoYXMgY2xpY2tlZCBvbi5cclxuICAgIHRhYi5hY3RpdmUgPSB0cnVlO1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXQsIEV2ZW50RW1pdHRlcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7T3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRleHQtZmllbGQnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cInRleHQtZmllbGQge3sgc3RhdGUgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxyXG4gICAgPGxhYmVsPlxyXG4gICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cInt7IGlkIH19XCIgY2xhc3M9XCJ7eyBpbnB1dFN0eWxlIH19XCIgWyhuZ01vZGVsKV09XCJ2YWx1ZUlucHV0XCIgbmFtZT1cInRleHQtZmllbGRcIiBbcmVxdWlyZWRdPVwidHJ1ZVwiXHJcbiAgICAgIGlucHV0LWRpcmVjdGl2ZSBbbWluTGVuZ3RoXT04IFttYXhMZW5ndGhdPTE2IFtwYXR0ZXJuXT1cInBhdHRlcm5cIiAoc2V0U3RhdGUpPVwib25TZXRTdGF0ZUNoYW5nZSgkZXZlbnQpXCIgKGtleXVwKT1cInZhbGlkYXRlRmllbGQoKVwiIFtkaXNhYmxlZF09XCIoc3RhdGUgPT0gJ2Rpc2FibGVkJylcIj5cclxuICAgICAgPHNwYW4+e3sgbGFiZWwgfX08L3NwYW4+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJpY29uXCI+XHJcbiAgICAgICAgPGkgY2xhc3M9XCJ7eyBpY29uUHJpbWFyeSB9fVwiPjwvaT5cclxuICAgICAgICA8aSBjbGFzcz1cInt7IGljb25TZWNvbmRhcnkgfX1cIj48L2k+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9sYWJlbD5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwibWVzc2FnZVwiPlxyXG4gICAgPHA+e3sgbWVzc2FnZSB9fTwvcD5cclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC50ZXh0LWZpZWxke21hcmdpbi1ib3R0b206MTBweH0udGV4dC1maWVsZC5kaXNhYmxlZHtvcGFjaXR5Oi4zfS50ZXh0LWZpZWxkLmRpc2FibGVkIC5jb250ZW50LC50ZXh0LWZpZWxkLmRpc2FibGVkIGlucHV0LC50ZXh0LWZpZWxkLmRpc2FibGVkIGxhYmVsLC50ZXh0LWZpZWxkLmRpc2FibGVkIHNwYW57Y3Vyc29yOm5vdC1hbGxvd2VkIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkLmVycm9yIHAsLnRleHQtZmllbGQuZXJyb3Igc3Bhbntjb2xvcjojZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciBpbnB1dHtib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZCAuY29udGVudHt3aWR0aDoxMDAlO2NvbG9yOiM0NTQ2NDg7YmFja2dyb3VuZC1jb2xvcjojZjdmN2Y3O2JvcmRlci1yYWRpdXM6NnB4IDZweCAwIDB9LnRleHQtZmllbGQgLmNvbnRlbnQgbGFiZWx7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt3aWR0aDoxMDAlO21pbi1oZWlnaHQ6NTBweH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTVweDt6LWluZGV4OjE7d2lkdGg6MTAwJTtmb250LXNpemU6MWVtO2JvcmRlcjowO2JvcmRlci1ib3R0b206MXB4IHNvbGlkICM4MDgyODU7dHJhbnNpdGlvbjpib3JkZXItY29sb3IgLjJzIGVhc2UtaW4tb3V0O291dGxpbmU6MDtoZWlnaHQ6MzVweDtiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dDpmb2N1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmZkMjAwfS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6dmFsaWQrc3Bhbnt0b3A6NnB4O2N1cnNvcjppbmhlcml0O2ZvbnQtc2l6ZTouOGVtO2NvbG9yOiM4MDgyODU7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLnN0YXR1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjMDA0NDhkfS50ZXh0LWZpZWxkIC5jb250ZW50IHNwYW57cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MjBweDt6LWluZGV4OjI7Zm9udC1zaXplOjFlbTt0cmFuc2l0aW9uOmFsbCAuMnMgZWFzZS1pbi1vdXQ7d2lkdGg6MTAwJTtjdXJzb3I6dGV4dDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb257ei1pbmRleDozO21hcmdpbjoxNXB4IDAgMDtyaWdodDowO3Bvc2l0aW9uOmFic29sdXRlfS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29uIGl7Zm9udC1zaXplOjEuMmVtO2xldHRlci1zcGFjaW5nOjE1cHg7Y3Vyc29yOnBvaW50ZXJ9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb24gaTpudGgtY2hpbGQoMil7Y29sb3I6I2ZhNWU1Yn0udGV4dC1maWVsZCAubWVzc2FnZXttaW4taGVpZ2h0OjIwcHh9LnRleHQtZmllbGQgLm1lc3NhZ2UgcHtwYWRkaW5nOjAgMCAwIDE1cHg7Zm9udC1zaXplOi44ZW07Y29sb3I6IzgwODI4NTttYXJnaW46LTNweCAwIDB9YF0sXHJcblxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGV4dEZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBwdWJsaWMgdmFsdWVJbnB1dDtcclxuICBpbnB1dFN0eWxlOiBzdHJpbmc7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyB2YWx1ZVVzZXJuYW1lID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvblByaW1hcnk6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uU2Vjb25kYXJ5OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgc3RhdGU6IHN0cmluZztcclxuICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgcGF0dGVybjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcblxyXG4gIGdldFZhbHVlVXNlcm5hbWUoKSB7XHJcbiAgICB0aGlzLnZhbHVlVXNlcm5hbWUuZW1pdCh7XHJcbiAgICAgIHZhbHVlOiB0aGlzLnZhbHVlSW5wdXQsXHJcbiAgICAgIHN0YXRlOiB0aGlzLmlucHV0U3R5bGVcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG4gIHZhbGlkYXRlRmllbGQoKSB7XHJcbiAgICBpZiAodGhpcy52YWx1ZUlucHV0ICE9PSBudWxsICYmIHRoaXMudmFsdWVJbnB1dCAhPT0gJycpIHtcclxuICAgICAgdGhpcy5pbnB1dFN0eWxlID0gJ3N0YXR1cyc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmlucHV0U3R5bGUgPSAnJztcclxuICAgIH1cclxuICAgIHRoaXMuZ2V0VmFsdWVVc2VybmFtZSgpO1xyXG4gIH1cclxuXHJcbiAgb25TZXRTdGF0ZUNoYW5nZShldmVudCkge1xyXG4gICAgdGhpcy5zdGF0ZSA9IGV2ZW50LnN0YXRlO1xyXG4gICAgdGhpcy5pbnB1dFN0eWxlID0gZXZlbnQuc3RhdGU7XHJcbiAgICBpZiAoZXZlbnQuc3RhdGUgIT09ICcnKSB7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICdObyBjdW1wbGUgY29uIGxhIGxvbmdpdHVkIG3Dg8KtbmltYSc7XHJcbiAgICAgIHRoaXMuaWNvblNlY29uZGFyeSA9ICdmYWwgZmEtdGltZXMnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICAgIHRoaXMuaWNvblNlY29uZGFyeSA9ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnZhbHVlSW5wdXQgPT09ICcnKSB7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICcnO1xyXG4gICAgICB0aGlzLnN0YXRlID0gJyc7XHJcbiAgICAgIHRoaXMuaWNvblNlY29uZGFyeSA9ICcnO1xyXG4gICAgfVxyXG4gICAgdGhpcy5nZXRWYWx1ZVVzZXJuYW1lKCk7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuXHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtdGl0bGUnLFxyXG4gIHRlbXBsYXRlOiBgPGgxPlxyXG4gIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuPC9oMT5cclxuYCxcclxuICBzdHlsZXM6IFtgaDF7Zm9udC1zaXplOjI0cHg7Zm9udC13ZWlnaHQ6NzAwO2xpbmUtaGVpZ2h0OjEuMTd9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRpdGxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRvb2x0aXAnLFxyXG4gIHRlbXBsYXRlOiBgPGEgIGNsYXNzPVwidG9vbHRpcFwiIHRpdGxlPVwie3sgdGl0bGV9fVwiPlxyXG4gIDxpIGNsYXNzPVwie3sgaWNvbiB9fVwiPjwvaT5cclxuPC9hPmAsXHJcbiAgc3R5bGVzOiBbYC50b29sdGlwe2Rpc3BsYXk6aW5saW5lO3Bvc2l0aW9uOnJlbGF0aXZlfS50b29sdGlwOmhvdmVyOmFmdGVye2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuOCk7Ym9yZGVyLXJhZGl1czo1cHg7Ym90dG9tOjI2cHg7Y29sb3I6I2ZmZjtjb250ZW50OmF0dHIodGl0bGUpO2xlZnQ6LTYwMCU7cGFkZGluZzo1cHggMTVweDtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4Ojk4O3dpZHRoOjIyMHB4fS50b29sdGlwOmhvdmVyOmJlZm9yZXtib3JkZXI6c29saWQ7Ym9yZGVyLWNvbG9yOiMzMzMgdHJhbnNwYXJlbnQ7Ym9yZGVyLXdpZHRoOjZweCA2cHggMDtib3R0b206MjBweDtjb250ZW50OlwiXCI7bGVmdDowO3Bvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6OTl9LnRvb2x0aXAgaXtmb250LXNpemU6MS4yZW07bGV0dGVyLXNwYWNpbmc6MTVweDtjdXJzb3I6cG9pbnRlcn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9vbHRpcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHRpdGxlOnN0cmluZztcclxuICBASW5wdXQoKSBpY29uOnN0cmluZztcclxuICBcclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJhbm5lckNvbXBvbmVudCB9IGZyb20gJy4vYmFubmVyL2Jhbm5lci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBCdXR0b25Db21wb25lbnQgfSBmcm9tICcuL2J1dHRvbi9idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZENvbXBvbmVudCB9IGZyb20gJy4vY2FyZC9jYXJkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcm91c2VsQmFubmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jYXJvdXNlbC1iYW5uZXIvY2Fyb3VzZWwtYmFubmVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvb3RlckNvbXBvbmVudCB9IGZyb20gJy4vZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb290ZXJFeHRlcm5hbENvbXBvbmVudCB9IGZyb20gJy4vZm9vdGVyLWV4dGVybmFsL2Zvb3Rlci1leHRlcm5hbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBIeXBlcmxpbmtDb21wb25lbnQgfSBmcm9tICcuL2h5cGVybGluay9oeXBlcmxpbmsuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG9hZGluZ0NvbXBvbmVudCB9IGZyb20gJy4vbG9hZGluZy9sb2FkaW5nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC9tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOYXZiYXJDb21wb25lbnQgfSBmcm9tICcuL25hdmJhci9uYXZiYXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE91dGxpbmVCdXR0b25Db21wb25lbnQgfSBmcm9tICcuL291dGxpbmUtYnV0dG9uL291dGxpbmUtYnV0dG9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFBhc3N3b3JkRmllbGRDb21wb25lbnQgfSBmcm9tICcuL3Bhc3N3b3JkLWZpZWxkL3Bhc3N3b3JkLWZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlY29uZGFyeUxpbmtDb21wb25lbnQgfSBmcm9tICcuL3NlY29uZGFyeS1saW5rL3NlY29uZGFyeS1saW5rLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlbGVjdENvbXBvbmVudCB9IGZyb20gJy4vc2VsZWN0L3NlbGVjdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTdWJ0aXRsZUNvbXBvbmVudCB9IGZyb20gJy4vc3VidGl0bGUvc3VidGl0bGUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFiQ29tcG9uZW50IH0gZnJvbSAnLi90YWIvdGFiLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRhYnNDb21wb25lbnQgfSBmcm9tICcuL3RhYnMvdGFicy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUZXh0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL3RleHQtZmllbGQvdGV4dC1maWVsZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUaXRsZUNvbXBvbmVudCB9IGZyb20gJy4vdGl0bGUvdGl0bGUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVG9vbHRpcENvbXBvbmVudCB9IGZyb20gJy4vdG9vbHRpcC90b29sdGlwLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZVxyXG4gIF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBCYW5uZXJDb21wb25lbnQsXHJcbiAgICBCdXR0b25Db21wb25lbnQsXHJcbiAgICBDYXJkQ29tcG9uZW50LFxyXG4gICAgQ2Fyb3VzZWxCYW5uZXJDb21wb25lbnQsXHJcbiAgICBGb290ZXJDb21wb25lbnQsXHJcbiAgICBGb290ZXJFeHRlcm5hbENvbXBvbmVudCxcclxuICAgIEh5cGVybGlua0NvbXBvbmVudCxcclxuICAgIExvYWRpbmdDb21wb25lbnQsXHJcbiAgICBNb2RhbENvbXBvbmVudCxcclxuICAgIE5hdmJhckNvbXBvbmVudCxcclxuICAgIE5vdGlmaWNhdGlvbkNvbXBvbmVudCxcclxuICAgIE91dGxpbmVCdXR0b25Db21wb25lbnQsXHJcbiAgICBQYXNzd29yZEZpZWxkQ29tcG9uZW50LFxyXG4gICAgU2Vjb25kYXJ5TGlua0NvbXBvbmVudCxcclxuICAgIFNlbGVjdENvbXBvbmVudCxcclxuICAgIFN1YnRpdGxlQ29tcG9uZW50LFxyXG4gICAgVGFiQ29tcG9uZW50LFxyXG4gICAgVGFic0NvbXBvbmVudCxcclxuICAgIFRleHRGaWVsZENvbXBvbmVudCxcclxuICAgIFRpdGxlQ29tcG9uZW50LFxyXG4gICAgVG9vbHRpcENvbXBvbmVudFxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgQmFubmVyQ29tcG9uZW50LFxyXG4gICAgQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQ2FyZENvbXBvbmVudCxcclxuICAgIENhcm91c2VsQmFubmVyQ29tcG9uZW50LFxyXG4gICAgRm9vdGVyQ29tcG9uZW50LFxyXG4gICAgRm9vdGVyRXh0ZXJuYWxDb21wb25lbnQsXHJcbiAgICBIeXBlcmxpbmtDb21wb25lbnQsXHJcbiAgICBMb2FkaW5nQ29tcG9uZW50LFxyXG4gICAgTW9kYWxDb21wb25lbnQsXHJcbiAgICBOYXZiYXJDb21wb25lbnQsXHJcbiAgICBOb3RpZmljYXRpb25Db21wb25lbnQsXHJcbiAgICBPdXRsaW5lQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgUGFzc3dvcmRGaWVsZENvbXBvbmVudCxcclxuICAgIFNlY29uZGFyeUxpbmtDb21wb25lbnQsXHJcbiAgICBTZWxlY3RDb21wb25lbnQsXHJcbiAgICBTdWJ0aXRsZUNvbXBvbmVudCxcclxuICAgIFRhYkNvbXBvbmVudCxcclxuICAgIFRhYnNDb21wb25lbnQsXHJcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXHJcbiAgICBUaXRsZUNvbXBvbmVudCxcclxuICAgIFRvb2x0aXBDb21wb25lbnRcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWaWV3TGliTW9kdWxlIHsgfVxyXG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtJQU9FLGlCQUFpQjs7O1lBTGxCLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7Ozs7OztBQ0pEO0lBWUUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBYkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixRQUFRLEVBQUU7OztDQUdYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLGlJQUFpSSxDQUFDO2FBQzVJOzs7Ozs7Ozs7QUNURDtJQWVFLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7OztZQWhCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFFBQVEsRUFBRTs7O0NBR1g7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsdWdDQUF1Z0MsQ0FBQzthQUNsaEM7Ozs7O21CQUdFLEtBQUs7b0JBQ0wsS0FBSzs7Ozs7OztBQ2JSO0lBWUUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBYkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxVQUFVO2dCQUNwQixRQUFRLEVBQUU7OztDQUdYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHVKQUF1SixDQUFDO2FBQ2xLOzs7Ozs7Ozs7QUNURDtJQWdCRSxpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUFqQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLFFBQVEsRUFBRTs7Ozs7OztDQU9YO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHNnQkFBc2dCLENBQUM7YUFDamhCOzs7Ozs7Ozs7QUNiRDtJQXFCRSxpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUF0QkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7OztDQVlYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHVMQUF1TCxDQUFDO2FBQ2xNOzs7Ozs7Ozs7QUNsQkQ7SUF3QkUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBekJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7OztDQWVYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHlLQUF5SyxDQUFDO2FBQ3BMOzs7Ozs7Ozs7QUNyQkQ7SUFjRTtLQUNDOzs7O0lBRUQsUUFBUTtLQUNQOzs7WUFoQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxlQUFlO2dCQUN6QixRQUFRLEVBQUU7OztDQUdYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLG9GQUFvRixDQUFDO2FBQy9GOzs7OztvQkFFRSxLQUFLO2tCQUNMLEtBQUs7Ozs7Ozs7QUNaUjtJQXFCRSxpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUF0QkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2dCQUN2QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Q0FVWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyxpcUVBQWlxRSxDQUFDO2FBQzVxRTs7Ozs7aUJBR0UsS0FBSzs7Ozs7OztBQ25CUjs7OztJQXNDRSxTQUFTO1FBQ1AsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztLQUN6Qzs7OztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN0QixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDO0tBQ3ZDOzs7WUE1Q0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxXQUFXO2dCQUNyQixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQXFCWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyx5bENBQXlsQyxDQUFDO2FBQ3BtQzs7O2lCQUdFLEtBQUs7eUJBQ0wsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7MkJBQ0wsS0FBSzs7Ozs7OztBQ2xDUjtJQXNDRTtvQkFKTyxJQUFJLElBQUksRUFBRTtLQUlBOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBdkNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQTBCWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyx1ckJBQXVyQixDQUFDO2FBQ2xzQjs7Ozs7c0JBSUUsS0FBSzs7Ozs7OztBQ3BDUjtJQXFCRSxpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUF0QkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLFFBQVEsRUFBRTs7Ozs7Ozs7Q0FRWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyx3YUFBd2EsQ0FBQzthQUNuYjs7Ozs7bUJBR0UsS0FBSzttQkFDTCxLQUFLO3NCQUNMLEtBQUs7Ozs7Ozs7QUNuQlI7SUFlRSxpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUFoQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLFFBQVEsRUFBRTs7OztDQUlYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHNVQUFzVSxDQUFDO2FBQ2pWOzs7OztpQkFFRSxLQUFLO21CQUNMLEtBQUs7Ozs7Ozs7QUNiUjtJQXdDRTs2QkFYaUMsSUFBSSxZQUFZLEVBQUU7cUJBU3BDLEVBQUU7S0FJaEI7Ozs7SUFDRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztLQUNsRDs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztLQUV4Qjs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN6QixDQUFDLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLGtCQUFrQixLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztLQUN6SDs7OztJQUNELFVBQVU7UUFDUixJQUFJLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0tBQ25COzs7OztJQUVELGdCQUFnQixDQUFDLEtBQUs7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3ZCLElBQUcsS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFLEVBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxrQ0FBa0MsQ0FBQTtTQUNsRDthQUFLO1lBQ0osSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7U0FDbkI7S0FDRjs7O1lBbkVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQWtCWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyxvcURBQW9xRCxDQUFDO2FBQy9xRDs7Ozs7NEJBS0UsTUFBTTtvQkFDTixLQUFLO29CQUNMLEtBQUs7c0JBQ0wsS0FBSzttQkFDTCxLQUFLO3NCQUNMLEtBQUs7bUJBQ0wsS0FBSztpQkFDTCxLQUFLOzs7Ozs7O0FDcENSO0lBcUJFLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7OztZQXRCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsUUFBUSxFQUFFOzs7Ozs7O0NBT1g7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsa2FBQWthLENBQUM7YUFDN2E7Ozs7O3dCQUVFLEtBQUs7bUJBQ0wsS0FBSztpQkFDTCxLQUFLOzs7Ozs7O0FDakJSO0lBbUNFOzJCQVRjLENBQUM7S0FVZDs7OztJQUVELFFBQVE7S0FDUDs7OztJQUVELGFBQWE7UUFDWCxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO0tBQ2hGOzs7WUExQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQWtCWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQywyb0RBQTJvRCxDQUFDO2FBQ3RwRDs7Ozs7b0JBTUUsS0FBSzttQkFDTCxLQUFLO21CQUNMLEtBQUs7b0JBQ0wsS0FBSztzQkFDTCxLQUFLOzs7Ozs7O0FDakNSO0lBWUUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBYkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixRQUFRLEVBQUU7OztDQUdYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHFEQUFxRCxDQUFDO2FBQ2hFOzs7Ozs7Ozs7QUNURDtJQWdCRTtzQkFGa0IsS0FBSztLQUVOOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBakJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsU0FBUztnQkFDbkIsUUFBUSxFQUFFOztPQUVMO2dCQUNMLE1BQU0sRUFBRSxDQUFDLGlEQUFpRCxDQUFDO2FBQzVEOzs7OztvQkFJRSxLQUFLO2lCQUNMLEtBQUs7cUJBQ0wsS0FBSzs7Ozs7OztBQ2RSO0lBcUJFLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7Ozs7SUFFRCxrQkFBa0I7O1FBRWhCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxLQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7UUFHekQsSUFBSSxVQUFVLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDakM7S0FDRjs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBaUI7O1FBRXpCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFFLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDOztRQUd4RCxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztLQUNuQjs7O1lBdENGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsUUFBUSxFQUFFOzs7Ozs7O0NBT1g7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsK1VBQStVLENBQUM7YUFDMVY7Ozs7O3VCQUdFLEtBQUs7bUJBQ0wsZUFBZSxTQUFDLFlBQVk7Ozs7Ozs7QUNuQi9CO0lBNENFOzZCQWhCaUMsSUFBSSxZQUFZLEVBQUU7S0FpQmxEOzs7O0lBUkQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7WUFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVTtTQUN2QixDQUFDLENBQUM7S0FDSjs7OztJQUtELFFBQVE7S0FDUDs7OztJQUVELGFBQWE7UUFDWCxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssRUFBRSxFQUFFO1lBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO1NBQzVCO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztTQUN0QjtRQUNELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0tBQ3pCOzs7OztJQUVELGdCQUFnQixDQUFDLEtBQUs7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUM5QixJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsa0NBQWtDLENBQUM7WUFDbEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxjQUFjLENBQUM7U0FDckM7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1NBQ3pCO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUUsRUFBRTtZQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUNoQixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztTQUN6QjtRQUNELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0tBQ3pCOzs7WUF6RUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7OztDQWdCWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyw4ckRBQThyRCxDQUFDO2FBRXpzRDs7Ozs7NEJBSUUsTUFBTTtvQkFDTixLQUFLOzBCQUNMLEtBQUs7NEJBQ0wsS0FBSztvQkFDTCxLQUFLO3NCQUNMLEtBQUs7c0JBQ0wsS0FBSztpQkFDTCxLQUFLOzs7Ozs7O0FDbkNSO0lBWUUsaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBYkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxXQUFXO2dCQUNyQixRQUFRLEVBQUU7OztDQUdYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHFEQUFxRCxDQUFDO2FBQ2hFOzs7Ozs7Ozs7QUNURDtJQWNFLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7OztZQWZGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsUUFBUSxFQUFFOztLQUVQO2dCQUNILE1BQU0sRUFBRSxDQUFDLDhhQUE4YSxDQUFDO2FBQ3piOzs7OztvQkFHRSxLQUFLO21CQUNMLEtBQUs7Ozs7Ozs7QUNaUjs7O1lBeUJDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO2lCQUNaO2dCQUNELFlBQVksRUFBRTtvQkFDWixlQUFlO29CQUNmLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYix1QkFBdUI7b0JBQ3ZCLGVBQWU7b0JBQ2YsdUJBQXVCO29CQUN2QixrQkFBa0I7b0JBQ2xCLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFDZCxlQUFlO29CQUNmLHFCQUFxQjtvQkFDckIsc0JBQXNCO29CQUN0QixzQkFBc0I7b0JBQ3RCLHNCQUFzQjtvQkFDdEIsZUFBZTtvQkFDZixpQkFBaUI7b0JBQ2pCLFlBQVk7b0JBQ1osYUFBYTtvQkFDYixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsZUFBZTtvQkFDZixlQUFlO29CQUNmLGFBQWE7b0JBQ2IsdUJBQXVCO29CQUN2QixlQUFlO29CQUNmLHVCQUF1QjtvQkFDdkIsa0JBQWtCO29CQUNsQixnQkFBZ0I7b0JBQ2hCLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixxQkFBcUI7b0JBQ3JCLHNCQUFzQjtvQkFDdEIsc0JBQXNCO29CQUN0QixzQkFBc0I7b0JBQ3RCLGVBQWU7b0JBQ2YsaUJBQWlCO29CQUNqQixZQUFZO29CQUNaLGFBQWE7b0JBQ2Isa0JBQWtCO29CQUNsQixjQUFjO29CQUNkLGdCQUFnQjtpQkFDakI7YUFDRjs7Ozs7Ozs7Ozs7Ozs7OyJ9