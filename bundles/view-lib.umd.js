(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('view-lib', ['exports', '@angular/core', '@angular/forms', '@angular/common'], factory) :
    (factory((global['view-lib'] = {}),global.ng.core,global.ng.forms,global.ng.common));
}(this, (function (exports,i0,forms,common) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ViewLibService = (function () {
        function ViewLibService() {
        }
        ViewLibService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        ViewLibService.ctorParameters = function () { return []; };
        /** @nocollapse */ ViewLibService.ngInjectableDef = i0.defineInjectable({ factory: function ViewLibService_Factory() { return new ViewLibService(); }, token: ViewLibService, providedIn: "root" });
        return ViewLibService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var BannerComponent = (function () {
        function BannerComponent() {
        }
        /**
         * @return {?}
         */
        BannerComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        BannerComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-banner',
                        template: "<div class=\"banner\">\n  <ng-content></ng-content>\n</div>\n",
                        styles: [".banner{width:auto;height:auto;border-radius:6px;background-color:#fff;border:1.5px solid #ffd200;padding:15px;overflow:hidden}"]
                    },] },
        ];
        /** @nocollapse */
        BannerComponent.ctorParameters = function () { return []; };
        return BannerComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ButtonComponent = (function () {
        function ButtonComponent() {
        }
        /**
         * @return {?}
         */
        ButtonComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        ButtonComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-button',
                        template: "<button class=\"btn {{ type }}\">\n  {{ label }}\n</button>\n",
                        styles: [".btn{width:144px;height:40px;border-radius:20px;font-size:14px;font-weight:700;border:0;text-transform:uppercase}.btn.primary-gray{background-color:#58595b;color:#fff}.btn.primary-gray:hover{background-color:#363636}.btn.primary-gray:focus{background-color:#58595b}.btn.primary-yellow{background-color:#ffd200;color:#454648}.btn.primary-yellow:hover{background-color:#ffb500}.btn.primary-yellow:focus{background-color:#ffd200}.btn.primary-red{background-color:#fa5e5b;color:#fff}.btn.primary-red:hover{background-color:#d14d4a}.btn.primary-red:focus{background-color:#fa5e5b}.btn.white{background-color:#fff;color:#454648}.btn.white:hover{background-color:transparent;border:1px solid #fff}.btn.white:focus{background-color:#fff;color:#454648}.btn.secondary{border:1px solid #00448d;background-color:#fff;color:#00448d}.btn.secondary:hover{border-width:2px}.btn.secondary:focus{background-color:#e6e7e8}.btn.flat{border-radius:0;background-color:transparent;color:#333}.btn.flat:hover{color:#ffd200}.btn.flat:focus{color:#58595b}"]
                    },] },
        ];
        /** @nocollapse */
        ButtonComponent.ctorParameters = function () { return []; };
        ButtonComponent.propDecorators = {
            type: [{ type: i0.Input }],
            label: [{ type: i0.Input }]
        };
        return ButtonComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CardComponent = (function () {
        function CardComponent() {
        }
        /**
         * @return {?}
         */
        CardComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        CardComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-card',
                        template: "<div class=\"card\">\n  <ng-content></ng-content>\n</div>\n",
                        styles: [".card{background-color:#fff;border-radius:8px;border:1px solid #f1f1f1;box-shadow:0 2px 6px 0 rgba(0,0,0,.07);width:100%;height:100%;overflow:hidden}"]
                    },] },
        ];
        /** @nocollapse */
        CardComponent.ctorParameters = function () { return []; };
        return CardComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CarouselBannerComponent = (function () {
        function CarouselBannerComponent() {
        }
        /**
         * @return {?}
         */
        CarouselBannerComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        CarouselBannerComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-carousel-banner',
                        template: "<div class=\"main-container\">\n  <div class=\"carousel-banner\">\n    <div class=\"banner-container\">\n      <a href=\"\"><img class=\"img-fluid\" src=\"../../../assets/images/bitmap.png\"></a>\n    </div>\n  </div>\n</div>\n",
                        styles: [".main-container{width:100%}.main-container .carousel-banner{background-color:#e6e7e8;height:auto}.main-container .carousel-banner .banner-container{max-width:942px;margin:auto;text-align:right}@media (max-width:768px){.carousel-banner{background-color:transparent!important}.carousel-banner .banner-container{max-width:100%;padding:0 26px!important;text-align:center!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.banner-container{padding:0 15px}}"]
                    },] },
        ];
        /** @nocollapse */
        CarouselBannerComponent.ctorParameters = function () { return []; };
        return CarouselBannerComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var FooterComponent = (function () {
        function FooterComponent() {
        }
        /**
         * @return {?}
         */
        FooterComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        FooterComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-footer',
                        template: "<footer>\n  <div class=\"contact-information text-center\">\n    <div>\n      <i class=\"footer-icon fal fa-phone\"></i>\n      Sucursal Telef\u00F3nica : 507-306-4700\n    </div>\n    <div>Copyright Banistmo SA. 2018</div>\n  </div>\n</footer>\n\n\n\n",
                        styles: ["footer{width:100%;background-color:#e6e7e8;padding:25px 0}footer .contact-information{font-size:.8em}footer .contact-information i{-webkit-transform:scaleX(-1);transform:scaleX(-1)}"]
                    },] },
        ];
        /** @nocollapse */
        FooterComponent.ctorParameters = function () { return []; };
        return FooterComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var FooterExternalComponent = (function () {
        function FooterExternalComponent() {
        }
        /**
         * @return {?}
         */
        FooterExternalComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        FooterExternalComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-footer-external',
                        template: "<footer>\n  <div class=\"container\">\n    <div class=\"row justify-content-md-center text-center\">\n      <div class=\"col col-lg-2\">\n        <a href=\"\" id=\"info_schedules\">Horarios</a>\n      </div>\n      <div class=\"col col-lg-2\">\n        <a href=\"\" id=\"info_commissions\">Comisiones</a>\n      </div>\n      <div class=\"col col-lg-2\">\n        <a href=\"\" id=\"info_security\">Seguridad</a>\n      </div>\n    </div>\n  </div>\n</footer>\n",
                        styles: ["footer{width:100%;background-color:#e6e7e8;padding:25px 0 0}footer a{color:#454648;font-size:.8em;line-height:34px}footer a:hover{font-weight:700;text-decoration:none}"]
                    },] },
        ];
        /** @nocollapse */
        FooterExternalComponent.ctorParameters = function () { return []; };
        return FooterExternalComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var HyperlinkComponent = (function () {
        function HyperlinkComponent() {
        }
        /**
         * @return {?}
         */
        HyperlinkComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        HyperlinkComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-hyperlink',
                        template: "<a href=\"{{ url }}\">\n  {{ label }}\n</a>\n",
                        styles: ["a{font-size:.85em;color:#00448d;text-decoration:underline}a:hover{font-weight:700}"]
                    },] },
        ];
        /** @nocollapse */
        HyperlinkComponent.ctorParameters = function () { return []; };
        HyperlinkComponent.propDecorators = {
            label: [{ type: i0.Input }],
            url: [{ type: i0.Input }]
        };
        return HyperlinkComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LoadingComponent = (function () {
        function LoadingComponent() {
        }
        /**
         * @return {?}
         */
        LoadingComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        LoadingComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-loading',
                        template: "<div class=\"blocking-background\"></div>\n<div class=\"container-loader\" id=\"{{ id }}\">\n  <div class=\"loader\">\n    <div class=\"spinner\">\n      <div></div>\n      <div></div>\n      <div></div>\n    </div>\n  </div>\n</div>\n",
                        styles: [".blocking-background{background-color:rgba(255,255,255,.4);position:fixed;top:0;left:0;width:100%;height:100%;z-index:400}.container-loader{background-color:rgba(255,255,255,.7);position:absolute;top:0;left:0;width:100%;height:100%;z-index:400;border-radius:8px}.container-loader .loader{display:block;position:relative;left:50%;top:50%;z-index:500}.container-loader[loaded] .spinner{-webkit-animation:2s ease-in fadeout;animation:2s ease-in fadeout;opacity:0}.container-loader .spinner{position:relative;-webkit-animation:2s ease-in fadein;animation:2s ease-in fadein}.container-loader .spinner div{position:absolute;width:20px;height:20px;border-radius:50%;opacity:.7;-webkit-transform:translateX(-30px);transform:translateX(-30px)}.container-loader .spinner div:nth-child(1){background-color:#ffd200;-webkit-animation:1.8s ease-in-out 1.2s infinite spin;animation:1.8s ease-in-out 1.2s infinite spin;height:24px;width:24px}.container-loader .spinner div:nth-child(2){background-color:#58595b;-webkit-animation:1.8s ease-in-out .6s infinite spin;animation:1.8s ease-in-out .6s infinite spin;height:20px;weight:20px}.container-loader .spinner div:nth-child(3){background-color:#00448d;-webkit-animation:1.8s ease-in-out infinite spin;animation:1.8s ease-in-out infinite spin;width:10px;height:10px}@-webkit-keyframes spin{0%{-webkit-transform:translateX(-30px);transform:translateX(-30px)}33.3%{-webkit-transform:translateX(30px);transform:translateX(30px)}66.7%{-webkit-transform:translateX(0) translateY(-45px);transform:translateX(0) translateY(-45px)}100%{-webkit-transform:translateX(-30px) translateY(0);transform:translateX(-30px) translateY(0)}}@keyframes spin{0%{-webkit-transform:translateX(-30px);transform:translateX(-30px)}33.3%{-webkit-transform:translateX(30px);transform:translateX(30px)}66.7%{-webkit-transform:translateX(0) translateY(-45px);transform:translateX(0) translateY(-45px)}100%{-webkit-transform:translateX(-30px) translateY(0);transform:translateX(-30px) translateY(0)}}@-webkit-keyframes fadein{from{opacity:0}to{opacity:1}}@keyframes fadein{from{opacity:0}to{opacity:1}}@-webkit-keyframes fadeout{from{opacity:1}to{opacity:0}}@keyframes fadeout{from{opacity:1}to{opacity:0}}"]
                    },] },
        ];
        /** @nocollapse */
        LoadingComponent.ctorParameters = function () { return []; };
        LoadingComponent.propDecorators = {
            id: [{ type: i0.Input }]
        };
        return LoadingComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ModalComponent = (function () {
        function ModalComponent() {
        }
        /**
         * @return {?}
         */
        ModalComponent.prototype.openModal = /**
         * @return {?}
         */
            function () {
                this.display = 'block';
                document.body.style.overflow = 'hidden';
            };
        /**
         * @return {?}
         */
        ModalComponent.prototype.closeModal = /**
         * @return {?}
         */
            function () {
                this.display = 'none';
                document.body.style.overflow = 'auto';
            };
        ModalComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-modal',
                        template: "<div class=\"modal-container\" [style.display]=display id=\"{{ id }}\">\n  <div class=\"centered\">\n    <div class=\"modal-content\">\n      <div class=\"row modal-header align-items-center\">\n        <div class=\"col-12 text-center\">\n          <h2>{{ titleModal }}</h2>\n        </div>\n        <i routerLink=\"/login\" class=\"fal fa-times close\" (click)=\"closeModal()\" id=\"btn_close\"></i>\n      </div>\n      <div class=\"modal-body text-center\">\n        <i class=\"icon-exit\"></i>\n        <p class=\"session-message\">{{ messageContent }}</p>\n        <div class=\"row justify-content-center actions\">\n          <div class=\"col-5\">\n            <app-button id=\"btn_accept\" (click)=\"closeModal()\" routerLink=\"/login\" type=\"primary-gray\" label=\"{{ labelActionBtn }}\"></app-button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                        styles: ["body{overflow:hidden}.modal-container{display:none;position:fixed;z-index:100;left:0;top:0;width:100%;height:100%;-webkit-backdrop-filter:blur(3px);backdrop-filter:blur(3px);background-color:rgba(247,247,247,.8)}.modal-container .centered{display:table;margin:12% auto}.modal-content{min-width:500px;border-radius:8px;box-shadow:0 2px 6px 0 rgba(0,0,0,.07);background-color:#fff;border:1px solid #f1f1f1;align-items:center}.modal-content .modal-header{width:100%;padding:15px;border-bottom:1px solid rgba(151,151,151,.15);color:#454648;margin:0;position:relative}.modal-content .modal-header .close{position:absolute;right:15px;cursor:pointer;font-size:1.5em;color:#58595b;opacity:.5}.modal-content .modal-header h2{font-size:1.12em;font-weight:700;margin-bottom:0}.modal-content .modal-body{padding:30px 0}.modal-content .modal-body .icon-exit{padding-bottom:30px;content:url(../../../assets/icons/exit.svg)}.modal-content .modal-body .session-message{padding-bottom:30px}@media (max-width:576px){.icon{position:absolute;right:10px;top:-32px}.modal-content{min-width:320px!important}.centered{margin:35% auto}}"]
                    },] },
        ];
        ModalComponent.propDecorators = {
            id: [{ type: i0.Input }],
            titleModal: [{ type: i0.Input }],
            messageContent: [{ type: i0.Input }],
            labelActionBtn: [{ type: i0.Input }],
            iconModalBtn: [{ type: i0.Input }]
        };
        return ModalComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var NavbarComponent = (function () {
        function NavbarComponent() {
            this.date = new Date();
        }
        /**
         * @return {?}
         */
        NavbarComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        NavbarComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-navbar',
                        template: "<div class=\"navbar\">\n  <nav class=\"main-navbar\">\n    <div class=\"nav-container\">\n      <div class=\"row\">\n        <div class=\"col-sm-6\">\n          <img src=\"../../../assets/images/logoBanistmo.svg\">\n        </div>\n        <div class=\"col-sm-6\">\n          <ng-content></ng-content>\n        </div>\n      </div>\n    </div>\n  </nav>\n  <nav class=\"secondary-navbar\">\n    <div class=\"nav-container\">\n      <div class=\"row\">\n        <div class=\"col-sm-6 welcome-text\">\n          Bienvenido a la Sucursal Virtual Personas\n        </div>\n        <div class=\"col-sm-6 date\">\n          \n        </div>\n      </div>\n    </div>\n  </nav>\n</div>\n",
                        styles: [".navbar{width:100%}.navbar .main-navbar{padding:13px 0;background-color:#fff}.navbar .secondary-navbar{padding:4px 0;background-color:#ffd200;min-height:30px;line-height:25px}.navbar .secondary-navbar .welcome-text{font-weight:700;font-size:.88em}.navbar .secondary-navbar .date{font-size:12px;text-align:right}.navbar .nav-container{max-width:942px;margin:auto}.navbar img{height:34px}@media (max-width:576px){.date,.main-navbar,.secondary-navbar{text-align:center!important}}@media (max-width:768px){.nav-container{max-width:100%;padding:0 26px!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.nav-container{padding:0 15px}}"]
                    },] },
        ];
        /** @nocollapse */
        NavbarComponent.ctorParameters = function () { return []; };
        NavbarComponent.propDecorators = {
            content: [{ type: i0.Input }]
        };
        return NavbarComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var NotificationComponent = (function () {
        function NotificationComponent() {
        }
        /**
         * @return {?}
         */
        NotificationComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        NotificationComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-notification',
                        template: "<div class=\"notification {{ type }}\">\n  <div class=\"icon\">\n    <i class=\"{{ icon }}\"></i>\n  </div>\n  <div class=\"content\">\n    {{ message }}\n  </div>\n</div>\n",
                        styles: [".notification{width:auto;height:auto;border-radius:8px;padding:15px;margin-bottom:15px}.notification .icon{float:left;margin-right:15px;font-size:18px}.notification .content{text-align:center}.notification.error{background-color:#fa5e5b;color:#fff}.notification.info{background-color:#00448d;color:#fff}.notification.success{background-color:#16c98d;color:#fff}.notification.warning{background-color:#ffd200;color:#454648}"]
                    },] },
        ];
        /** @nocollapse */
        NotificationComponent.ctorParameters = function () { return []; };
        NotificationComponent.propDecorators = {
            type: [{ type: i0.Input }],
            icon: [{ type: i0.Input }],
            message: [{ type: i0.Input }]
        };
        return NotificationComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var OutlineButtonComponent = (function () {
        function OutlineButtonComponent() {
        }
        /**
         * @return {?}
         */
        OutlineButtonComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        OutlineButtonComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-outline-button',
                        template: "<button class=\"btn-outline\" id=\"{{ id }}\">\n  <i class=\"icon-left {{ icon }}\"></i>\n  <ng-content></ng-content>\n</button>\n",
                        styles: [".btn-outline{width:287px;height:59px;border-radius:29.5px;border:1px solid #3a3b3b;background-color:transparent;font-size:.85em;font-weight:700;padding:0 15px;margin-top:10px;cursor:pointer}.btn-outline:hover{border:2px solid #3a3b3b}.btn-outline .icon-left{font-size:1.8em;width:30px;vertical-align:bottom;line-height:10px}"]
                    },] },
        ];
        /** @nocollapse */
        OutlineButtonComponent.ctorParameters = function () { return []; };
        OutlineButtonComponent.propDecorators = {
            id: [{ type: i0.Input }],
            icon: [{ type: i0.Input }]
        };
        return OutlineButtonComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var PasswordFieldComponent = (function () {
        function PasswordFieldComponent() {
            this.valuePassword = new i0.EventEmitter();
            this.class = '';
        }
        /**
         * @return {?}
         */
        PasswordFieldComponent.prototype.getValuePassword = /**
         * @return {?}
         */
            function () {
                this.valuePassword.emit(this.valueInputPassword);
            };
        /**
         * @return {?}
         */
        PasswordFieldComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.type = 'password';
            };
        /**
         * @return {?}
         */
        PasswordFieldComponent.prototype.validateField = /**
         * @return {?}
         */
            function () {
                this.getValuePassword();
                (this.valueInputPassword !== null && this.valueInputPassword !== '') ? this.emptyInput = 'status' : this.emptyInput = '';
            };
        /**
         * @return {?}
         */
        PasswordFieldComponent.prototype.clearField = /**
         * @return {?}
         */
            function () {
                this.valueInputPassword = '';
                this.state = '';
                this.message = '';
            };
        /**
         * @param {?} event
         * @return {?}
         */
        PasswordFieldComponent.prototype.onSetStateChange = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.state = event.state;
                if (event.state !== '') {
                    this.message = 'No cumple con la longitud mínima';
                }
                else {
                    this.message = '';
                }
            };
        PasswordFieldComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-password-field',
                        template: "<div class=\"text-field {{ state }}\">\n  <div class=\"content\">\n    <label>\n      <input type=\"{{ type }}\" id=\"{{ id }}\" class=\"{{ emptyInput }}\" [(ngModel)]=\"valueInputPassword\" name=\"text-field\" required (change)=\"validateField()\"\n        autocomplete=\"off\" onpaste=\"return false\" onCopy=\"return false\" input-directive [minLength]=8 [maxLength]=16 [pattern]=\"pattern\"\n        (setState)=\"onSetStateChange($event)\">\n      <span>{{ label }}</span>\n      <div class=\"icon\">\n        <i [class]=\"'fal fa-eye' + class\" (mousedown)=\"class='-slash';type='text'\" (mouseup)=\"class='';type='password'\" ></i>\n        <i *ngIf=\"state !== 'error'\" class=\"fal fa-info-circle\"></i>\n        <i *ngIf=\"state === 'error'\" class=\"fal fa-times error\" (click)=\"clearField()\"></i>\n      </div>\n    </label>\n  </div>\n  <div class=\"message\">\n    <p>{{ message }}</p>\n  </div>\n</div>\n",
                        styles: [".text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon .error,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:8px;cursor:pointer}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}"]
                    },] },
        ];
        /** @nocollapse */
        PasswordFieldComponent.ctorParameters = function () { return []; };
        PasswordFieldComponent.propDecorators = {
            valuePassword: [{ type: i0.Output }],
            label: [{ type: i0.Input }],
            state: [{ type: i0.Input }],
            message: [{ type: i0.Input }],
            type: [{ type: i0.Input }],
            pattern: [{ type: i0.Input }],
            icon: [{ type: i0.Input }],
            id: [{ type: i0.Input }]
        };
        return PasswordFieldComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var SecondaryLinkComponent = (function () {
        function SecondaryLinkComponent() {
        }
        /**
         * @return {?}
         */
        SecondaryLinkComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        SecondaryLinkComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-secondary-link',
                        template: "<div class=\"secondary-link\">\n  <i class=\"icon-left {{ icon }}\">\n  </i>\n  <a href=\"\">{{ labelLink }}</a>\n  <i class=\"fas fa-plus icon-right\">\n  </i>\n</div>\n",
                        styles: [".secondary-link{margin-bottom:30px}.secondary-link .icon-left{font-size:2em;float:left;margin-right:20px;width:35px;text-align:center}.secondary-link a{color:#3a3b3b;font-size:.9em;font-weight:700;line-height:32px}.secondary-link a:hover{color:#ffd200;font-weight:700;text-decoration:none}.secondary-link a:focus{color:#3a3b3b}.secondary-link .icon-right{font-weight:700;font-size:.55em;float:right;line-height:32px}"]
                    },] },
        ];
        /** @nocollapse */
        SecondaryLinkComponent.ctorParameters = function () { return []; };
        SecondaryLinkComponent.propDecorators = {
            labelLink: [{ type: i0.Input }],
            icon: [{ type: i0.Input }],
            id: [{ type: i0.Input }]
        };
        return SecondaryLinkComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var SelectComponent = (function () {
        function SelectComponent() {
            this.valueSelect = 0;
        }
        /**
         * @return {?}
         */
        SelectComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @return {?}
         */
        SelectComponent.prototype.validateField = /**
         * @return {?}
         */
            function () {
                (this.valueSelect !== 0) ? this.emptySelect = 'status' : this.emptySelect = '';
            };
        SelectComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-select',
                        template: "<div class=\"text-field {{ state }}\">\n  <div class=\"content\">\n    <label>\n      <select class=\"{{ emptySelect }}\" [(ngModel)]=\"valueSelect\" name=\"select\" required\n              (change)=\"validateField()\" [disabled]=\"(state == 'disabled')\">\n\n        <ng-content></ng-content>\n      </select>\n      <span>Seleccione</span>\n      <div class=\"icon\">\n        <i class=\"{{ icon }}\"></i>\n      </div>\n    </label>\n  </div>\n  <div class=\"message\">\n    <p>{{ message }}</p>\n  </div>\n</div>\n",
                        styles: [".text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content select{position:absolute;top:15px;z-index:2;width:100%;font-size:16px;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px;-moz-appearance:none;-webkit-appearance:none;appearance:none}.text-field .content select:focus{border-bottom:2px solid #ffd200}.text-field .content select:focus+span,.text-field .content select:valid+span{top:6px;cursor:inherit;font-size:13px;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:1;font-size:16px;transition:all .2s ease-in-out;width:100%;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{font-size:18px;position:absolute;z-index:3;right:15px;top:15px}.text-field .message{min-height:20px}.text-field .message p{padding:4px 0 0 15px;font-size:12px;color:#808285;margin:0}"]
                    },] },
        ];
        /** @nocollapse */
        SelectComponent.ctorParameters = function () { return []; };
        SelectComponent.propDecorators = {
            label: [{ type: i0.Input }],
            icon: [{ type: i0.Input }],
            type: [{ type: i0.Input }],
            state: [{ type: i0.Input }],
            message: [{ type: i0.Input }]
        };
        return SelectComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var SubtitleComponent = (function () {
        function SubtitleComponent() {
        }
        /**
         * @return {?}
         */
        SubtitleComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        SubtitleComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-subtitle',
                        template: "<h2>\n  <ng-content></ng-content>\n</h2>\n",
                        styles: ["h2{font-size:17px;font-weight:700;line-height:1.35}"]
                    },] },
        ];
        /** @nocollapse */
        SubtitleComponent.ctorParameters = function () { return []; };
        return SubtitleComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var TabComponent = (function () {
        function TabComponent() {
            this.active = false;
        }
        /**
         * @return {?}
         */
        TabComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        TabComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-tab',
                        template: "<div [hidden]=\"!active\" class=\"pane\">\n  <ng-content></ng-content>\n</div>",
                        styles: [".pane{padding:30px 20px;justify-content:center}"]
                    },] },
        ];
        /** @nocollapse */
        TabComponent.ctorParameters = function () { return []; };
        TabComponent.propDecorators = {
            title: [{ type: i0.Input }],
            id: [{ type: i0.Input }],
            active: [{ type: i0.Input }]
        };
        return TabComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var TabsComponent = (function () {
        function TabsComponent() {
        }
        /**
         * @return {?}
         */
        TabsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @return {?}
         */
        TabsComponent.prototype.ngAfterContentInit = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var activeTabs = this.tabs.filter(function (tab) { return tab.active; });
                // if there is no active tab set, activate the first
                if (activeTabs.length === 0) {
                    this.selectTab(this.tabs.first);
                }
            };
        /**
         * @param {?} tab
         * @return {?}
         */
        TabsComponent.prototype.selectTab = /**
         * @param {?} tab
         * @return {?}
         */
            function (tab) {
                // deactivate all tabs
                this.tabs.toArray().forEach(function (tab) { return tab.active = false; });
                // activate the tab the user has clicked on.
                tab.active = true;
            };
        TabsComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-tabs',
                        template: "<div class=\"nav nav-tabs\">\n  <span *ngFor=\"let tab of tabs\" id=\"{{tab.id}}\" (click)=\"selectTab(tab)\" [class.active]=\"tab.active\" class=\"tab\"\n        ngClass=\"{{position}}\">\n    <span role=\"button\" class=\"tab-title\">{{tab.title}}</span>\n  </span>\n</div>\n<ng-content></ng-content>\n",
                        styles: [".nav-tabs{display:flex;flex-wrap:nowrap;font-size:1em;font-weight:700;text-transform:uppercase;border-bottom:1px solid #e6e7e8}.nav-tabs .tab{width:50%;padding:18px 10px 10px;text-align:center}.nav-tabs .center{flex:1 1;display:flex}.nav-tabs .tab-title{padding:0 7px 5px;cursor:pointer}.nav-tabs .active{border-bottom:solid #ffd200}"]
                    },] },
        ];
        /** @nocollapse */
        TabsComponent.ctorParameters = function () { return []; };
        TabsComponent.propDecorators = {
            position: [{ type: i0.Input }],
            tabs: [{ type: i0.ContentChildren, args: [TabComponent,] }]
        };
        return TabsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var TextFieldComponent = (function () {
        function TextFieldComponent() {
            this.valueUsername = new i0.EventEmitter();
        }
        /**
         * @return {?}
         */
        TextFieldComponent.prototype.getValueUsername = /**
         * @return {?}
         */
            function () {
                this.valueUsername.emit({
                    value: this.valueInput,
                    state: this.inputStyle
                });
            };
        /**
         * @return {?}
         */
        TextFieldComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @return {?}
         */
        TextFieldComponent.prototype.validateField = /**
         * @return {?}
         */
            function () {
                if (this.valueInput !== null && this.valueInput !== '') {
                    this.inputStyle = 'status';
                }
                else {
                    this.inputStyle = '';
                }
                this.getValueUsername();
            };
        /**
         * @param {?} event
         * @return {?}
         */
        TextFieldComponent.prototype.onSetStateChange = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.state = event.state;
                this.inputStyle = event.state;
                if (event.state !== '') {
                    this.message = 'No cumple con la longitud mínima';
                    this.iconSecondary = 'fal fa-times';
                }
                else {
                    this.message = '';
                    this.iconSecondary = '';
                }
                if (this.valueInput === '') {
                    this.message = '';
                    this.state = '';
                    this.iconSecondary = '';
                }
                this.getValueUsername();
            };
        TextFieldComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-text-field',
                        template: "<div class=\"text-field {{ state }}\">\n  <div class=\"content\">\n    <label>\n      <input type=\"text\" id=\"{{ id }}\" class=\"{{ inputStyle }}\" [(ngModel)]=\"valueInput\" name=\"text-field\" [required]=\"true\"\n      input-directive [minLength]=8 [maxLength]=16 [pattern]=\"pattern\" (setState)=\"onSetStateChange($event)\" (keyup)=\"validateField()\" [disabled]=\"(state == 'disabled')\">\n      <span>{{ label }}</span>\n      <div class=\"icon\">\n        <i class=\"{{ iconPrimary }}\"></i>\n        <i class=\"{{ iconSecondary }}\"></i>\n      </div>\n    </label>\n  </div>\n  <div class=\"message\">\n    <p>{{ message }}</p>\n  </div>\n</div>\n",
                        styles: [".text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:15px;cursor:pointer}.text-field .content .icon i:nth-child(2){color:#fa5e5b}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}"],
                    },] },
        ];
        /** @nocollapse */
        TextFieldComponent.ctorParameters = function () { return []; };
        TextFieldComponent.propDecorators = {
            valueUsername: [{ type: i0.Output }],
            label: [{ type: i0.Input }],
            iconPrimary: [{ type: i0.Input }],
            iconSecondary: [{ type: i0.Input }],
            state: [{ type: i0.Input }],
            message: [{ type: i0.Input }],
            pattern: [{ type: i0.Input }],
            id: [{ type: i0.Input }]
        };
        return TextFieldComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var TitleComponent = (function () {
        function TitleComponent() {
        }
        /**
         * @return {?}
         */
        TitleComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        TitleComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-title',
                        template: "<h1>\n  <ng-content></ng-content>\n</h1>\n",
                        styles: ["h1{font-size:24px;font-weight:700;line-height:1.17}"]
                    },] },
        ];
        /** @nocollapse */
        TitleComponent.ctorParameters = function () { return []; };
        return TitleComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var TooltipComponent = (function () {
        function TooltipComponent() {
        }
        /**
         * @return {?}
         */
        TooltipComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        TooltipComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-tooltip',
                        template: "<a  class=\"tooltip\" title=\"{{ title}}\">\n  <i class=\"{{ icon }}\"></i>\n</a>",
                        styles: [".tooltip{display:inline;position:relative}.tooltip:hover:after{background:rgba(0,0,0,.8);border-radius:5px;bottom:26px;color:#fff;content:attr(title);left:-600%;padding:5px 15px;position:absolute;z-index:98;width:220px}.tooltip:hover:before{border:solid;border-color:#333 transparent;border-width:6px 6px 0;bottom:20px;content:\"\";left:0;position:absolute;z-index:99}.tooltip i{font-size:1.2em;letter-spacing:15px;cursor:pointer}"]
                    },] },
        ];
        /** @nocollapse */
        TooltipComponent.ctorParameters = function () { return []; };
        TooltipComponent.propDecorators = {
            title: [{ type: i0.Input }],
            icon: [{ type: i0.Input }]
        };
        return TooltipComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ViewLibModule = (function () {
        function ViewLibModule() {
        }
        ViewLibModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            forms.FormsModule
                        ],
                        declarations: [
                            BannerComponent,
                            ButtonComponent,
                            CardComponent,
                            CarouselBannerComponent,
                            FooterComponent,
                            FooterExternalComponent,
                            HyperlinkComponent,
                            LoadingComponent,
                            ModalComponent,
                            NavbarComponent,
                            NotificationComponent,
                            OutlineButtonComponent,
                            PasswordFieldComponent,
                            SecondaryLinkComponent,
                            SelectComponent,
                            SubtitleComponent,
                            TabComponent,
                            TabsComponent,
                            TextFieldComponent,
                            TitleComponent,
                            TooltipComponent
                        ],
                        exports: [
                            BannerComponent,
                            ButtonComponent,
                            CardComponent,
                            CarouselBannerComponent,
                            FooterComponent,
                            FooterExternalComponent,
                            HyperlinkComponent,
                            LoadingComponent,
                            ModalComponent,
                            NavbarComponent,
                            NotificationComponent,
                            OutlineButtonComponent,
                            PasswordFieldComponent,
                            SecondaryLinkComponent,
                            SelectComponent,
                            SubtitleComponent,
                            TabComponent,
                            TabsComponent,
                            TextFieldComponent,
                            TitleComponent,
                            TooltipComponent
                        ]
                    },] },
        ];
        return ViewLibModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.BannerComponent = BannerComponent;
    exports.ButtonComponent = ButtonComponent;
    exports.CardComponent = CardComponent;
    exports.CarouselBannerComponent = CarouselBannerComponent;
    exports.FooterComponent = FooterComponent;
    exports.FooterExternalComponent = FooterExternalComponent;
    exports.HyperlinkComponent = HyperlinkComponent;
    exports.LoadingComponent = LoadingComponent;
    exports.ModalComponent = ModalComponent;
    exports.NavbarComponent = NavbarComponent;
    exports.NotificationComponent = NotificationComponent;
    exports.OutlineButtonComponent = OutlineButtonComponent;
    exports.PasswordFieldComponent = PasswordFieldComponent;
    exports.SecondaryLinkComponent = SecondaryLinkComponent;
    exports.SelectComponent = SelectComponent;
    exports.SubtitleComponent = SubtitleComponent;
    exports.TabComponent = TabComponent;
    exports.TabsComponent = TabsComponent;
    exports.TextFieldComponent = TextFieldComponent;
    exports.TitleComponent = TitleComponent;
    exports.TooltipComponent = TooltipComponent;
    exports.ViewLibService = ViewLibService;
    exports.ViewLibModule = ViewLibModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1saWIudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly92aWV3LWxpYi9saWIvdmlldy1saWIuc2VydmljZS50cyIsIm5nOi8vdmlldy1saWIvbGliL2Jhbm5lci9iYW5uZXIuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvYnV0dG9uL2J1dHRvbi5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9jYXJkL2NhcmQuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvY2Fyb3VzZWwtYmFubmVyL2Nhcm91c2VsLWJhbm5lci5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL2Zvb3Rlci1leHRlcm5hbC9mb290ZXItZXh0ZXJuYWwuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvaHlwZXJsaW5rL2h5cGVybGluay5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9sb2FkaW5nL2xvYWRpbmcuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvbW9kYWwvbW9kYWwuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvbmF2YmFyL25hdmJhci5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL291dGxpbmUtYnV0dG9uL291dGxpbmUtYnV0dG9uLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3Bhc3N3b3JkLWZpZWxkL3Bhc3N3b3JkLWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3NlY29uZGFyeS1saW5rL3NlY29uZGFyeS1saW5rLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvc3VidGl0bGUvc3VidGl0bGUuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvdGFiL3RhYi5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi90YWJzL3RhYnMuY29tcG9uZW50LnRzIiwibmc6Ly92aWV3LWxpYi9saWIvdGV4dC1maWVsZC90ZXh0LWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3RpdGxlL3RpdGxlLmNvbXBvbmVudC50cyIsIm5nOi8vdmlldy1saWIvbGliL3Rvb2x0aXAvdG9vbHRpcC5jb21wb25lbnQudHMiLCJuZzovL3ZpZXctbGliL2xpYi92aWV3LWxpYi5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVmlld0xpYlNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1iYW5uZXInLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cImJhbm5lclwiPlxyXG4gIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5iYW5uZXJ7d2lkdGg6YXV0bztoZWlnaHQ6YXV0bztib3JkZXItcmFkaXVzOjZweDtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Ym9yZGVyOjEuNXB4IHNvbGlkICNmZmQyMDA7cGFkZGluZzoxNXB4O292ZXJmbG93OmhpZGRlbn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQmFubmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWJ1dHRvbicsXHJcbiAgdGVtcGxhdGU6IGA8YnV0dG9uIGNsYXNzPVwiYnRuIHt7IHR5cGUgfX1cIj5cclxuICB7eyBsYWJlbCB9fVxyXG48L2J1dHRvbj5cclxuYCxcclxuICBzdHlsZXM6IFtgLmJ0bnt3aWR0aDoxNDRweDtoZWlnaHQ6NDBweDtib3JkZXItcmFkaXVzOjIwcHg7Zm9udC1zaXplOjE0cHg7Zm9udC13ZWlnaHQ6NzAwO2JvcmRlcjowO3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZX0uYnRuLnByaW1hcnktZ3JheXtiYWNrZ3JvdW5kLWNvbG9yOiM1ODU5NWI7Y29sb3I6I2ZmZn0uYnRuLnByaW1hcnktZ3JheTpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiMzNjM2MzZ9LmJ0bi5wcmltYXJ5LWdyYXk6Zm9jdXN7YmFja2dyb3VuZC1jb2xvcjojNTg1OTVifS5idG4ucHJpbWFyeS15ZWxsb3d7YmFja2dyb3VuZC1jb2xvcjojZmZkMjAwO2NvbG9yOiM0NTQ2NDh9LmJ0bi5wcmltYXJ5LXllbGxvdzpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiNmZmI1MDB9LmJ0bi5wcmltYXJ5LXllbGxvdzpmb2N1c3tiYWNrZ3JvdW5kLWNvbG9yOiNmZmQyMDB9LmJ0bi5wcmltYXJ5LXJlZHtiYWNrZ3JvdW5kLWNvbG9yOiNmYTVlNWI7Y29sb3I6I2ZmZn0uYnRuLnByaW1hcnktcmVkOmhvdmVye2JhY2tncm91bmQtY29sb3I6I2QxNGQ0YX0uYnRuLnByaW1hcnktcmVkOmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2ZhNWU1Yn0uYnRuLndoaXRle2JhY2tncm91bmQtY29sb3I6I2ZmZjtjb2xvcjojNDU0NjQ4fS5idG4ud2hpdGU6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtib3JkZXI6MXB4IHNvbGlkICNmZmZ9LmJ0bi53aGl0ZTpmb2N1c3tiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Y29sb3I6IzQ1NDY0OH0uYnRuLnNlY29uZGFyeXtib3JkZXI6MXB4IHNvbGlkICMwMDQ0OGQ7YmFja2dyb3VuZC1jb2xvcjojZmZmO2NvbG9yOiMwMDQ0OGR9LmJ0bi5zZWNvbmRhcnk6aG92ZXJ7Ym9yZGVyLXdpZHRoOjJweH0uYnRuLnNlY29uZGFyeTpmb2N1c3tiYWNrZ3JvdW5kLWNvbG9yOiNlNmU3ZTh9LmJ0bi5mbGF0e2JvcmRlci1yYWRpdXM6MDtiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2NvbG9yOiMzMzN9LmJ0bi5mbGF0OmhvdmVye2NvbG9yOiNmZmQyMDB9LmJ0bi5mbGF0OmZvY3Vze2NvbG9yOiM1ODU5NWJ9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHR5cGU6IHN0cmluZztcclxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1jYXJkJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLmNhcmR7YmFja2dyb3VuZC1jb2xvcjojZmZmO2JvcmRlci1yYWRpdXM6OHB4O2JvcmRlcjoxcHggc29saWQgI2YxZjFmMTtib3gtc2hhZG93OjAgMnB4IDZweCAwIHJnYmEoMCwwLDAsLjA3KTt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO292ZXJmbG93OmhpZGRlbn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWNhcm91c2VsLWJhbm5lcicsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwibWFpbi1jb250YWluZXJcIj5cclxuICA8ZGl2IGNsYXNzPVwiY2Fyb3VzZWwtYmFubmVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiYmFubmVyLWNvbnRhaW5lclwiPlxyXG4gICAgICA8YSBocmVmPVwiXCI+PGltZyBjbGFzcz1cImltZy1mbHVpZFwiIHNyYz1cIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvYml0bWFwLnBuZ1wiPjwvYT5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLm1haW4tY29udGFpbmVye3dpZHRoOjEwMCV9Lm1haW4tY29udGFpbmVyIC5jYXJvdXNlbC1iYW5uZXJ7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4O2hlaWdodDphdXRvfS5tYWluLWNvbnRhaW5lciAuY2Fyb3VzZWwtYmFubmVyIC5iYW5uZXItY29udGFpbmVye21heC13aWR0aDo5NDJweDttYXJnaW46YXV0bzt0ZXh0LWFsaWduOnJpZ2h0fUBtZWRpYSAobWF4LXdpZHRoOjc2OHB4KXsuY2Fyb3VzZWwtYmFubmVye2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQhaW1wb3J0YW50fS5jYXJvdXNlbC1iYW5uZXIgLmJhbm5lci1jb250YWluZXJ7bWF4LXdpZHRoOjEwMCU7cGFkZGluZzowIDI2cHghaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyIWltcG9ydGFudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3h9fUBtZWRpYSAobWF4LXdpZHRoOjk5MnB4KXsuYmFubmVyLWNvbnRhaW5lcntwYWRkaW5nOjAgMTVweH19YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENhcm91c2VsQmFubmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWZvb3RlcicsXHJcbiAgdGVtcGxhdGU6IGA8Zm9vdGVyPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWN0LWluZm9ybWF0aW9uIHRleHQtY2VudGVyXCI+XHJcbiAgICA8ZGl2PlxyXG4gICAgICA8aSBjbGFzcz1cImZvb3Rlci1pY29uIGZhbCBmYS1waG9uZVwiPjwvaT5cclxuICAgICAgU3VjdXJzYWwgVGVsZWbDg8KzbmljYSA6IDUwNy0zMDYtNDcwMFxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2PkNvcHlyaWdodCBCYW5pc3RtbyBTQS4gMjAxODwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Zvb3Rlcj5cclxuXHJcblxyXG5cclxuYCxcclxuICBzdHlsZXM6IFtgZm9vdGVye3dpZHRoOjEwMCU7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4O3BhZGRpbmc6MjVweCAwfWZvb3RlciAuY29udGFjdC1pbmZvcm1hdGlvbntmb250LXNpemU6LjhlbX1mb290ZXIgLmNvbnRhY3QtaW5mb3JtYXRpb24gaXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLTEpO3RyYW5zZm9ybTpzY2FsZVgoLTEpfWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb290ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1mb290ZXItZXh0ZXJuYWwnLFxyXG4gIHRlbXBsYXRlOiBgPGZvb3Rlcj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93IGp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbCBjb2wtbGctMlwiPlxyXG4gICAgICAgIDxhIGhyZWY9XCJcIiBpZD1cImluZm9fc2NoZWR1bGVzXCI+SG9yYXJpb3M8L2E+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sIGNvbC1sZy0yXCI+XHJcbiAgICAgICAgPGEgaHJlZj1cIlwiIGlkPVwiaW5mb19jb21taXNzaW9uc1wiPkNvbWlzaW9uZXM8L2E+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sIGNvbC1sZy0yXCI+XHJcbiAgICAgICAgPGEgaHJlZj1cIlwiIGlkPVwiaW5mb19zZWN1cml0eVwiPlNlZ3VyaWRhZDwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC9mb290ZXI+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYGZvb3Rlcnt3aWR0aDoxMDAlO2JhY2tncm91bmQtY29sb3I6I2U2ZTdlODtwYWRkaW5nOjI1cHggMCAwfWZvb3RlciBhe2NvbG9yOiM0NTQ2NDg7Zm9udC1zaXplOi44ZW07bGluZS1oZWlnaHQ6MzRweH1mb290ZXIgYTpob3Zlcntmb250LXdlaWdodDo3MDA7dGV4dC1kZWNvcmF0aW9uOm5vbmV9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvb3RlckV4dGVybmFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1oeXBlcmxpbmsnLFxyXG4gIHRlbXBsYXRlOiBgPGEgaHJlZj1cInt7IHVybCB9fVwiPlxyXG4gIHt7IGxhYmVsIH19XHJcbjwvYT5cclxuYCxcclxuICBzdHlsZXM6IFtgYXtmb250LXNpemU6Ljg1ZW07Y29sb3I6IzAwNDQ4ZDt0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lfWE6aG92ZXJ7Zm9udC13ZWlnaHQ6NzAwfWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIeXBlcmxpbmtDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgdXJsOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1sb2FkaW5nJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJibG9ja2luZy1iYWNrZ3JvdW5kXCI+PC9kaXY+XHJcbjxkaXYgY2xhc3M9XCJjb250YWluZXItbG9hZGVyXCIgaWQ9XCJ7eyBpZCB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJsb2FkZXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJzcGlubmVyXCI+XHJcbiAgICAgIDxkaXY+PC9kaXY+XHJcbiAgICAgIDxkaXY+PC9kaXY+XHJcbiAgICAgIDxkaXY+PC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5ibG9ja2luZy1iYWNrZ3JvdW5ke2JhY2tncm91bmQtY29sb3I6cmdiYSgyNTUsMjU1LDI1NSwuNCk7cG9zaXRpb246Zml4ZWQ7dG9wOjA7bGVmdDowO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7ei1pbmRleDo0MDB9LmNvbnRhaW5lci1sb2FkZXJ7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDI1NSwyNTUsMjU1LC43KTtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtsZWZ0OjA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTt6LWluZGV4OjQwMDtib3JkZXItcmFkaXVzOjhweH0uY29udGFpbmVyLWxvYWRlciAubG9hZGVye2Rpc3BsYXk6YmxvY2s7cG9zaXRpb246cmVsYXRpdmU7bGVmdDo1MCU7dG9wOjUwJTt6LWluZGV4OjUwMH0uY29udGFpbmVyLWxvYWRlcltsb2FkZWRdIC5zcGlubmVyey13ZWJraXQtYW5pbWF0aW9uOjJzIGVhc2UtaW4gZmFkZW91dDthbmltYXRpb246MnMgZWFzZS1pbiBmYWRlb3V0O29wYWNpdHk6MH0uY29udGFpbmVyLWxvYWRlciAuc3Bpbm5lcntwb3NpdGlvbjpyZWxhdGl2ZTstd2Via2l0LWFuaW1hdGlvbjoycyBlYXNlLWluIGZhZGVpbjthbmltYXRpb246MnMgZWFzZS1pbiBmYWRlaW59LmNvbnRhaW5lci1sb2FkZXIgLnNwaW5uZXIgZGl2e3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjIwcHg7aGVpZ2h0OjIwcHg7Ym9yZGVyLXJhZGl1czo1MCU7b3BhY2l0eTouNzstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCl9LmNvbnRhaW5lci1sb2FkZXIgLnNwaW5uZXIgZGl2Om50aC1jaGlsZCgxKXtiYWNrZ3JvdW5kLWNvbG9yOiNmZmQyMDA7LXdlYmtpdC1hbmltYXRpb246MS44cyBlYXNlLWluLW91dCAxLjJzIGluZmluaXRlIHNwaW47YW5pbWF0aW9uOjEuOHMgZWFzZS1pbi1vdXQgMS4ycyBpbmZpbml0ZSBzcGluO2hlaWdodDoyNHB4O3dpZHRoOjI0cHh9LmNvbnRhaW5lci1sb2FkZXIgLnNwaW5uZXIgZGl2Om50aC1jaGlsZCgyKXtiYWNrZ3JvdW5kLWNvbG9yOiM1ODU5NWI7LXdlYmtpdC1hbmltYXRpb246MS44cyBlYXNlLWluLW91dCAuNnMgaW5maW5pdGUgc3BpbjthbmltYXRpb246MS44cyBlYXNlLWluLW91dCAuNnMgaW5maW5pdGUgc3BpbjtoZWlnaHQ6MjBweDt3ZWlnaHQ6MjBweH0uY29udGFpbmVyLWxvYWRlciAuc3Bpbm5lciBkaXY6bnRoLWNoaWxkKDMpe2JhY2tncm91bmQtY29sb3I6IzAwNDQ4ZDstd2Via2l0LWFuaW1hdGlvbjoxLjhzIGVhc2UtaW4tb3V0IGluZmluaXRlIHNwaW47YW5pbWF0aW9uOjEuOHMgZWFzZS1pbi1vdXQgaW5maW5pdGUgc3Bpbjt3aWR0aDoxMHB4O2hlaWdodDoxMHB4fUAtd2Via2l0LWtleWZyYW1lcyBzcGluezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KX0zMy4zJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDMwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDMwcHgpfTY2Ljcley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMCkgdHJhbnNsYXRlWSgtNDVweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMCkgdHJhbnNsYXRlWSgtNDVweCl9MTAwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KSB0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KSB0cmFuc2xhdGVZKDApfX1Aa2V5ZnJhbWVzIHNwaW57MCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpfTMzLjMley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMzBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMzBweCl9NjYuNyV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgwKSB0cmFuc2xhdGVZKC00NXB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgwKSB0cmFuc2xhdGVZKC00NXB4KX0xMDAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpIHRyYW5zbGF0ZVkoMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpIHRyYW5zbGF0ZVkoMCl9fUAtd2Via2l0LWtleWZyYW1lcyBmYWRlaW57ZnJvbXtvcGFjaXR5OjB9dG97b3BhY2l0eToxfX1Aa2V5ZnJhbWVzIGZhZGVpbntmcm9te29wYWNpdHk6MH10b3tvcGFjaXR5OjF9fUAtd2Via2l0LWtleWZyYW1lcyBmYWRlb3V0e2Zyb217b3BhY2l0eToxfXRve29wYWNpdHk6MH19QGtleWZyYW1lcyBmYWRlb3V0e2Zyb217b3BhY2l0eToxfXRve29wYWNpdHk6MH19YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIExvYWRpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLW1vZGFsJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJtb2RhbC1jb250YWluZXJcIiBbc3R5bGUuZGlzcGxheV09ZGlzcGxheSBpZD1cInt7IGlkIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImNlbnRlcmVkXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwicm93IG1vZGFsLWhlYWRlciBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLTEyIHRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICA8aDI+e3sgdGl0bGVNb2RhbCB9fTwvaDI+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGkgcm91dGVyTGluaz1cIi9sb2dpblwiIGNsYXNzPVwiZmFsIGZhLXRpbWVzIGNsb3NlXCIgKGNsaWNrKT1cImNsb3NlTW9kYWwoKVwiIGlkPVwiYnRuX2Nsb3NlXCI+PC9pPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHkgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgICA8aSBjbGFzcz1cImljb24tZXhpdFwiPjwvaT5cclxuICAgICAgICA8cCBjbGFzcz1cInNlc3Npb24tbWVzc2FnZVwiPnt7IG1lc3NhZ2VDb250ZW50IH19PC9wPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3cganVzdGlmeS1jb250ZW50LWNlbnRlciBhY3Rpb25zXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLTVcIj5cclxuICAgICAgICAgICAgPGFwcC1idXR0b24gaWQ9XCJidG5fYWNjZXB0XCIgKGNsaWNrKT1cImNsb3NlTW9kYWwoKVwiIHJvdXRlckxpbms9XCIvbG9naW5cIiB0eXBlPVwicHJpbWFyeS1ncmF5XCIgbGFiZWw9XCJ7eyBsYWJlbEFjdGlvbkJ0biB9fVwiPjwvYXBwLWJ1dHRvbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgYm9keXtvdmVyZmxvdzpoaWRkZW59Lm1vZGFsLWNvbnRhaW5lcntkaXNwbGF5Om5vbmU7cG9zaXRpb246Zml4ZWQ7ei1pbmRleDoxMDA7bGVmdDowO3RvcDowO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7LXdlYmtpdC1iYWNrZHJvcC1maWx0ZXI6Ymx1cigzcHgpO2JhY2tkcm9wLWZpbHRlcjpibHVyKDNweCk7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDI0NywyNDcsMjQ3LC44KX0ubW9kYWwtY29udGFpbmVyIC5jZW50ZXJlZHtkaXNwbGF5OnRhYmxlO21hcmdpbjoxMiUgYXV0b30ubW9kYWwtY29udGVudHttaW4td2lkdGg6NTAwcHg7Ym9yZGVyLXJhZGl1czo4cHg7Ym94LXNoYWRvdzowIDJweCA2cHggMCByZ2JhKDAsMCwwLC4wNyk7YmFja2dyb3VuZC1jb2xvcjojZmZmO2JvcmRlcjoxcHggc29saWQgI2YxZjFmMTthbGlnbi1pdGVtczpjZW50ZXJ9Lm1vZGFsLWNvbnRlbnQgLm1vZGFsLWhlYWRlcnt3aWR0aDoxMDAlO3BhZGRpbmc6MTVweDtib3JkZXItYm90dG9tOjFweCBzb2xpZCByZ2JhKDE1MSwxNTEsMTUxLC4xNSk7Y29sb3I6IzQ1NDY0ODttYXJnaW46MDtwb3NpdGlvbjpyZWxhdGl2ZX0ubW9kYWwtY29udGVudCAubW9kYWwtaGVhZGVyIC5jbG9zZXtwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDoxNXB4O2N1cnNvcjpwb2ludGVyO2ZvbnQtc2l6ZToxLjVlbTtjb2xvcjojNTg1OTViO29wYWNpdHk6LjV9Lm1vZGFsLWNvbnRlbnQgLm1vZGFsLWhlYWRlciBoMntmb250LXNpemU6MS4xMmVtO2ZvbnQtd2VpZ2h0OjcwMDttYXJnaW4tYm90dG9tOjB9Lm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHl7cGFkZGluZzozMHB4IDB9Lm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgLmljb24tZXhpdHtwYWRkaW5nLWJvdHRvbTozMHB4O2NvbnRlbnQ6dXJsKC4uLy4uLy4uL2Fzc2V0cy9pY29ucy9leGl0LnN2Zyl9Lm1vZGFsLWNvbnRlbnQgLm1vZGFsLWJvZHkgLnNlc3Npb24tbWVzc2FnZXtwYWRkaW5nLWJvdHRvbTozMHB4fUBtZWRpYSAobWF4LXdpZHRoOjU3NnB4KXsuaWNvbntwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDoxMHB4O3RvcDotMzJweH0ubW9kYWwtY29udGVudHttaW4td2lkdGg6MzIwcHghaW1wb3J0YW50fS5jZW50ZXJlZHttYXJnaW46MzUlIGF1dG99fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNb2RhbENvbXBvbmVudCB7XHJcblxyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgdGl0bGVNb2RhbDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2VDb250ZW50OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbGFiZWxBY3Rpb25CdG46IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uTW9kYWxCdG46IHN0cmluZztcclxuXHJcbiAgZGlzcGxheTogc3RyaW5nO1xyXG5cclxuICBvcGVuTW9kYWwoKSB7XHJcbiAgICB0aGlzLmRpc3BsYXkgPSAnYmxvY2snO1xyXG4gICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xyXG4gIH1cclxuXHJcbiAgY2xvc2VNb2RhbCgpIHtcclxuICAgIHRoaXMuZGlzcGxheSA9ICdub25lJztcclxuICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSAnYXV0byc7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1uYXZiYXInLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cIm5hdmJhclwiPlxyXG4gIDxuYXYgY2xhc3M9XCJtYWluLW5hdmJhclwiPlxyXG4gICAgPGRpdiBjbGFzcz1cIm5hdi1jb250YWluZXJcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNlwiPlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIuLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2xvZ29CYW5pc3Rtby5zdmdcIj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTZcIj5cclxuICAgICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L25hdj5cclxuICA8bmF2IGNsYXNzPVwic2Vjb25kYXJ5LW5hdmJhclwiPlxyXG4gICAgPGRpdiBjbGFzcz1cIm5hdi1jb250YWluZXJcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNiB3ZWxjb21lLXRleHRcIj5cclxuICAgICAgICAgIEJpZW52ZW5pZG8gYSBsYSBTdWN1cnNhbCBWaXJ0dWFsIFBlcnNvbmFzXHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02IGRhdGVcIj5cclxuICAgICAgICAgIFxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvbmF2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLm5hdmJhcnt3aWR0aDoxMDAlfS5uYXZiYXIgLm1haW4tbmF2YmFye3BhZGRpbmc6MTNweCAwO2JhY2tncm91bmQtY29sb3I6I2ZmZn0ubmF2YmFyIC5zZWNvbmRhcnktbmF2YmFye3BhZGRpbmc6NHB4IDA7YmFja2dyb3VuZC1jb2xvcjojZmZkMjAwO21pbi1oZWlnaHQ6MzBweDtsaW5lLWhlaWdodDoyNXB4fS5uYXZiYXIgLnNlY29uZGFyeS1uYXZiYXIgLndlbGNvbWUtdGV4dHtmb250LXdlaWdodDo3MDA7Zm9udC1zaXplOi44OGVtfS5uYXZiYXIgLnNlY29uZGFyeS1uYXZiYXIgLmRhdGV7Zm9udC1zaXplOjEycHg7dGV4dC1hbGlnbjpyaWdodH0ubmF2YmFyIC5uYXYtY29udGFpbmVye21heC13aWR0aDo5NDJweDttYXJnaW46YXV0b30ubmF2YmFyIGltZ3toZWlnaHQ6MzRweH1AbWVkaWEgKG1heC13aWR0aDo1NzZweCl7LmRhdGUsLm1haW4tbmF2YmFyLC5zZWNvbmRhcnktbmF2YmFye3RleHQtYWxpZ246Y2VudGVyIWltcG9ydGFudH19QG1lZGlhIChtYXgtd2lkdGg6NzY4cHgpey5uYXYtY29udGFpbmVye21heC13aWR0aDoxMDAlO3BhZGRpbmc6MCAyNnB4IWltcG9ydGFudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3h9fUBtZWRpYSAobWF4LXdpZHRoOjk5MnB4KXsubmF2LWNvbnRhaW5lcntwYWRkaW5nOjAgMTVweH19YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmJhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgZGF0ZSA9IG5ldyBEYXRlKCk7XHJcblxyXG4gIEBJbnB1dCgpIGNvbnRlbnQ6IGFueTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1ub3RpZmljYXRpb24nLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cIm5vdGlmaWNhdGlvbiB7eyB0eXBlIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImljb25cIj5cclxuICAgIDxpIGNsYXNzPVwie3sgaWNvbiB9fVwiPjwvaT5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxyXG4gICAge3sgbWVzc2FnZSB9fVxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLm5vdGlmaWNhdGlvbnt3aWR0aDphdXRvO2hlaWdodDphdXRvO2JvcmRlci1yYWRpdXM6OHB4O3BhZGRpbmc6MTVweDttYXJnaW4tYm90dG9tOjE1cHh9Lm5vdGlmaWNhdGlvbiAuaWNvbntmbG9hdDpsZWZ0O21hcmdpbi1yaWdodDoxNXB4O2ZvbnQtc2l6ZToxOHB4fS5ub3RpZmljYXRpb24gLmNvbnRlbnR7dGV4dC1hbGlnbjpjZW50ZXJ9Lm5vdGlmaWNhdGlvbi5lcnJvcntiYWNrZ3JvdW5kLWNvbG9yOiNmYTVlNWI7Y29sb3I6I2ZmZn0ubm90aWZpY2F0aW9uLmluZm97YmFja2dyb3VuZC1jb2xvcjojMDA0NDhkO2NvbG9yOiNmZmZ9Lm5vdGlmaWNhdGlvbi5zdWNjZXNze2JhY2tncm91bmQtY29sb3I6IzE2Yzk4ZDtjb2xvcjojZmZmfS5ub3RpZmljYXRpb24ud2FybmluZ3tiYWNrZ3JvdW5kLWNvbG9yOiNmZmQyMDA7Y29sb3I6IzQ1NDY0OH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtb3V0bGluZS1idXR0b24nLFxyXG4gIHRlbXBsYXRlOiBgPGJ1dHRvbiBjbGFzcz1cImJ0bi1vdXRsaW5lXCIgaWQ9XCJ7eyBpZCB9fVwiPlxyXG4gIDxpIGNsYXNzPVwiaWNvbi1sZWZ0IHt7IGljb24gfX1cIj48L2k+XHJcbiAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG48L2J1dHRvbj5cclxuYCxcclxuICBzdHlsZXM6IFtgLmJ0bi1vdXRsaW5le3dpZHRoOjI4N3B4O2hlaWdodDo1OXB4O2JvcmRlci1yYWRpdXM6MjkuNXB4O2JvcmRlcjoxcHggc29saWQgIzNhM2IzYjtiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2ZvbnQtc2l6ZTouODVlbTtmb250LXdlaWdodDo3MDA7cGFkZGluZzowIDE1cHg7bWFyZ2luLXRvcDoxMHB4O2N1cnNvcjpwb2ludGVyfS5idG4tb3V0bGluZTpob3Zlcntib3JkZXI6MnB4IHNvbGlkICMzYTNiM2J9LmJ0bi1vdXRsaW5lIC5pY29uLWxlZnR7Zm9udC1zaXplOjEuOGVtO3dpZHRoOjMwcHg7dmVydGljYWwtYWxpZ246Ym90dG9tO2xpbmUtaGVpZ2h0OjEwcHh9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE91dGxpbmVCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtcGFzc3dvcmQtZmllbGQnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cInRleHQtZmllbGQge3sgc3RhdGUgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxyXG4gICAgPGxhYmVsPlxyXG4gICAgICA8aW5wdXQgdHlwZT1cInt7IHR5cGUgfX1cIiBpZD1cInt7IGlkIH19XCIgY2xhc3M9XCJ7eyBlbXB0eUlucHV0IH19XCIgWyhuZ01vZGVsKV09XCJ2YWx1ZUlucHV0UGFzc3dvcmRcIiBuYW1lPVwidGV4dC1maWVsZFwiIHJlcXVpcmVkIChjaGFuZ2UpPVwidmFsaWRhdGVGaWVsZCgpXCJcclxuICAgICAgICBhdXRvY29tcGxldGU9XCJvZmZcIiBvbnBhc3RlPVwicmV0dXJuIGZhbHNlXCIgb25Db3B5PVwicmV0dXJuIGZhbHNlXCIgaW5wdXQtZGlyZWN0aXZlIFttaW5MZW5ndGhdPTggW21heExlbmd0aF09MTYgW3BhdHRlcm5dPVwicGF0dGVyblwiXHJcbiAgICAgICAgKHNldFN0YXRlKT1cIm9uU2V0U3RhdGVDaGFuZ2UoJGV2ZW50KVwiPlxyXG4gICAgICA8c3Bhbj57eyBsYWJlbCB9fTwvc3Bhbj5cclxuICAgICAgPGRpdiBjbGFzcz1cImljb25cIj5cclxuICAgICAgICA8aSBbY2xhc3NdPVwiJ2ZhbCBmYS1leWUnICsgY2xhc3NcIiAobW91c2Vkb3duKT1cImNsYXNzPSctc2xhc2gnO3R5cGU9J3RleHQnXCIgKG1vdXNldXApPVwiY2xhc3M9Jyc7dHlwZT0ncGFzc3dvcmQnXCIgPjwvaT5cclxuICAgICAgICA8aSAqbmdJZj1cInN0YXRlICE9PSAnZXJyb3InXCIgY2xhc3M9XCJmYWwgZmEtaW5mby1jaXJjbGVcIj48L2k+XHJcbiAgICAgICAgPGkgKm5nSWY9XCJzdGF0ZSA9PT0gJ2Vycm9yJ1wiIGNsYXNzPVwiZmFsIGZhLXRpbWVzIGVycm9yXCIgKGNsaWNrKT1cImNsZWFyRmllbGQoKVwiPjwvaT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2xhYmVsPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJtZXNzYWdlXCI+XHJcbiAgICA8cD57eyBtZXNzYWdlIH19PC9wPlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLnRleHQtZmllbGR7bWFyZ2luLWJvdHRvbToxMHB4fS50ZXh0LWZpZWxkLmRpc2FibGVke29wYWNpdHk6LjN9LnRleHQtZmllbGQuZGlzYWJsZWQgLmNvbnRlbnQsLnRleHQtZmllbGQuZGlzYWJsZWQgaW5wdXQsLnRleHQtZmllbGQuZGlzYWJsZWQgbGFiZWwsLnRleHQtZmllbGQuZGlzYWJsZWQgc3BhbntjdXJzb3I6bm90LWFsbG93ZWQhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIC5pY29uIC5lcnJvciwudGV4dC1maWVsZC5lcnJvciBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkLmVycm9yIHAsLnRleHQtZmllbGQuZXJyb3Igc3Bhbntjb2xvcjojZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciBpbnB1dHtib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZCAuY29udGVudHt3aWR0aDoxMDAlO2NvbG9yOiM0NTQ2NDg7YmFja2dyb3VuZC1jb2xvcjojZjdmN2Y3O2JvcmRlci1yYWRpdXM6NnB4IDZweCAwIDB9LnRleHQtZmllbGQgLmNvbnRlbnQgbGFiZWx7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt3aWR0aDoxMDAlO21pbi1oZWlnaHQ6NTBweH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTVweDt6LWluZGV4OjE7d2lkdGg6MTAwJTtmb250LXNpemU6MWVtO2JvcmRlcjowO2JvcmRlci1ib3R0b206MXB4IHNvbGlkICM4MDgyODU7dHJhbnNpdGlvbjpib3JkZXItY29sb3IgLjJzIGVhc2UtaW4tb3V0O291dGxpbmU6MDtoZWlnaHQ6MzVweDtiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dDpmb2N1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmZkMjAwfS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6dmFsaWQrc3Bhbnt0b3A6NnB4O2N1cnNvcjppbmhlcml0O2ZvbnQtc2l6ZTouOGVtO2NvbG9yOiM4MDgyODU7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLnN0YXR1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjMDA0NDhkfS50ZXh0LWZpZWxkIC5jb250ZW50IHNwYW57cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MjBweDt6LWluZGV4OjI7Zm9udC1zaXplOjFlbTt0cmFuc2l0aW9uOmFsbCAuMnMgZWFzZS1pbi1vdXQ7d2lkdGg6MTAwJTtjdXJzb3I6dGV4dDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb257ei1pbmRleDozO21hcmdpbjoxNXB4IDAgMDtyaWdodDowO3Bvc2l0aW9uOmFic29sdXRlfS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29uIGl7Zm9udC1zaXplOjEuMmVtO2xldHRlci1zcGFjaW5nOjhweDtjdXJzb3I6cG9pbnRlcn0udGV4dC1maWVsZCAubWVzc2FnZXttaW4taGVpZ2h0OjIwcHh9LnRleHQtZmllbGQgLm1lc3NhZ2UgcHtwYWRkaW5nOjAgMCAwIDE1cHg7Zm9udC1zaXplOi44ZW07Y29sb3I6IzgwODI4NTttYXJnaW46LTNweCAwIDB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBhc3N3b3JkRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICB2YWx1ZUlucHV0UGFzc3dvcmQ6IGFueTtcclxuICBlbXB0eUlucHV0OiBzdHJpbmc7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyB2YWx1ZVBhc3N3b3JkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgc3RhdGU6IHN0cmluZztcclxuICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHBhdHRlcm46IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuXHJcbiAgcHVibGljIGNsYXNzID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICB9XHJcbiAgZ2V0VmFsdWVQYXNzd29yZCgpIHtcclxuICAgIHRoaXMudmFsdWVQYXNzd29yZC5lbWl0KHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy50eXBlID0gJ3Bhc3N3b3JkJztcclxuXHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZpZWxkKCkge1xyXG4gICAgdGhpcy5nZXRWYWx1ZVBhc3N3b3JkKCk7XHJcbiAgICh0aGlzLnZhbHVlSW5wdXRQYXNzd29yZCAhPT0gbnVsbCAmJiB0aGlzLnZhbHVlSW5wdXRQYXNzd29yZCAhPT0gJycpID8gdGhpcy5lbXB0eUlucHV0ID0gJ3N0YXR1cycgOiB0aGlzLmVtcHR5SW5wdXQgPSAnJztcclxuICB9XHJcbiAgY2xlYXJGaWVsZCgpIHtcclxuICAgIHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkID0gJyc7XHJcbiAgICB0aGlzLnN0YXRlID0gJyc7XHJcbiAgICB0aGlzLm1lc3NhZ2UgPSAnJztcclxuICB9XHJcblxyXG4gIG9uU2V0U3RhdGVDaGFuZ2UoZXZlbnQpe1xyXG4gICAgdGhpcy5zdGF0ZT1ldmVudC5zdGF0ZTtcclxuICAgIGlmKGV2ZW50LnN0YXRlICE9PSAnJyl7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICdObyBjdW1wbGUgY29uIGxhIGxvbmdpdHVkIG3Dg8KtbmltYSdcclxuICAgIH0gZWxzZXtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXNlY29uZGFyeS1saW5rJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJzZWNvbmRhcnktbGlua1wiPlxyXG4gIDxpIGNsYXNzPVwiaWNvbi1sZWZ0IHt7IGljb24gfX1cIj5cclxuICA8L2k+XHJcbiAgPGEgaHJlZj1cIlwiPnt7IGxhYmVsTGluayB9fTwvYT5cclxuICA8aSBjbGFzcz1cImZhcyBmYS1wbHVzIGljb24tcmlnaHRcIj5cclxuICA8L2k+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2Auc2Vjb25kYXJ5LWxpbmt7bWFyZ2luLWJvdHRvbTozMHB4fS5zZWNvbmRhcnktbGluayAuaWNvbi1sZWZ0e2ZvbnQtc2l6ZToyZW07ZmxvYXQ6bGVmdDttYXJnaW4tcmlnaHQ6MjBweDt3aWR0aDozNXB4O3RleHQtYWxpZ246Y2VudGVyfS5zZWNvbmRhcnktbGluayBhe2NvbG9yOiMzYTNiM2I7Zm9udC1zaXplOi45ZW07Zm9udC13ZWlnaHQ6NzAwO2xpbmUtaGVpZ2h0OjMycHh9LnNlY29uZGFyeS1saW5rIGE6aG92ZXJ7Y29sb3I6I2ZmZDIwMDtmb250LXdlaWdodDo3MDA7dGV4dC1kZWNvcmF0aW9uOm5vbmV9LnNlY29uZGFyeS1saW5rIGE6Zm9jdXN7Y29sb3I6IzNhM2IzYn0uc2Vjb25kYXJ5LWxpbmsgLmljb24tcmlnaHR7Zm9udC13ZWlnaHQ6NzAwO2ZvbnQtc2l6ZTouNTVlbTtmbG9hdDpyaWdodDtsaW5lLWhlaWdodDozMnB4fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWNvbmRhcnlMaW5rQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBsYWJlbExpbms6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBcclxuICBcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXNlbGVjdCcsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwidGV4dC1maWVsZCB7eyBzdGF0ZSB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICA8bGFiZWw+XHJcbiAgICAgIDxzZWxlY3QgY2xhc3M9XCJ7eyBlbXB0eVNlbGVjdCB9fVwiIFsobmdNb2RlbCldPVwidmFsdWVTZWxlY3RcIiBuYW1lPVwic2VsZWN0XCIgcmVxdWlyZWRcclxuICAgICAgICAgICAgICAoY2hhbmdlKT1cInZhbGlkYXRlRmllbGQoKVwiIFtkaXNhYmxlZF09XCIoc3RhdGUgPT0gJ2Rpc2FibGVkJylcIj5cclxuXHJcbiAgICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gICAgICA8L3NlbGVjdD5cclxuICAgICAgPHNwYW4+U2VsZWNjaW9uZTwvc3Bhbj5cclxuICAgICAgPGRpdiBjbGFzcz1cImljb25cIj5cclxuICAgICAgICA8aSBjbGFzcz1cInt7IGljb24gfX1cIj48L2k+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9sYWJlbD5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwibWVzc2FnZVwiPlxyXG4gICAgPHA+e3sgbWVzc2FnZSB9fTwvcD5cclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC50ZXh0LWZpZWxke21hcmdpbi1ib3R0b206MTBweH0udGV4dC1maWVsZC5kaXNhYmxlZHtvcGFjaXR5Oi4zfS50ZXh0LWZpZWxkLmRpc2FibGVkIC5jb250ZW50LC50ZXh0LWZpZWxkLmRpc2FibGVkIGlucHV0LC50ZXh0LWZpZWxkLmRpc2FibGVkIGxhYmVsLC50ZXh0LWZpZWxkLmRpc2FibGVkIHNwYW57Y3Vyc29yOm5vdC1hbGxvd2VkIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciAuaWNvbiwudGV4dC1maWVsZC5lcnJvciBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkLmVycm9yIHAsLnRleHQtZmllbGQuZXJyb3Igc3Bhbntjb2xvcjojZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciBpbnB1dHtib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZCAuY29udGVudHt3aWR0aDoxMDAlO2NvbG9yOiM0NTQ2NDg7YmFja2dyb3VuZC1jb2xvcjojZjdmN2Y3O2JvcmRlci1yYWRpdXM6NnB4IDZweCAwIDB9LnRleHQtZmllbGQgLmNvbnRlbnQgbGFiZWx7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt3aWR0aDoxMDAlO21pbi1oZWlnaHQ6NTBweH0udGV4dC1maWVsZCAuY29udGVudCBzZWxlY3R7cG9zaXRpb246YWJzb2x1dGU7dG9wOjE1cHg7ei1pbmRleDoyO3dpZHRoOjEwMCU7Zm9udC1zaXplOjE2cHg7Ym9yZGVyOjA7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgIzgwODI4NTt0cmFuc2l0aW9uOmJvcmRlci1jb2xvciAuMnMgZWFzZS1pbi1vdXQ7b3V0bGluZTowO2hlaWdodDozNXB4O2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4Oy1tb3otYXBwZWFyYW5jZTpub25lOy13ZWJraXQtYXBwZWFyYW5jZTpub25lO2FwcGVhcmFuY2U6bm9uZX0udGV4dC1maWVsZCAuY29udGVudCBzZWxlY3Q6Zm9jdXN7Ym9yZGVyLWJvdHRvbToycHggc29saWQgI2ZmZDIwMH0udGV4dC1maWVsZCAuY29udGVudCBzZWxlY3Q6Zm9jdXMrc3BhbiwudGV4dC1maWVsZCAuY29udGVudCBzZWxlY3Q6dmFsaWQrc3Bhbnt0b3A6NnB4O2N1cnNvcjppbmhlcml0O2ZvbnQtc2l6ZToxM3B4O2NvbG9yOiM4MDgyODU7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLnN0YXR1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjMDA0NDhkfS50ZXh0LWZpZWxkIC5jb250ZW50IHNwYW57cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MjBweDt6LWluZGV4OjE7Zm9udC1zaXplOjE2cHg7dHJhbnNpdGlvbjphbGwgLjJzIGVhc2UtaW4tb3V0O3dpZHRoOjEwMCU7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29ue2ZvbnQtc2l6ZToxOHB4O3Bvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6MztyaWdodDoxNXB4O3RvcDoxNXB4fS50ZXh0LWZpZWxkIC5tZXNzYWdle21pbi1oZWlnaHQ6MjBweH0udGV4dC1maWVsZCAubWVzc2FnZSBwe3BhZGRpbmc6NHB4IDAgMCAxNXB4O2ZvbnQtc2l6ZToxMnB4O2NvbG9yOiM4MDgyODU7bWFyZ2luOjB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGVjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHZhbHVlU2VsZWN0ID0gMDtcclxuICBlbXB0eVNlbGVjdDogc3RyaW5nO1xyXG5cclxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgc3RhdGU6IHN0cmluZztcclxuICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZpZWxkKCkge1xyXG4gICAgKHRoaXMudmFsdWVTZWxlY3QgIT09IDApID8gdGhpcy5lbXB0eVNlbGVjdCA9ICdzdGF0dXMnIDogdGhpcy5lbXB0eVNlbGVjdCA9ICcnO1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXN1YnRpdGxlJyxcclxuICB0ZW1wbGF0ZTogYDxoMj5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvaDI+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYGgye2ZvbnQtc2l6ZToxN3B4O2ZvbnQtd2VpZ2h0OjcwMDtsaW5lLWhlaWdodDoxLjM1fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdWJ0aXRsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10YWInLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBbaGlkZGVuXT1cIiFhY3RpdmVcIiBjbGFzcz1cInBhbmVcIj5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvZGl2PmAsXHJcbiAgc3R5bGVzOiBbYC5wYW5le3BhZGRpbmc6MzBweCAyMHB4O2p1c3RpZnktY29udGVudDpjZW50ZXJ9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cclxuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgYWN0aXZlID0gZmFsc2U7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHtDb21wb25lbnQsIENvbnRlbnRDaGlsZHJlbiwgSW5wdXQsIE9uSW5pdCwgQWZ0ZXJDb250ZW50SW5pdCwgUXVlcnlMaXN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7VGFiQ29tcG9uZW50fSBmcm9tICcuLi90YWIvdGFiLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10YWJzJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJuYXYgbmF2LXRhYnNcIj5cclxuICA8c3BhbiAqbmdGb3I9XCJsZXQgdGFiIG9mIHRhYnNcIiBpZD1cInt7dGFiLmlkfX1cIiAoY2xpY2spPVwic2VsZWN0VGFiKHRhYilcIiBbY2xhc3MuYWN0aXZlXT1cInRhYi5hY3RpdmVcIiBjbGFzcz1cInRhYlwiXHJcbiAgICAgICAgbmdDbGFzcz1cInt7cG9zaXRpb259fVwiPlxyXG4gICAgPHNwYW4gcm9sZT1cImJ1dHRvblwiIGNsYXNzPVwidGFiLXRpdGxlXCI+e3t0YWIudGl0bGV9fTwvc3Bhbj5cclxuICA8L3NwYW4+XHJcbjwvZGl2PlxyXG48bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5uYXYtdGFic3tkaXNwbGF5OmZsZXg7ZmxleC13cmFwOm5vd3JhcDtmb250LXNpemU6MWVtO2ZvbnQtd2VpZ2h0OjcwMDt0ZXh0LXRyYW5zZm9ybTp1cHBlcmNhc2U7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgI2U2ZTdlOH0ubmF2LXRhYnMgLnRhYnt3aWR0aDo1MCU7cGFkZGluZzoxOHB4IDEwcHggMTBweDt0ZXh0LWFsaWduOmNlbnRlcn0ubmF2LXRhYnMgLmNlbnRlcntmbGV4OjEgMTtkaXNwbGF5OmZsZXh9Lm5hdi10YWJzIC50YWItdGl0bGV7cGFkZGluZzowIDdweCA1cHg7Y3Vyc29yOnBvaW50ZXJ9Lm5hdi10YWJzIC5hY3RpdmV7Ym9yZGVyLWJvdHRvbTpzb2xpZCAjZmZkMjAwfWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlckNvbnRlbnRJbml0ICB7XHJcblxyXG4gIEBJbnB1dCgpIHBvc2l0aW9uOiBzdHJpbmc7XHJcbiAgQENvbnRlbnRDaGlsZHJlbihUYWJDb21wb25lbnQpIHRhYnM6IFF1ZXJ5TGlzdDxUYWJDb21wb25lbnQ+O1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcclxuICAgIC8vIGdldCBhbGwgYWN0aXZlIHRhYnNcclxuICAgIGNvbnN0IGFjdGl2ZVRhYnMgPSB0aGlzLnRhYnMuZmlsdGVyKCh0YWIpID0+IHRhYi5hY3RpdmUpO1xyXG5cclxuICAgIC8vIGlmIHRoZXJlIGlzIG5vIGFjdGl2ZSB0YWIgc2V0LCBhY3RpdmF0ZSB0aGUgZmlyc3RcclxuICAgIGlmIChhY3RpdmVUYWJzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdFRhYih0aGlzLnRhYnMuZmlyc3QpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2VsZWN0VGFiKHRhYjogVGFiQ29tcG9uZW50KSB7XHJcbiAgICAvLyBkZWFjdGl2YXRlIGFsbCB0YWJzXHJcbiAgICB0aGlzLnRhYnMudG9BcnJheSgpLmZvckVhY2goIHRhYiA9PiB0YWIuYWN0aXZlID0gZmFsc2UpO1xyXG5cclxuICAgIC8vIGFjdGl2YXRlIHRoZSB0YWIgdGhlIHVzZXIgaGFzIGNsaWNrZWQgb24uXHJcbiAgICB0YWIuYWN0aXZlID0gdHJ1ZTtcclxuICB9XHJcblxyXG59XHJcbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge091dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10ZXh0LWZpZWxkJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJ0ZXh0LWZpZWxkIHt7IHN0YXRlIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgIDxsYWJlbD5cclxuICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJ7eyBpZCB9fVwiIGNsYXNzPVwie3sgaW5wdXRTdHlsZSB9fVwiIFsobmdNb2RlbCldPVwidmFsdWVJbnB1dFwiIG5hbWU9XCJ0ZXh0LWZpZWxkXCIgW3JlcXVpcmVkXT1cInRydWVcIlxyXG4gICAgICBpbnB1dC1kaXJlY3RpdmUgW21pbkxlbmd0aF09OCBbbWF4TGVuZ3RoXT0xNiBbcGF0dGVybl09XCJwYXR0ZXJuXCIgKHNldFN0YXRlKT1cIm9uU2V0U3RhdGVDaGFuZ2UoJGV2ZW50KVwiIChrZXl1cCk9XCJ2YWxpZGF0ZUZpZWxkKClcIiBbZGlzYWJsZWRdPVwiKHN0YXRlID09ICdkaXNhYmxlZCcpXCI+XHJcbiAgICAgIDxzcGFuPnt7IGxhYmVsIH19PC9zcGFuPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwie3sgaWNvblByaW1hcnkgfX1cIj48L2k+XHJcbiAgICAgICAgPGkgY2xhc3M9XCJ7eyBpY29uU2Vjb25kYXJ5IH19XCI+PC9pPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvbGFiZWw+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj5cclxuICAgIDxwPnt7IG1lc3NhZ2UgfX08L3A+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AudGV4dC1maWVsZHttYXJnaW4tYm90dG9tOjEwcHh9LnRleHQtZmllbGQuZGlzYWJsZWR7b3BhY2l0eTouM30udGV4dC1maWVsZC5kaXNhYmxlZCAuY29udGVudCwudGV4dC1maWVsZC5kaXNhYmxlZCBpbnB1dCwudGV4dC1maWVsZC5kaXNhYmxlZCBsYWJlbCwudGV4dC1maWVsZC5kaXNhYmxlZCBzcGFue2N1cnNvcjpub3QtYWxsb3dlZCFpbXBvcnRhbnR9LnRleHQtZmllbGQuZXJyb3IgaW5wdXQ6Zm9jdXMrc3BhbiwudGV4dC1maWVsZC5lcnJvciBwLC50ZXh0LWZpZWxkLmVycm9yIHNwYW57Y29sb3I6I2ZhNWU1YiFpbXBvcnRhbnR9LnRleHQtZmllbGQuZXJyb3IgaW5wdXR7Ym9yZGVyLWJvdHRvbToycHggc29saWQgI2ZhNWU1YiFpbXBvcnRhbnR9LnRleHQtZmllbGQgLmNvbnRlbnR7d2lkdGg6MTAwJTtjb2xvcjojNDU0NjQ4O2JhY2tncm91bmQtY29sb3I6I2Y3ZjdmNztib3JkZXItcmFkaXVzOjZweCA2cHggMCAwfS50ZXh0LWZpZWxkIC5jb250ZW50IGxhYmVse3Bvc2l0aW9uOnJlbGF0aXZlO2Rpc3BsYXk6YmxvY2s7d2lkdGg6MTAwJTttaW4taGVpZ2h0OjUwcHh9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXR7cG9zaXRpb246YWJzb2x1dGU7dG9wOjE1cHg7ei1pbmRleDoxO3dpZHRoOjEwMCU7Zm9udC1zaXplOjFlbTtib3JkZXI6MDtib3JkZXItYm90dG9tOjFweCBzb2xpZCAjODA4Mjg1O3RyYW5zaXRpb246Ym9yZGVyLWNvbG9yIC4ycyBlYXNlLWluLW91dDtvdXRsaW5lOjA7aGVpZ2h0OjM1cHg7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6Zm9jdXN7Ym9yZGVyLWJvdHRvbToycHggc29saWQgI2ZmZDIwMH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OnZhbGlkK3NwYW57dG9wOjZweDtjdXJzb3I6aW5oZXJpdDtmb250LXNpemU6LjhlbTtjb2xvcjojODA4Mjg1O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IC5zdGF0dXN7Ym9yZGVyLWJvdHRvbToycHggc29saWQgIzAwNDQ4ZH0udGV4dC1maWVsZCAuY29udGVudCBzcGFue3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6YmxvY2s7dG9wOjIwcHg7ei1pbmRleDoyO2ZvbnQtc2l6ZToxZW07dHJhbnNpdGlvbjphbGwgLjJzIGVhc2UtaW4tb3V0O3dpZHRoOjEwMCU7Y3Vyc29yOnRleHQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29ue3otaW5kZXg6MzttYXJnaW46MTVweCAwIDA7cmlnaHQ6MDtwb3NpdGlvbjphYnNvbHV0ZX0udGV4dC1maWVsZCAuY29udGVudCAuaWNvbiBpe2ZvbnQtc2l6ZToxLjJlbTtsZXR0ZXItc3BhY2luZzoxNXB4O2N1cnNvcjpwb2ludGVyfS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29uIGk6bnRoLWNoaWxkKDIpe2NvbG9yOiNmYTVlNWJ9LnRleHQtZmllbGQgLm1lc3NhZ2V7bWluLWhlaWdodDoyMHB4fS50ZXh0LWZpZWxkIC5tZXNzYWdlIHB7cGFkZGluZzowIDAgMCAxNXB4O2ZvbnQtc2l6ZTouOGVtO2NvbG9yOiM4MDgyODU7bWFyZ2luOi0zcHggMCAwfWBdLFxyXG5cclxufSlcclxuZXhwb3J0IGNsYXNzIFRleHRGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcHVibGljIHZhbHVlSW5wdXQ7XHJcbiAgaW5wdXRTdHlsZTogc3RyaW5nO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgdmFsdWVVc2VybmFtZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb25QcmltYXJ5OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvblNlY29uZGFyeTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHN0YXRlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbWVzc2FnZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHBhdHRlcm46IHN0cmluZztcclxuICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG5cclxuICBnZXRWYWx1ZVVzZXJuYW1lKCkge1xyXG4gICAgdGhpcy52YWx1ZVVzZXJuYW1lLmVtaXQoe1xyXG4gICAgICB2YWx1ZTogdGhpcy52YWx1ZUlucHV0LFxyXG4gICAgICBzdGF0ZTogdGhpcy5pbnB1dFN0eWxlXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZpZWxkKCkge1xyXG4gICAgaWYgKHRoaXMudmFsdWVJbnB1dCAhPT0gbnVsbCAmJiB0aGlzLnZhbHVlSW5wdXQgIT09ICcnKSB7XHJcbiAgICAgIHRoaXMuaW5wdXRTdHlsZSA9ICdzdGF0dXMnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pbnB1dFN0eWxlID0gJyc7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdldFZhbHVlVXNlcm5hbWUoKTtcclxuICB9XHJcblxyXG4gIG9uU2V0U3RhdGVDaGFuZ2UoZXZlbnQpIHtcclxuICAgIHRoaXMuc3RhdGUgPSBldmVudC5zdGF0ZTtcclxuICAgIHRoaXMuaW5wdXRTdHlsZSA9IGV2ZW50LnN0YXRlO1xyXG4gICAgaWYgKGV2ZW50LnN0YXRlICE9PSAnJykge1xyXG4gICAgICB0aGlzLm1lc3NhZ2UgPSAnTm8gY3VtcGxlIGNvbiBsYSBsb25naXR1ZCBtw4PCrW5pbWEnO1xyXG4gICAgICB0aGlzLmljb25TZWNvbmRhcnkgPSAnZmFsIGZhLXRpbWVzJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICcnO1xyXG4gICAgICB0aGlzLmljb25TZWNvbmRhcnkgPSAnJztcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy52YWx1ZUlucHV0ID09PSAnJykge1xyXG4gICAgICB0aGlzLm1lc3NhZ2UgPSAnJztcclxuICAgICAgdGhpcy5zdGF0ZSA9ICcnO1xyXG4gICAgICB0aGlzLmljb25TZWNvbmRhcnkgPSAnJztcclxuICAgIH1cclxuICAgIHRoaXMuZ2V0VmFsdWVVc2VybmFtZSgpO1xyXG4gIH1cclxuXHJcbn1cclxuXHJcblxyXG4iLCJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRpdGxlJyxcclxuICB0ZW1wbGF0ZTogYDxoMT5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvaDE+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYGgxe2ZvbnQtc2l6ZToyNHB4O2ZvbnQtd2VpZ2h0OjcwMDtsaW5lLWhlaWdodDoxLjE3fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUaXRsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10b29sdGlwJyxcclxuICB0ZW1wbGF0ZTogYDxhICBjbGFzcz1cInRvb2x0aXBcIiB0aXRsZT1cInt7IHRpdGxlfX1cIj5cclxuICA8aSBjbGFzcz1cInt7IGljb24gfX1cIj48L2k+XHJcbjwvYT5gLFxyXG4gIHN0eWxlczogW2AudG9vbHRpcHtkaXNwbGF5OmlubGluZTtwb3NpdGlvbjpyZWxhdGl2ZX0udG9vbHRpcDpob3ZlcjphZnRlcntiYWNrZ3JvdW5kOnJnYmEoMCwwLDAsLjgpO2JvcmRlci1yYWRpdXM6NXB4O2JvdHRvbToyNnB4O2NvbG9yOiNmZmY7Y29udGVudDphdHRyKHRpdGxlKTtsZWZ0Oi02MDAlO3BhZGRpbmc6NXB4IDE1cHg7cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDo5ODt3aWR0aDoyMjBweH0udG9vbHRpcDpob3ZlcjpiZWZvcmV7Ym9yZGVyOnNvbGlkO2JvcmRlci1jb2xvcjojMzMzIHRyYW5zcGFyZW50O2JvcmRlci13aWR0aDo2cHggNnB4IDA7Ym90dG9tOjIwcHg7Y29udGVudDpcIlwiO2xlZnQ6MDtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4Ojk5fS50b29sdGlwIGl7Zm9udC1zaXplOjEuMmVtO2xldHRlci1zcGFjaW5nOjE1cHg7Y3Vyc29yOnBvaW50ZXJ9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRvb2x0aXBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSB0aXRsZTpzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjpzdHJpbmc7XHJcbiAgXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBCYW5uZXJDb21wb25lbnQgfSBmcm9tICcuL2Jhbm5lci9iYW5uZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9idXR0b24vYnV0dG9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRDb21wb25lbnQgfSBmcm9tICcuL2NhcmQvY2FyZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJvdXNlbEJhbm5lckNvbXBvbmVudCB9IGZyb20gJy4vY2Fyb3VzZWwtYmFubmVyL2Nhcm91c2VsLWJhbm5lci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb290ZXJDb21wb25lbnQgfSBmcm9tICcuL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9vdGVyRXh0ZXJuYWxDb21wb25lbnQgfSBmcm9tICcuL2Zvb3Rlci1leHRlcm5hbC9mb290ZXItZXh0ZXJuYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSHlwZXJsaW5rQ29tcG9uZW50IH0gZnJvbSAnLi9oeXBlcmxpbmsvaHlwZXJsaW5rLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvYWRpbmdDb21wb25lbnQgfSBmcm9tICcuL2xvYWRpbmcvbG9hZGluZy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vbW9kYWwvbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmF2YmFyQ29tcG9uZW50IH0gZnJvbSAnLi9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBPdXRsaW5lQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9vdXRsaW5lLWJ1dHRvbi9vdXRsaW5lLWJ1dHRvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBQYXNzd29yZEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9wYXNzd29yZC1maWVsZC9wYXNzd29yZC1maWVsZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZWNvbmRhcnlMaW5rQ29tcG9uZW50IH0gZnJvbSAnLi9zZWNvbmRhcnktbGluay9zZWNvbmRhcnktbGluay5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZWxlY3RDb21wb25lbnQgfSBmcm9tICcuL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3VidGl0bGVDb21wb25lbnQgfSBmcm9tICcuL3N1YnRpdGxlL3N1YnRpdGxlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRhYkNvbXBvbmVudCB9IGZyb20gJy4vdGFiL3RhYi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYWJzQ29tcG9uZW50IH0gZnJvbSAnLi90YWJzL3RhYnMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGV4dEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi90ZXh0LWZpZWxkL3RleHQtZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGl0bGVDb21wb25lbnQgfSBmcm9tICcuL3RpdGxlL3RpdGxlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRvb2x0aXBDb21wb25lbnQgfSBmcm9tICcuL3Rvb2x0aXAvdG9vbHRpcC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgRm9ybXNNb2R1bGVcclxuICBdLFxyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgQmFubmVyQ29tcG9uZW50LFxyXG4gICAgQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQ2FyZENvbXBvbmVudCxcclxuICAgIENhcm91c2VsQmFubmVyQ29tcG9uZW50LFxyXG4gICAgRm9vdGVyQ29tcG9uZW50LFxyXG4gICAgRm9vdGVyRXh0ZXJuYWxDb21wb25lbnQsXHJcbiAgICBIeXBlcmxpbmtDb21wb25lbnQsXHJcbiAgICBMb2FkaW5nQ29tcG9uZW50LFxyXG4gICAgTW9kYWxDb21wb25lbnQsXHJcbiAgICBOYXZiYXJDb21wb25lbnQsXHJcbiAgICBOb3RpZmljYXRpb25Db21wb25lbnQsXHJcbiAgICBPdXRsaW5lQnV0dG9uQ29tcG9uZW50LFxyXG4gICAgUGFzc3dvcmRGaWVsZENvbXBvbmVudCxcclxuICAgIFNlY29uZGFyeUxpbmtDb21wb25lbnQsXHJcbiAgICBTZWxlY3RDb21wb25lbnQsXHJcbiAgICBTdWJ0aXRsZUNvbXBvbmVudCxcclxuICAgIFRhYkNvbXBvbmVudCxcclxuICAgIFRhYnNDb21wb25lbnQsXHJcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXHJcbiAgICBUaXRsZUNvbXBvbmVudCxcclxuICAgIFRvb2x0aXBDb21wb25lbnRcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIEJhbm5lckNvbXBvbmVudCxcclxuICAgIEJ1dHRvbkNvbXBvbmVudCxcclxuICAgIENhcmRDb21wb25lbnQsXHJcbiAgICBDYXJvdXNlbEJhbm5lckNvbXBvbmVudCxcclxuICAgIEZvb3RlckNvbXBvbmVudCxcclxuICAgIEZvb3RlckV4dGVybmFsQ29tcG9uZW50LFxyXG4gICAgSHlwZXJsaW5rQ29tcG9uZW50LFxyXG4gICAgTG9hZGluZ0NvbXBvbmVudCxcclxuICAgIE1vZGFsQ29tcG9uZW50LFxyXG4gICAgTmF2YmFyQ29tcG9uZW50LFxyXG4gICAgTm90aWZpY2F0aW9uQ29tcG9uZW50LFxyXG4gICAgT3V0bGluZUJ1dHRvbkNvbXBvbmVudCxcclxuICAgIFBhc3N3b3JkRmllbGRDb21wb25lbnQsXHJcbiAgICBTZWNvbmRhcnlMaW5rQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0Q29tcG9uZW50LFxyXG4gICAgU3VidGl0bGVDb21wb25lbnQsXHJcbiAgICBUYWJDb21wb25lbnQsXHJcbiAgICBUYWJzQ29tcG9uZW50LFxyXG4gICAgVGV4dEZpZWxkQ29tcG9uZW50LFxyXG4gICAgVGl0bGVDb21wb25lbnQsXHJcbiAgICBUb29sdGlwQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVmlld0xpYk1vZHVsZSB7IH1cclxuIl0sIm5hbWVzIjpbIkluamVjdGFibGUiLCJDb21wb25lbnQiLCJJbnB1dCIsIkV2ZW50RW1pdHRlciIsIk91dHB1dCIsIkNvbnRlbnRDaGlsZHJlbiIsIk5nTW9kdWxlIiwiQ29tbW9uTW9kdWxlIiwiRm9ybXNNb2R1bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtRQU9FO1NBQWlCOztvQkFMbEJBLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7OzZCQUpEOzs7Ozs7O0FDQUE7UUFZRTtTQUFpQjs7OztRQUVqQixrQ0FBUTs7O1lBQVI7YUFDQzs7b0JBYkZDLFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsUUFBUSxFQUFFLCtEQUdYO3dCQUNDLE1BQU0sRUFBRSxDQUFDLGlJQUFpSSxDQUFDO3FCQUM1STs7Ozs4QkFURDs7Ozs7OztBQ0FBO1FBZUU7U0FBaUI7Ozs7UUFFakIsa0NBQVE7OztZQUFSO2FBQ0M7O29CQWhCRkEsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxZQUFZO3dCQUN0QixRQUFRLEVBQUUsK0RBR1g7d0JBQ0MsTUFBTSxFQUFFLENBQUMsdWdDQUF1Z0MsQ0FBQztxQkFDbGhDOzs7OzsyQkFHRUMsUUFBSzs0QkFDTEEsUUFBSzs7OEJBYlI7Ozs7Ozs7QUNBQTtRQVlFO1NBQWlCOzs7O1FBRWpCLGdDQUFROzs7WUFBUjthQUNDOztvQkFiRkQsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxVQUFVO3dCQUNwQixRQUFRLEVBQUUsNkRBR1g7d0JBQ0MsTUFBTSxFQUFFLENBQUMsdUpBQXVKLENBQUM7cUJBQ2xLOzs7OzRCQVREOzs7Ozs7O0FDQUE7UUFnQkU7U0FBaUI7Ozs7UUFFakIsMENBQVE7OztZQUFSO2FBQ0M7O29CQWpCRkEsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxxQkFBcUI7d0JBQy9CLFFBQVEsRUFBRSxxT0FPWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQyxzZ0JBQXNnQixDQUFDO3FCQUNqaEI7Ozs7c0NBYkQ7Ozs7Ozs7QUNBQTtRQXFCRTtTQUFpQjs7OztRQUVqQixrQ0FBUTs7O1lBQVI7YUFDQzs7b0JBdEJGQSxZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLFlBQVk7d0JBQ3RCLFFBQVEsRUFBRSw4UEFZWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQyx1TEFBdUwsQ0FBQztxQkFDbE07Ozs7OEJBbEJEOzs7Ozs7O0FDQUE7UUF3QkU7U0FBaUI7Ozs7UUFFakIsMENBQVE7OztZQUFSO2FBQ0M7O29CQXpCRkEsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxxQkFBcUI7d0JBQy9CLFFBQVEsRUFBRSw4Y0FlWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQyx5S0FBeUssQ0FBQztxQkFDcEw7Ozs7c0NBckJEOzs7Ozs7O0FDQUE7UUFjRTtTQUNDOzs7O1FBRUQscUNBQVE7OztZQUFSO2FBQ0M7O29CQWhCRkEsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxlQUFlO3dCQUN6QixRQUFRLEVBQUUsK0NBR1g7d0JBQ0MsTUFBTSxFQUFFLENBQUMsb0ZBQW9GLENBQUM7cUJBQy9GOzs7Ozs0QkFFRUMsUUFBSzswQkFDTEEsUUFBSzs7aUNBWlI7Ozs7Ozs7QUNBQTtRQXFCRTtTQUFpQjs7OztRQUVqQixtQ0FBUTs7O1lBQVI7YUFDQzs7b0JBdEJGRCxZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGFBQWE7d0JBQ3ZCLFFBQVEsRUFBRSw2T0FVWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQyxpcUVBQWlxRSxDQUFDO3FCQUM1cUU7Ozs7O3lCQUdFQyxRQUFLOzsrQkFuQlI7Ozs7Ozs7QUNBQTs7Ozs7O1FBc0NFLGtDQUFTOzs7WUFBVDtnQkFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztnQkFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQzthQUN6Qzs7OztRQUVELG1DQUFVOzs7WUFBVjtnQkFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztnQkFDdEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQzthQUN2Qzs7b0JBNUNGRCxZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLFdBQVc7d0JBQ3JCLFFBQVEsRUFBRSw0M0JBcUJYO3dCQUNDLE1BQU0sRUFBRSxDQUFDLHlsQ0FBeWxDLENBQUM7cUJBQ3BtQzs7O3lCQUdFQyxRQUFLO2lDQUNMQSxRQUFLO3FDQUNMQSxRQUFLO3FDQUNMQSxRQUFLO21DQUNMQSxRQUFLOzs2QkFsQ1I7Ozs7Ozs7QUNBQTtRQXNDRTt3QkFKTyxJQUFJLElBQUksRUFBRTtTQUlBOzs7O1FBRWpCLGtDQUFROzs7WUFBUjthQUNDOztvQkF2Q0ZELFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsUUFBUSxFQUFFLDBxQkEwQlg7d0JBQ0MsTUFBTSxFQUFFLENBQUMsdXJCQUF1ckIsQ0FBQztxQkFDbHNCOzs7Ozs4QkFJRUMsUUFBSzs7OEJBcENSOzs7Ozs7O0FDQUE7UUFxQkU7U0FBaUI7Ozs7UUFFakIsd0NBQVE7OztZQUFSO2FBQ0M7O29CQXRCRkQsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxrQkFBa0I7d0JBQzVCLFFBQVEsRUFBRSwrS0FRWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQyx3YUFBd2EsQ0FBQztxQkFDbmI7Ozs7OzJCQUdFQyxRQUFLOzJCQUNMQSxRQUFLOzhCQUNMQSxRQUFLOztvQ0FuQlI7Ozs7Ozs7QUNBQTtRQWVFO1NBQWlCOzs7O1FBRWpCLHlDQUFROzs7WUFBUjthQUNDOztvQkFoQkZELFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsb0JBQW9CO3dCQUM5QixRQUFRLEVBQUUsb0lBSVg7d0JBQ0MsTUFBTSxFQUFFLENBQUMsc1VBQXNVLENBQUM7cUJBQ2pWOzs7Ozt5QkFFRUMsUUFBSzsyQkFDTEEsUUFBSzs7cUNBYlI7Ozs7Ozs7QUNBQTtRQXdDRTtpQ0FYaUMsSUFBSUMsZUFBWSxFQUFFO3lCQVNwQyxFQUFFO1NBSWhCOzs7O1FBQ0QsaURBQWdCOzs7WUFBaEI7Z0JBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDbEQ7Ozs7UUFFRCx5Q0FBUTs7O1lBQVI7Z0JBQ0UsSUFBSSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7YUFFeEI7Ozs7UUFFRCw4Q0FBYTs7O1lBQWI7Z0JBQ0UsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ3pCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2FBQ3pIOzs7O1FBQ0QsMkNBQVU7OztZQUFWO2dCQUNFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQzthQUNuQjs7Ozs7UUFFRCxpREFBZ0I7Ozs7WUFBaEIsVUFBaUIsS0FBSztnQkFDcEIsSUFBSSxDQUFDLEtBQUssR0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO2dCQUN2QixJQUFHLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFDO29CQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLGtDQUFrQyxDQUFBO2lCQUNsRDtxQkFBSztvQkFDSixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztpQkFDbkI7YUFDRjs7b0JBbkVGRixZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjt3QkFDOUIsUUFBUSxFQUFFLDI1QkFrQlg7d0JBQ0MsTUFBTSxFQUFFLENBQUMsb3FEQUFvcUQsQ0FBQztxQkFDL3FEOzs7OztvQ0FLRUcsU0FBTTs0QkFDTkYsUUFBSzs0QkFDTEEsUUFBSzs4QkFDTEEsUUFBSzsyQkFDTEEsUUFBSzs4QkFDTEEsUUFBSzsyQkFDTEEsUUFBSzt5QkFDTEEsUUFBSzs7cUNBcENSOzs7Ozs7O0FDQUE7UUFxQkU7U0FBaUI7Ozs7UUFFakIseUNBQVE7OztZQUFSO2FBQ0M7O29CQXRCRkQsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxvQkFBb0I7d0JBQzlCLFFBQVEsRUFBRSw0S0FPWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQyxrYUFBa2EsQ0FBQztxQkFDN2E7Ozs7O2dDQUVFQyxRQUFLOzJCQUNMQSxRQUFLO3lCQUNMQSxRQUFLOztxQ0FqQlI7Ozs7Ozs7QUNBQTtRQW1DRTsrQkFUYyxDQUFDO1NBVWQ7Ozs7UUFFRCxrQ0FBUTs7O1lBQVI7YUFDQzs7OztRQUVELHVDQUFhOzs7WUFBYjtnQkFDRSxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2FBQ2hGOztvQkExQ0ZELFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsUUFBUSxFQUFFLHdnQkFrQlg7d0JBQ0MsTUFBTSxFQUFFLENBQUMsMm9EQUEyb0QsQ0FBQztxQkFDdHBEOzs7Ozs0QkFNRUMsUUFBSzsyQkFDTEEsUUFBSzsyQkFDTEEsUUFBSzs0QkFDTEEsUUFBSzs4QkFDTEEsUUFBSzs7OEJBakNSOzs7Ozs7O0FDQUE7UUFZRTtTQUFpQjs7OztRQUVqQixvQ0FBUTs7O1lBQVI7YUFDQzs7b0JBYkZELFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsY0FBYzt3QkFDeEIsUUFBUSxFQUFFLDRDQUdYO3dCQUNDLE1BQU0sRUFBRSxDQUFDLHFEQUFxRCxDQUFDO3FCQUNoRTs7OztnQ0FURDs7Ozs7OztBQ0FBO1FBZ0JFOzBCQUZrQixLQUFLO1NBRU47Ozs7UUFFakIsK0JBQVE7OztZQUFSO2FBQ0M7O29CQWpCRkEsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxTQUFTO3dCQUNuQixRQUFRLEVBQUUsZ0ZBRUw7d0JBQ0wsTUFBTSxFQUFFLENBQUMsaURBQWlELENBQUM7cUJBQzVEOzs7Ozs0QkFJRUMsUUFBSzt5QkFDTEEsUUFBSzs2QkFDTEEsUUFBSzs7MkJBZFI7Ozs7Ozs7QUNBQTtRQXFCRTtTQUFpQjs7OztRQUVqQixnQ0FBUTs7O1lBQVI7YUFDQzs7OztRQUVELDBDQUFrQjs7O1lBQWxCOztnQkFFRSxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEdBQUcsQ0FBQyxNQUFNLEdBQUEsQ0FBQyxDQUFDOztnQkFHekQsSUFBSSxVQUFVLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQzthQUNGOzs7OztRQUVELGlDQUFTOzs7O1lBQVQsVUFBVSxHQUFpQjs7Z0JBRXpCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFFLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLEdBQUEsQ0FBQyxDQUFDOztnQkFHeEQsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDbkI7O29CQXRDRkQsWUFBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxVQUFVO3dCQUNwQixRQUFRLEVBQUUsa1RBT1g7d0JBQ0MsTUFBTSxFQUFFLENBQUMsK1VBQStVLENBQUM7cUJBQzFWOzs7OzsrQkFHRUMsUUFBSzsyQkFDTEcsa0JBQWUsU0FBQyxZQUFZOzs0QkFuQi9COzs7Ozs7O0FDQUE7UUE0Q0U7aUNBaEJpQyxJQUFJRixlQUFZLEVBQUU7U0FpQmxEOzs7O1FBUkQsNkNBQWdCOzs7WUFBaEI7Z0JBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7b0JBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVTtvQkFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVO2lCQUN2QixDQUFDLENBQUM7YUFDSjs7OztRQUtELHFDQUFROzs7WUFBUjthQUNDOzs7O1FBRUQsMENBQWE7OztZQUFiO2dCQUNFLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxFQUFFLEVBQUU7b0JBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO2lCQUM1QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztpQkFDdEI7Z0JBQ0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7YUFDekI7Ozs7O1FBRUQsNkNBQWdCOzs7O1lBQWhCLFVBQWlCLEtBQUs7Z0JBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztnQkFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO2dCQUM5QixJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO29CQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLGtDQUFrQyxDQUFDO29CQUNsRCxJQUFJLENBQUMsYUFBYSxHQUFHLGNBQWMsQ0FBQztpQkFDckM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2lCQUN6QjtnQkFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssRUFBRSxFQUFFO29CQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2lCQUN6QjtnQkFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUN6Qjs7b0JBekVGRixZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjt3QkFDMUIsUUFBUSxFQUFFLHFwQkFnQlg7d0JBQ0MsTUFBTSxFQUFFLENBQUMsOHJEQUE4ckQsQ0FBQztxQkFFenNEOzs7OztvQ0FJRUcsU0FBTTs0QkFDTkYsUUFBSztrQ0FDTEEsUUFBSztvQ0FDTEEsUUFBSzs0QkFDTEEsUUFBSzs4QkFDTEEsUUFBSzs4QkFDTEEsUUFBSzt5QkFDTEEsUUFBSzs7aUNBbkNSOzs7Ozs7O0FDQUE7UUFZRTtTQUFpQjs7OztRQUVqQixpQ0FBUTs7O1lBQVI7YUFDQzs7b0JBYkZELFlBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsV0FBVzt3QkFDckIsUUFBUSxFQUFFLDRDQUdYO3dCQUNDLE1BQU0sRUFBRSxDQUFDLHFEQUFxRCxDQUFDO3FCQUNoRTs7Ozs2QkFURDs7Ozs7OztBQ0FBO1FBY0U7U0FBaUI7Ozs7UUFFakIsbUNBQVE7OztZQUFSO2FBQ0M7O29CQWZGQSxZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGFBQWE7d0JBQ3ZCLFFBQVEsRUFBRSxtRkFFUDt3QkFDSCxNQUFNLEVBQUUsQ0FBQyxnYkFBOGEsQ0FBQztxQkFDemI7Ozs7OzRCQUdFQyxRQUFLOzJCQUNMQSxRQUFLOzsrQkFaUjs7Ozs7OztBQ0FBOzs7O29CQXlCQ0ksV0FBUSxTQUFDO3dCQUNSLE9BQU8sRUFBRTs0QkFDUEMsbUJBQVk7NEJBQ1pDLGlCQUFXO3lCQUNaO3dCQUNELFlBQVksRUFBRTs0QkFDWixlQUFlOzRCQUNmLGVBQWU7NEJBQ2YsYUFBYTs0QkFDYix1QkFBdUI7NEJBQ3ZCLGVBQWU7NEJBQ2YsdUJBQXVCOzRCQUN2QixrQkFBa0I7NEJBQ2xCLGdCQUFnQjs0QkFDaEIsY0FBYzs0QkFDZCxlQUFlOzRCQUNmLHFCQUFxQjs0QkFDckIsc0JBQXNCOzRCQUN0QixzQkFBc0I7NEJBQ3RCLHNCQUFzQjs0QkFDdEIsZUFBZTs0QkFDZixpQkFBaUI7NEJBQ2pCLFlBQVk7NEJBQ1osYUFBYTs0QkFDYixrQkFBa0I7NEJBQ2xCLGNBQWM7NEJBQ2QsZ0JBQWdCO3lCQUNqQjt3QkFDRCxPQUFPLEVBQUU7NEJBQ1AsZUFBZTs0QkFDZixlQUFlOzRCQUNmLGFBQWE7NEJBQ2IsdUJBQXVCOzRCQUN2QixlQUFlOzRCQUNmLHVCQUF1Qjs0QkFDdkIsa0JBQWtCOzRCQUNsQixnQkFBZ0I7NEJBQ2hCLGNBQWM7NEJBQ2QsZUFBZTs0QkFDZixxQkFBcUI7NEJBQ3JCLHNCQUFzQjs0QkFDdEIsc0JBQXNCOzRCQUN0QixzQkFBc0I7NEJBQ3RCLGVBQWU7NEJBQ2YsaUJBQWlCOzRCQUNqQixZQUFZOzRCQUNaLGFBQWE7NEJBQ2Isa0JBQWtCOzRCQUNsQixjQUFjOzRCQUNkLGdCQUFnQjt5QkFDakI7cUJBQ0Y7OzRCQTVFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9