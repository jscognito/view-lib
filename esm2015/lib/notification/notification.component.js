/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class NotificationComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
NotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-notification',
                template: `<div class="notification {{ type }}">
  <div class="icon">
    <i class="{{ icon }}"></i>
  </div>
  <div class="content">
    {{ message }}
  </div>
</div>
`,
                styles: [`.notification{width:auto;height:auto;border-radius:8px;padding:15px;margin-bottom:15px}.notification .icon{float:left;margin-right:15px;font-size:18px}.notification .content{text-align:center}.notification.error{background-color:#fa5e5b;color:#fff}.notification.info{background-color:#00448d;color:#fff}.notification.success{background-color:#16c98d;color:#fff}.notification.warning{background-color:#ffd200;color:#454648}`]
            },] },
];
/** @nocollapse */
NotificationComponent.ctorParameters = () => [];
NotificationComponent.propDecorators = {
    type: [{ type: Input }],
    icon: [{ type: Input }],
    message: [{ type: Input }]
};
function NotificationComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    NotificationComponent.prototype.type;
    /** @type {?} */
    NotificationComponent.prototype.icon;
    /** @type {?} */
    NotificationComponent.prototype.message;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQWV2RCxNQUFNO0lBTUosaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBdEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixRQUFRLEVBQUU7Ozs7Ozs7O0NBUVg7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsd2FBQXdhLENBQUM7YUFDbmI7Ozs7O21CQUdFLEtBQUs7bUJBQ0wsS0FBSztzQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbm90aWZpY2F0aW9uJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJub3RpZmljYXRpb24ge3sgdHlwZSB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJpY29uXCI+XHJcbiAgICA8aSBjbGFzcz1cInt7IGljb24gfX1cIj48L2k+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgIHt7IG1lc3NhZ2UgfX1cclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5ub3RpZmljYXRpb257d2lkdGg6YXV0bztoZWlnaHQ6YXV0bztib3JkZXItcmFkaXVzOjhweDtwYWRkaW5nOjE1cHg7bWFyZ2luLWJvdHRvbToxNXB4fS5ub3RpZmljYXRpb24gLmljb257ZmxvYXQ6bGVmdDttYXJnaW4tcmlnaHQ6MTVweDtmb250LXNpemU6MThweH0ubm90aWZpY2F0aW9uIC5jb250ZW50e3RleHQtYWxpZ246Y2VudGVyfS5ub3RpZmljYXRpb24uZXJyb3J7YmFja2dyb3VuZC1jb2xvcjojZmE1ZTViO2NvbG9yOiNmZmZ9Lm5vdGlmaWNhdGlvbi5pbmZve2JhY2tncm91bmQtY29sb3I6IzAwNDQ4ZDtjb2xvcjojZmZmfS5ub3RpZmljYXRpb24uc3VjY2Vzc3tiYWNrZ3JvdW5kLWNvbG9yOiMxNmM5OGQ7Y29sb3I6I2ZmZn0ubm90aWZpY2F0aW9uLndhcm5pbmd7YmFja2dyb3VuZC1jb2xvcjojZmZkMjAwO2NvbG9yOiM0NTQ2NDh9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHR5cGU6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbWVzc2FnZTogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==