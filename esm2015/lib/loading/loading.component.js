/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class LoadingComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
LoadingComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-loading',
                template: `<div class="blocking-background"></div>
<div class="container-loader" id="{{ id }}">
  <div class="loader">
    <div class="spinner">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
</div>
`,
                styles: [`.blocking-background{background-color:rgba(255,255,255,.4);position:fixed;top:0;left:0;width:100%;height:100%;z-index:400}.container-loader{background-color:rgba(255,255,255,.7);position:absolute;top:0;left:0;width:100%;height:100%;z-index:400;border-radius:8px}.container-loader .loader{display:block;position:relative;left:50%;top:50%;z-index:500}.container-loader[loaded] .spinner{-webkit-animation:2s ease-in fadeout;animation:2s ease-in fadeout;opacity:0}.container-loader .spinner{position:relative;-webkit-animation:2s ease-in fadein;animation:2s ease-in fadein}.container-loader .spinner div{position:absolute;width:20px;height:20px;border-radius:50%;opacity:.7;-webkit-transform:translateX(-30px);transform:translateX(-30px)}.container-loader .spinner div:nth-child(1){background-color:#ffd200;-webkit-animation:1.8s ease-in-out 1.2s infinite spin;animation:1.8s ease-in-out 1.2s infinite spin;height:24px;width:24px}.container-loader .spinner div:nth-child(2){background-color:#58595b;-webkit-animation:1.8s ease-in-out .6s infinite spin;animation:1.8s ease-in-out .6s infinite spin;height:20px;weight:20px}.container-loader .spinner div:nth-child(3){background-color:#00448d;-webkit-animation:1.8s ease-in-out infinite spin;animation:1.8s ease-in-out infinite spin;width:10px;height:10px}@-webkit-keyframes spin{0%{-webkit-transform:translateX(-30px);transform:translateX(-30px)}33.3%{-webkit-transform:translateX(30px);transform:translateX(30px)}66.7%{-webkit-transform:translateX(0) translateY(-45px);transform:translateX(0) translateY(-45px)}100%{-webkit-transform:translateX(-30px) translateY(0);transform:translateX(-30px) translateY(0)}}@keyframes spin{0%{-webkit-transform:translateX(-30px);transform:translateX(-30px)}33.3%{-webkit-transform:translateX(30px);transform:translateX(30px)}66.7%{-webkit-transform:translateX(0) translateY(-45px);transform:translateX(0) translateY(-45px)}100%{-webkit-transform:translateX(-30px) translateY(0);transform:translateX(-30px) translateY(0)}}@-webkit-keyframes fadein{from{opacity:0}to{opacity:1}}@keyframes fadein{from{opacity:0}to{opacity:1}}@-webkit-keyframes fadeout{from{opacity:1}to{opacity:0}}@keyframes fadeout{from{opacity:1}to{opacity:0}}`]
            },] },
];
/** @nocollapse */
LoadingComponent.ctorParameters = () => [];
LoadingComponent.propDecorators = {
    id: [{ type: Input }]
};
function LoadingComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    LoadingComponent.prototype.id;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi9sb2FkaW5nL2xvYWRpbmcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQWlCdkQsTUFBTTtJQUlKLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7OztZQXRCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFFBQVEsRUFBRTs7Ozs7Ozs7OztDQVVYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLGlxRUFBaXFFLENBQUM7YUFDNXFFOzs7OztpQkFHRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbG9hZGluZycsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwiYmxvY2tpbmctYmFja2dyb3VuZFwiPjwvZGl2PlxyXG48ZGl2IGNsYXNzPVwiY29udGFpbmVyLWxvYWRlclwiIGlkPVwie3sgaWQgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwibG9hZGVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwic3Bpbm5lclwiPlxyXG4gICAgICA8ZGl2PjwvZGl2PlxyXG4gICAgICA8ZGl2PjwvZGl2PlxyXG4gICAgICA8ZGl2PjwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AuYmxvY2tpbmctYmFja2dyb3VuZHtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMjU1LDI1NSwyNTUsLjQpO3Bvc2l0aW9uOmZpeGVkO3RvcDowO2xlZnQ6MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO3otaW5kZXg6NDAwfS5jb250YWluZXItbG9hZGVye2JhY2tncm91bmQtY29sb3I6cmdiYSgyNTUsMjU1LDI1NSwuNyk7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7bGVmdDowO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7ei1pbmRleDo0MDA7Ym9yZGVyLXJhZGl1czo4cHh9LmNvbnRhaW5lci1sb2FkZXIgLmxvYWRlcntkaXNwbGF5OmJsb2NrO3Bvc2l0aW9uOnJlbGF0aXZlO2xlZnQ6NTAlO3RvcDo1MCU7ei1pbmRleDo1MDB9LmNvbnRhaW5lci1sb2FkZXJbbG9hZGVkXSAuc3Bpbm5lcnstd2Via2l0LWFuaW1hdGlvbjoycyBlYXNlLWluIGZhZGVvdXQ7YW5pbWF0aW9uOjJzIGVhc2UtaW4gZmFkZW91dDtvcGFjaXR5OjB9LmNvbnRhaW5lci1sb2FkZXIgLnNwaW5uZXJ7cG9zaXRpb246cmVsYXRpdmU7LXdlYmtpdC1hbmltYXRpb246MnMgZWFzZS1pbiBmYWRlaW47YW5pbWF0aW9uOjJzIGVhc2UtaW4gZmFkZWlufS5jb250YWluZXItbG9hZGVyIC5zcGlubmVyIGRpdntwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoyMHB4O2hlaWdodDoyMHB4O2JvcmRlci1yYWRpdXM6NTAlO29wYWNpdHk6Ljc7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpfS5jb250YWluZXItbG9hZGVyIC5zcGlubmVyIGRpdjpudGgtY2hpbGQoMSl7YmFja2dyb3VuZC1jb2xvcjojZmZkMjAwOy13ZWJraXQtYW5pbWF0aW9uOjEuOHMgZWFzZS1pbi1vdXQgMS4ycyBpbmZpbml0ZSBzcGluO2FuaW1hdGlvbjoxLjhzIGVhc2UtaW4tb3V0IDEuMnMgaW5maW5pdGUgc3BpbjtoZWlnaHQ6MjRweDt3aWR0aDoyNHB4fS5jb250YWluZXItbG9hZGVyIC5zcGlubmVyIGRpdjpudGgtY2hpbGQoMil7YmFja2dyb3VuZC1jb2xvcjojNTg1OTViOy13ZWJraXQtYW5pbWF0aW9uOjEuOHMgZWFzZS1pbi1vdXQgLjZzIGluZmluaXRlIHNwaW47YW5pbWF0aW9uOjEuOHMgZWFzZS1pbi1vdXQgLjZzIGluZmluaXRlIHNwaW47aGVpZ2h0OjIwcHg7d2VpZ2h0OjIwcHh9LmNvbnRhaW5lci1sb2FkZXIgLnNwaW5uZXIgZGl2Om50aC1jaGlsZCgzKXtiYWNrZ3JvdW5kLWNvbG9yOiMwMDQ0OGQ7LXdlYmtpdC1hbmltYXRpb246MS44cyBlYXNlLWluLW91dCBpbmZpbml0ZSBzcGluO2FuaW1hdGlvbjoxLjhzIGVhc2UtaW4tb3V0IGluZmluaXRlIHNwaW47d2lkdGg6MTBweDtoZWlnaHQ6MTBweH1ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbnswJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCl9MzMuMyV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgzMHB4KTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgzMHB4KX02Ni43JXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDApIHRyYW5zbGF0ZVkoLTQ1cHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDApIHRyYW5zbGF0ZVkoLTQ1cHgpfTEwMCV7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCkgdHJhbnNsYXRlWSgwKTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtMzBweCkgdHJhbnNsYXRlWSgwKX19QGtleWZyYW1lcyBzcGluezAley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTMwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KX0zMy4zJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKDMwcHgpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKDMwcHgpfTY2Ljcley13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoMCkgdHJhbnNsYXRlWSgtNDVweCk7dHJhbnNmb3JtOnRyYW5zbGF0ZVgoMCkgdHJhbnNsYXRlWSgtNDVweCl9MTAwJXstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KSB0cmFuc2xhdGVZKDApO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC0zMHB4KSB0cmFuc2xhdGVZKDApfX1ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZWlue2Zyb217b3BhY2l0eTowfXRve29wYWNpdHk6MX19QGtleWZyYW1lcyBmYWRlaW57ZnJvbXtvcGFjaXR5OjB9dG97b3BhY2l0eToxfX1ALXdlYmtpdC1rZXlmcmFtZXMgZmFkZW91dHtmcm9te29wYWNpdHk6MX10b3tvcGFjaXR5OjB9fUBrZXlmcmFtZXMgZmFkZW91dHtmcm9te29wYWNpdHk6MX10b3tvcGFjaXR5OjB9fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2FkaW5nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=