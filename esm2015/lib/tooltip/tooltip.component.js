/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class TooltipComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
TooltipComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-tooltip',
                template: `<a  class="tooltip" title="{{ title}}">
  <i class="{{ icon }}"></i>
</a>`,
                styles: [`.tooltip{display:inline;position:relative}.tooltip:hover:after{background:rgba(0,0,0,.8);border-radius:5px;bottom:26px;color:#fff;content:attr(title);left:-600%;padding:5px 15px;position:absolute;z-index:98;width:220px}.tooltip:hover:before{border:solid;border-color:#333 transparent;border-width:6px 6px 0;bottom:20px;content:"";left:0;position:absolute;z-index:99}.tooltip i{font-size:1.2em;letter-spacing:15px;cursor:pointer}`]
            },] },
];
/** @nocollapse */
TooltipComponent.ctorParameters = () => [];
TooltipComponent.propDecorators = {
    title: [{ type: Input }],
    icon: [{ type: Input }]
};
function TooltipComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TooltipComponent.prototype.title;
    /** @type {?} */
    TooltipComponent.prototype.icon;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi90b29sdGlwL3Rvb2x0aXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVN6RCxNQUFNO0lBS0osaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBZkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2dCQUN2QixRQUFRLEVBQUU7O0tBRVA7Z0JBQ0gsTUFBTSxFQUFFLENBQUMsOGFBQThhLENBQUM7YUFDemI7Ozs7O29CQUdFLEtBQUs7bUJBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtdG9vbHRpcCcsXHJcbiAgdGVtcGxhdGU6IGA8YSAgY2xhc3M9XCJ0b29sdGlwXCIgdGl0bGU9XCJ7eyB0aXRsZX19XCI+XHJcbiAgPGkgY2xhc3M9XCJ7eyBpY29uIH19XCI+PC9pPlxyXG48L2E+YCxcclxuICBzdHlsZXM6IFtgLnRvb2x0aXB7ZGlzcGxheTppbmxpbmU7cG9zaXRpb246cmVsYXRpdmV9LnRvb2x0aXA6aG92ZXI6YWZ0ZXJ7YmFja2dyb3VuZDpyZ2JhKDAsMCwwLC44KTtib3JkZXItcmFkaXVzOjVweDtib3R0b206MjZweDtjb2xvcjojZmZmO2NvbnRlbnQ6YXR0cih0aXRsZSk7bGVmdDotNjAwJTtwYWRkaW5nOjVweCAxNXB4O3Bvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6OTg7d2lkdGg6MjIwcHh9LnRvb2x0aXA6aG92ZXI6YmVmb3Jle2JvcmRlcjpzb2xpZDtib3JkZXItY29sb3I6IzMzMyB0cmFuc3BhcmVudDtib3JkZXItd2lkdGg6NnB4IDZweCAwO2JvdHRvbToyMHB4O2NvbnRlbnQ6XCJcIjtsZWZ0OjA7cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDo5OX0udG9vbHRpcCBpe2ZvbnQtc2l6ZToxLjJlbTtsZXR0ZXItc3BhY2luZzoxNXB4O2N1cnNvcjpwb2ludGVyfWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUb29sdGlwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgdGl0bGU6c3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246c3RyaW5nO1xyXG4gIFxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19