/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
export class FooterComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FooterComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-footer',
                template: `<footer>
  <div class="contact-information text-center">
    <div>
      <i class="footer-icon fal fa-phone"></i>
      Sucursal Telefónica : 507-306-4700
    </div>
    <div>Copyright Banistmo SA. 2018</div>
  </div>
</footer>



`,
                styles: [`footer{width:100%;background-color:#e6e7e8;padding:25px 0}footer .contact-information{font-size:.8em}footer .contact-information i{-webkit-transform:scaleX(-1);transform:scaleX(-1)}`]
            },] },
];
/** @nocollapse */
FooterComponent.ctorParameters = () => [];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQW1CekQsTUFBTTtJQUVKLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7OztZQXRCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7O0NBWVg7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsdUxBQXVMLENBQUM7YUFDbE0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWZvb3RlcicsXHJcbiAgdGVtcGxhdGU6IGA8Zm9vdGVyPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWN0LWluZm9ybWF0aW9uIHRleHQtY2VudGVyXCI+XHJcbiAgICA8ZGl2PlxyXG4gICAgICA8aSBjbGFzcz1cImZvb3Rlci1pY29uIGZhbCBmYS1waG9uZVwiPjwvaT5cclxuICAgICAgU3VjdXJzYWwgVGVsZWbDs25pY2EgOiA1MDctMzA2LTQ3MDBcclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdj5Db3B5cmlnaHQgQmFuaXN0bW8gU0EuIDIwMTg8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC9mb290ZXI+XHJcblxyXG5cclxuXHJcbmAsXHJcbiAgc3R5bGVzOiBbYGZvb3Rlcnt3aWR0aDoxMDAlO2JhY2tncm91bmQtY29sb3I6I2U2ZTdlODtwYWRkaW5nOjI1cHggMH1mb290ZXIgLmNvbnRhY3QtaW5mb3JtYXRpb257Zm9udC1zaXplOi44ZW19Zm9vdGVyIC5jb250YWN0LWluZm9ybWF0aW9uIGl7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGVYKC0xKTt0cmFuc2Zvcm06c2NhbGVYKC0xKX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9vdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=