/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
export class TextFieldComponent {
    constructor() {
        this.valueUsername = new EventEmitter();
    }
    /**
     * @return {?}
     */
    getValueUsername() {
        this.valueUsername.emit({
            value: this.valueInput,
            state: this.inputStyle
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    validateField() {
        if (this.valueInput !== null && this.valueInput !== '') {
            this.inputStyle = 'status';
        }
        else {
            this.inputStyle = '';
        }
        this.getValueUsername();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onSetStateChange(event) {
        this.state = event.state;
        this.inputStyle = event.state;
        if (event.state !== '') {
            this.message = 'No cumple con la longitud mínima';
            this.iconSecondary = 'fal fa-times';
        }
        else {
            this.message = '';
            this.iconSecondary = '';
        }
        if (this.valueInput === '') {
            this.message = '';
            this.state = '';
            this.iconSecondary = '';
        }
        this.getValueUsername();
    }
}
TextFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-text-field',
                template: `<div class="text-field {{ state }}">
  <div class="content">
    <label>
      <input type="text" id="{{ id }}" class="{{ inputStyle }}" [(ngModel)]="valueInput" name="text-field" [required]="true"
      input-directive [minLength]=8 [maxLength]=16 [pattern]="pattern" (setState)="onSetStateChange($event)" (keyup)="validateField()" [disabled]="(state == 'disabled')">
      <span>{{ label }}</span>
      <div class="icon">
        <i class="{{ iconPrimary }}"></i>
        <i class="{{ iconSecondary }}"></i>
      </div>
    </label>
  </div>
  <div class="message">
    <p>{{ message }}</p>
  </div>
</div>
`,
                styles: [`.text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:15px;cursor:pointer}.text-field .content .icon i:nth-child(2){color:#fa5e5b}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}`],
            },] },
];
/** @nocollapse */
TextFieldComponent.ctorParameters = () => [];
TextFieldComponent.propDecorators = {
    valueUsername: [{ type: Output }],
    label: [{ type: Input }],
    iconPrimary: [{ type: Input }],
    iconSecondary: [{ type: Input }],
    state: [{ type: Input }],
    message: [{ type: Input }],
    pattern: [{ type: Input }],
    id: [{ type: Input }]
};
function TextFieldComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TextFieldComponent.prototype.valueInput;
    /** @type {?} */
    TextFieldComponent.prototype.inputStyle;
    /** @type {?} */
    TextFieldComponent.prototype.valueUsername;
    /** @type {?} */
    TextFieldComponent.prototype.label;
    /** @type {?} */
    TextFieldComponent.prototype.iconPrimary;
    /** @type {?} */
    TextFieldComponent.prototype.iconSecondary;
    /** @type {?} */
    TextFieldComponent.prototype.state;
    /** @type {?} */
    TextFieldComponent.prototype.message;
    /** @type {?} */
    TextFieldComponent.prototype.pattern;
    /** @type {?} */
    TextFieldComponent.prototype.id;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi90ZXh0LWZpZWxkL3RleHQtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBVSxZQUFZLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQXdCckMsTUFBTTtJQW1CSjs2QkFoQmlDLElBQUksWUFBWSxFQUFFO0tBaUJsRDs7OztJQVJELGdCQUFnQjtRQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO1lBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVTtZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVU7U0FDdkIsQ0FBQyxDQUFDO0tBQ0o7Ozs7SUFLRCxRQUFRO0tBQ1A7Ozs7SUFFRCxhQUFhO1FBQ1gsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO1NBQzVCO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztTQUN0QjtRQUNELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0tBQ3pCOzs7OztJQUVELGdCQUFnQixDQUFDLEtBQUs7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUM5QixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxrQ0FBa0MsQ0FBQztZQUNsRCxJQUFJLENBQUMsYUFBYSxHQUFHLGNBQWMsQ0FBQztTQUNyQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7U0FDekI7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7U0FDekI7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztLQUN6Qjs7O1lBekVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Q0FnQlg7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsOHJEQUE4ckQsQ0FBQzthQUV6c0Q7Ozs7OzRCQUlFLE1BQU07b0JBQ04sS0FBSzswQkFDTCxLQUFLOzRCQUNMLEtBQUs7b0JBQ0wsS0FBSztzQkFDTCxLQUFLO3NCQUNMLEtBQUs7aUJBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge091dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10ZXh0LWZpZWxkJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJ0ZXh0LWZpZWxkIHt7IHN0YXRlIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgIDxsYWJlbD5cclxuICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJ7eyBpZCB9fVwiIGNsYXNzPVwie3sgaW5wdXRTdHlsZSB9fVwiIFsobmdNb2RlbCldPVwidmFsdWVJbnB1dFwiIG5hbWU9XCJ0ZXh0LWZpZWxkXCIgW3JlcXVpcmVkXT1cInRydWVcIlxyXG4gICAgICBpbnB1dC1kaXJlY3RpdmUgW21pbkxlbmd0aF09OCBbbWF4TGVuZ3RoXT0xNiBbcGF0dGVybl09XCJwYXR0ZXJuXCIgKHNldFN0YXRlKT1cIm9uU2V0U3RhdGVDaGFuZ2UoJGV2ZW50KVwiIChrZXl1cCk9XCJ2YWxpZGF0ZUZpZWxkKClcIiBbZGlzYWJsZWRdPVwiKHN0YXRlID09ICdkaXNhYmxlZCcpXCI+XHJcbiAgICAgIDxzcGFuPnt7IGxhYmVsIH19PC9zcGFuPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwie3sgaWNvblByaW1hcnkgfX1cIj48L2k+XHJcbiAgICAgICAgPGkgY2xhc3M9XCJ7eyBpY29uU2Vjb25kYXJ5IH19XCI+PC9pPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvbGFiZWw+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj5cclxuICAgIDxwPnt7IG1lc3NhZ2UgfX08L3A+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AudGV4dC1maWVsZHttYXJnaW4tYm90dG9tOjEwcHh9LnRleHQtZmllbGQuZGlzYWJsZWR7b3BhY2l0eTouM30udGV4dC1maWVsZC5kaXNhYmxlZCAuY29udGVudCwudGV4dC1maWVsZC5kaXNhYmxlZCBpbnB1dCwudGV4dC1maWVsZC5kaXNhYmxlZCBsYWJlbCwudGV4dC1maWVsZC5kaXNhYmxlZCBzcGFue2N1cnNvcjpub3QtYWxsb3dlZCFpbXBvcnRhbnR9LnRleHQtZmllbGQuZXJyb3IgaW5wdXQ6Zm9jdXMrc3BhbiwudGV4dC1maWVsZC5lcnJvciBwLC50ZXh0LWZpZWxkLmVycm9yIHNwYW57Y29sb3I6I2ZhNWU1YiFpbXBvcnRhbnR9LnRleHQtZmllbGQuZXJyb3IgaW5wdXR7Ym9yZGVyLWJvdHRvbToycHggc29saWQgI2ZhNWU1YiFpbXBvcnRhbnR9LnRleHQtZmllbGQgLmNvbnRlbnR7d2lkdGg6MTAwJTtjb2xvcjojNDU0NjQ4O2JhY2tncm91bmQtY29sb3I6I2Y3ZjdmNztib3JkZXItcmFkaXVzOjZweCA2cHggMCAwfS50ZXh0LWZpZWxkIC5jb250ZW50IGxhYmVse3Bvc2l0aW9uOnJlbGF0aXZlO2Rpc3BsYXk6YmxvY2s7d2lkdGg6MTAwJTttaW4taGVpZ2h0OjUwcHh9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXR7cG9zaXRpb246YWJzb2x1dGU7dG9wOjE1cHg7ei1pbmRleDoxO3dpZHRoOjEwMCU7Zm9udC1zaXplOjFlbTtib3JkZXI6MDtib3JkZXItYm90dG9tOjFweCBzb2xpZCAjODA4Mjg1O3RyYW5zaXRpb246Ym9yZGVyLWNvbG9yIC4ycyBlYXNlLWluLW91dDtvdXRsaW5lOjA7aGVpZ2h0OjM1cHg7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6Zm9jdXN7Ym9yZGVyLWJvdHRvbToycHggc29saWQgI2ZmZDIwMH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OnZhbGlkK3NwYW57dG9wOjZweDtjdXJzb3I6aW5oZXJpdDtmb250LXNpemU6LjhlbTtjb2xvcjojODA4Mjg1O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IC5zdGF0dXN7Ym9yZGVyLWJvdHRvbToycHggc29saWQgIzAwNDQ4ZH0udGV4dC1maWVsZCAuY29udGVudCBzcGFue3Bvc2l0aW9uOmFic29sdXRlO2Rpc3BsYXk6YmxvY2s7dG9wOjIwcHg7ei1pbmRleDoyO2ZvbnQtc2l6ZToxZW07dHJhbnNpdGlvbjphbGwgLjJzIGVhc2UtaW4tb3V0O3dpZHRoOjEwMCU7Y3Vyc29yOnRleHQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29ue3otaW5kZXg6MzttYXJnaW46MTVweCAwIDA7cmlnaHQ6MDtwb3NpdGlvbjphYnNvbHV0ZX0udGV4dC1maWVsZCAuY29udGVudCAuaWNvbiBpe2ZvbnQtc2l6ZToxLjJlbTtsZXR0ZXItc3BhY2luZzoxNXB4O2N1cnNvcjpwb2ludGVyfS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29uIGk6bnRoLWNoaWxkKDIpe2NvbG9yOiNmYTVlNWJ9LnRleHQtZmllbGQgLm1lc3NhZ2V7bWluLWhlaWdodDoyMHB4fS50ZXh0LWZpZWxkIC5tZXNzYWdlIHB7cGFkZGluZzowIDAgMCAxNXB4O2ZvbnQtc2l6ZTouOGVtO2NvbG9yOiM4MDgyODU7bWFyZ2luOi0zcHggMCAwfWBdLFxyXG5cclxufSlcclxuZXhwb3J0IGNsYXNzIFRleHRGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcHVibGljIHZhbHVlSW5wdXQ7XHJcbiAgaW5wdXRTdHlsZTogc3RyaW5nO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgdmFsdWVVc2VybmFtZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb25QcmltYXJ5OiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvblNlY29uZGFyeTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHN0YXRlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbWVzc2FnZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHBhdHRlcm46IHN0cmluZztcclxuICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG5cclxuICBnZXRWYWx1ZVVzZXJuYW1lKCkge1xyXG4gICAgdGhpcy52YWx1ZVVzZXJuYW1lLmVtaXQoe1xyXG4gICAgICB2YWx1ZTogdGhpcy52YWx1ZUlucHV0LFxyXG4gICAgICBzdGF0ZTogdGhpcy5pbnB1dFN0eWxlXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZpZWxkKCkge1xyXG4gICAgaWYgKHRoaXMudmFsdWVJbnB1dCAhPT0gbnVsbCAmJiB0aGlzLnZhbHVlSW5wdXQgIT09ICcnKSB7XHJcbiAgICAgIHRoaXMuaW5wdXRTdHlsZSA9ICdzdGF0dXMnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pbnB1dFN0eWxlID0gJyc7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdldFZhbHVlVXNlcm5hbWUoKTtcclxuICB9XHJcblxyXG4gIG9uU2V0U3RhdGVDaGFuZ2UoZXZlbnQpIHtcclxuICAgIHRoaXMuc3RhdGUgPSBldmVudC5zdGF0ZTtcclxuICAgIHRoaXMuaW5wdXRTdHlsZSA9IGV2ZW50LnN0YXRlO1xyXG4gICAgaWYgKGV2ZW50LnN0YXRlICE9PSAnJykge1xyXG4gICAgICB0aGlzLm1lc3NhZ2UgPSAnTm8gY3VtcGxlIGNvbiBsYSBsb25naXR1ZCBtw61uaW1hJztcclxuICAgICAgdGhpcy5pY29uU2Vjb25kYXJ5ID0gJ2ZhbCBmYS10aW1lcyc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm1lc3NhZ2UgPSAnJztcclxuICAgICAgdGhpcy5pY29uU2Vjb25kYXJ5ID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMudmFsdWVJbnB1dCA9PT0gJycpIHtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICAgIHRoaXMuc3RhdGUgPSAnJztcclxuICAgICAgdGhpcy5pY29uU2Vjb25kYXJ5ID0gJyc7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdldFZhbHVlVXNlcm5hbWUoKTtcclxuICB9XHJcblxyXG59XHJcblxyXG5cclxuIl19