/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class PasswordFieldComponent {
    constructor() {
        this.valuePassword = new EventEmitter();
        this.class = '';
    }
    /**
     * @return {?}
     */
    getValuePassword() {
        this.valuePassword.emit(this.valueInputPassword);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.type = 'password';
    }
    /**
     * @return {?}
     */
    validateField() {
        this.getValuePassword();
        (this.valueInputPassword !== null && this.valueInputPassword !== '') ? this.emptyInput = 'status' : this.emptyInput = '';
    }
    /**
     * @return {?}
     */
    clearField() {
        this.valueInputPassword = '';
        this.state = '';
        this.message = '';
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onSetStateChange(event) {
        this.state = event.state;
        if (event.state !== '') {
            this.message = 'No cumple con la longitud mínima';
        }
        else {
            this.message = '';
        }
    }
}
PasswordFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-password-field',
                template: `<div class="text-field {{ state }}">
  <div class="content">
    <label>
      <input type="{{ type }}" id="{{ id }}" class="{{ emptyInput }}" [(ngModel)]="valueInputPassword" name="text-field" required (change)="validateField()"
        autocomplete="off" onpaste="return false" onCopy="return false" input-directive [minLength]=8 [maxLength]=16 [pattern]="pattern"
        (setState)="onSetStateChange($event)">
      <span>{{ label }}</span>
      <div class="icon">
        <i [class]="'fal fa-eye' + class" (mousedown)="class='-slash';type='text'" (mouseup)="class='';type='password'" ></i>
        <i *ngIf="state !== 'error'" class="fal fa-info-circle"></i>
        <i *ngIf="state === 'error'" class="fal fa-times error" (click)="clearField()"></i>
      </div>
    </label>
  </div>
  <div class="message">
    <p>{{ message }}</p>
  </div>
</div>
`,
                styles: [`.text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon .error,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:8px;cursor:pointer}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}`]
            },] },
];
/** @nocollapse */
PasswordFieldComponent.ctorParameters = () => [];
PasswordFieldComponent.propDecorators = {
    valuePassword: [{ type: Output }],
    label: [{ type: Input }],
    state: [{ type: Input }],
    message: [{ type: Input }],
    type: [{ type: Input }],
    pattern: [{ type: Input }],
    icon: [{ type: Input }],
    id: [{ type: Input }]
};
function PasswordFieldComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    PasswordFieldComponent.prototype.valueInputPassword;
    /** @type {?} */
    PasswordFieldComponent.prototype.emptyInput;
    /** @type {?} */
    PasswordFieldComponent.prototype.valuePassword;
    /** @type {?} */
    PasswordFieldComponent.prototype.label;
    /** @type {?} */
    PasswordFieldComponent.prototype.state;
    /** @type {?} */
    PasswordFieldComponent.prototype.message;
    /** @type {?} */
    PasswordFieldComponent.prototype.type;
    /** @type {?} */
    PasswordFieldComponent.prototype.pattern;
    /** @type {?} */
    PasswordFieldComponent.prototype.icon;
    /** @type {?} */
    PasswordFieldComponent.prototype.id;
    /** @type {?} */
    PasswordFieldComponent.prototype.class;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFzc3dvcmQtZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvcGFzc3dvcmQtZmllbGQvcGFzc3dvcmQtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBeUI5RSxNQUFNO0lBZUo7NkJBWGlDLElBQUksWUFBWSxFQUFFO3FCQVNwQyxFQUFFO0tBSWhCOzs7O0lBQ0QsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7S0FDbEQ7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7S0FFeEI7Ozs7SUFFRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDekIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO0tBQ3pIOzs7O0lBQ0QsVUFBVTtRQUNSLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7S0FDbkI7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsS0FBSztRQUNwQixJQUFJLENBQUMsS0FBSyxHQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDdkIsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFLENBQUMsQ0FBQSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsa0NBQWtDLENBQUE7U0FDbEQ7UUFBQyxJQUFJLENBQUEsQ0FBQztZQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1NBQ25CO0tBQ0Y7OztZQW5FRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0FrQlg7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsb3FEQUFvcUQsQ0FBQzthQUMvcUQ7Ozs7OzRCQUtFLE1BQU07b0JBQ04sS0FBSztvQkFDTCxLQUFLO3NCQUNMLEtBQUs7bUJBQ0wsS0FBSztzQkFDTCxLQUFLO21CQUNMLEtBQUs7aUJBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtcGFzc3dvcmQtZmllbGQnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cInRleHQtZmllbGQge3sgc3RhdGUgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxyXG4gICAgPGxhYmVsPlxyXG4gICAgICA8aW5wdXQgdHlwZT1cInt7IHR5cGUgfX1cIiBpZD1cInt7IGlkIH19XCIgY2xhc3M9XCJ7eyBlbXB0eUlucHV0IH19XCIgWyhuZ01vZGVsKV09XCJ2YWx1ZUlucHV0UGFzc3dvcmRcIiBuYW1lPVwidGV4dC1maWVsZFwiIHJlcXVpcmVkIChjaGFuZ2UpPVwidmFsaWRhdGVGaWVsZCgpXCJcclxuICAgICAgICBhdXRvY29tcGxldGU9XCJvZmZcIiBvbnBhc3RlPVwicmV0dXJuIGZhbHNlXCIgb25Db3B5PVwicmV0dXJuIGZhbHNlXCIgaW5wdXQtZGlyZWN0aXZlIFttaW5MZW5ndGhdPTggW21heExlbmd0aF09MTYgW3BhdHRlcm5dPVwicGF0dGVyblwiXHJcbiAgICAgICAgKHNldFN0YXRlKT1cIm9uU2V0U3RhdGVDaGFuZ2UoJGV2ZW50KVwiPlxyXG4gICAgICA8c3Bhbj57eyBsYWJlbCB9fTwvc3Bhbj5cclxuICAgICAgPGRpdiBjbGFzcz1cImljb25cIj5cclxuICAgICAgICA8aSBbY2xhc3NdPVwiJ2ZhbCBmYS1leWUnICsgY2xhc3NcIiAobW91c2Vkb3duKT1cImNsYXNzPSctc2xhc2gnO3R5cGU9J3RleHQnXCIgKG1vdXNldXApPVwiY2xhc3M9Jyc7dHlwZT0ncGFzc3dvcmQnXCIgPjwvaT5cclxuICAgICAgICA8aSAqbmdJZj1cInN0YXRlICE9PSAnZXJyb3InXCIgY2xhc3M9XCJmYWwgZmEtaW5mby1jaXJjbGVcIj48L2k+XHJcbiAgICAgICAgPGkgKm5nSWY9XCJzdGF0ZSA9PT0gJ2Vycm9yJ1wiIGNsYXNzPVwiZmFsIGZhLXRpbWVzIGVycm9yXCIgKGNsaWNrKT1cImNsZWFyRmllbGQoKVwiPjwvaT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2xhYmVsPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJtZXNzYWdlXCI+XHJcbiAgICA8cD57eyBtZXNzYWdlIH19PC9wPlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLnRleHQtZmllbGR7bWFyZ2luLWJvdHRvbToxMHB4fS50ZXh0LWZpZWxkLmRpc2FibGVke29wYWNpdHk6LjN9LnRleHQtZmllbGQuZGlzYWJsZWQgLmNvbnRlbnQsLnRleHQtZmllbGQuZGlzYWJsZWQgaW5wdXQsLnRleHQtZmllbGQuZGlzYWJsZWQgbGFiZWwsLnRleHQtZmllbGQuZGlzYWJsZWQgc3BhbntjdXJzb3I6bm90LWFsbG93ZWQhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIC5pY29uIC5lcnJvciwudGV4dC1maWVsZC5lcnJvciBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkLmVycm9yIHAsLnRleHQtZmllbGQuZXJyb3Igc3Bhbntjb2xvcjojZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciBpbnB1dHtib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZCAuY29udGVudHt3aWR0aDoxMDAlO2NvbG9yOiM0NTQ2NDg7YmFja2dyb3VuZC1jb2xvcjojZjdmN2Y3O2JvcmRlci1yYWRpdXM6NnB4IDZweCAwIDB9LnRleHQtZmllbGQgLmNvbnRlbnQgbGFiZWx7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt3aWR0aDoxMDAlO21pbi1oZWlnaHQ6NTBweH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTVweDt6LWluZGV4OjE7d2lkdGg6MTAwJTtmb250LXNpemU6MWVtO2JvcmRlcjowO2JvcmRlci1ib3R0b206MXB4IHNvbGlkICM4MDgyODU7dHJhbnNpdGlvbjpib3JkZXItY29sb3IgLjJzIGVhc2UtaW4tb3V0O291dGxpbmU6MDtoZWlnaHQ6MzVweDtiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCBpbnB1dDpmb2N1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmZkMjAwfS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6dmFsaWQrc3Bhbnt0b3A6NnB4O2N1cnNvcjppbmhlcml0O2ZvbnQtc2l6ZTouOGVtO2NvbG9yOiM4MDgyODU7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLnN0YXR1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjMDA0NDhkfS50ZXh0LWZpZWxkIC5jb250ZW50IHNwYW57cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MjBweDt6LWluZGV4OjI7Zm9udC1zaXplOjFlbTt0cmFuc2l0aW9uOmFsbCAuMnMgZWFzZS1pbi1vdXQ7d2lkdGg6MTAwJTtjdXJzb3I6dGV4dDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb257ei1pbmRleDozO21hcmdpbjoxNXB4IDAgMDtyaWdodDowO3Bvc2l0aW9uOmFic29sdXRlfS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29uIGl7Zm9udC1zaXplOjEuMmVtO2xldHRlci1zcGFjaW5nOjhweDtjdXJzb3I6cG9pbnRlcn0udGV4dC1maWVsZCAubWVzc2FnZXttaW4taGVpZ2h0OjIwcHh9LnRleHQtZmllbGQgLm1lc3NhZ2UgcHtwYWRkaW5nOjAgMCAwIDE1cHg7Zm9udC1zaXplOi44ZW07Y29sb3I6IzgwODI4NTttYXJnaW46LTNweCAwIDB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBhc3N3b3JkRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICB2YWx1ZUlucHV0UGFzc3dvcmQ6IGFueTtcclxuICBlbXB0eUlucHV0OiBzdHJpbmc7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyB2YWx1ZVBhc3N3b3JkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgc3RhdGU6IHN0cmluZztcclxuICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHBhdHRlcm46IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuXHJcbiAgcHVibGljIGNsYXNzID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICB9XHJcbiAgZ2V0VmFsdWVQYXNzd29yZCgpIHtcclxuICAgIHRoaXMudmFsdWVQYXNzd29yZC5lbWl0KHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy50eXBlID0gJ3Bhc3N3b3JkJztcclxuXHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZpZWxkKCkge1xyXG4gICAgdGhpcy5nZXRWYWx1ZVBhc3N3b3JkKCk7XHJcbiAgICh0aGlzLnZhbHVlSW5wdXRQYXNzd29yZCAhPT0gbnVsbCAmJiB0aGlzLnZhbHVlSW5wdXRQYXNzd29yZCAhPT0gJycpID8gdGhpcy5lbXB0eUlucHV0ID0gJ3N0YXR1cycgOiB0aGlzLmVtcHR5SW5wdXQgPSAnJztcclxuICB9XHJcbiAgY2xlYXJGaWVsZCgpIHtcclxuICAgIHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkID0gJyc7XHJcbiAgICB0aGlzLnN0YXRlID0gJyc7XHJcbiAgICB0aGlzLm1lc3NhZ2UgPSAnJztcclxuICB9XHJcblxyXG4gIG9uU2V0U3RhdGVDaGFuZ2UoZXZlbnQpe1xyXG4gICAgdGhpcy5zdGF0ZT1ldmVudC5zdGF0ZTtcclxuICAgIGlmKGV2ZW50LnN0YXRlICE9PSAnJyl7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICdObyBjdW1wbGUgY29uIGxhIGxvbmdpdHVkIG3DrW5pbWEnXHJcbiAgICB9IGVsc2V7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICcnO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19