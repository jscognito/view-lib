/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class OutlineButtonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
OutlineButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-outline-button',
                template: `<button class="btn-outline" id="{{ id }}">
  <i class="icon-left {{ icon }}"></i>
  <ng-content></ng-content>
</button>
`,
                styles: [`.btn-outline{width:287px;height:59px;border-radius:29.5px;border:1px solid #3a3b3b;background-color:transparent;font-size:.85em;font-weight:700;padding:0 15px;margin-top:10px;cursor:pointer}.btn-outline:hover{border:2px solid #3a3b3b}.btn-outline .icon-left{font-size:1.8em;width:30px;vertical-align:bottom;line-height:10px}`]
            },] },
];
/** @nocollapse */
OutlineButtonComponent.ctorParameters = () => [];
OutlineButtonComponent.propDecorators = {
    id: [{ type: Input }],
    icon: [{ type: Input }]
};
function OutlineButtonComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    OutlineButtonComponent.prototype.id;
    /** @type {?} */
    OutlineButtonComponent.prototype.icon;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3V0bGluZS1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvb3V0bGluZS1idXR0b24vb3V0bGluZS1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQVd2RCxNQUFNO0lBSUosaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBaEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixRQUFRLEVBQUU7Ozs7Q0FJWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyxzVUFBc1UsQ0FBQzthQUNqVjs7Ozs7aUJBRUUsS0FBSzttQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtb3V0bGluZS1idXR0b24nLFxyXG4gIHRlbXBsYXRlOiBgPGJ1dHRvbiBjbGFzcz1cImJ0bi1vdXRsaW5lXCIgaWQ9XCJ7eyBpZCB9fVwiPlxyXG4gIDxpIGNsYXNzPVwiaWNvbi1sZWZ0IHt7IGljb24gfX1cIj48L2k+XHJcbiAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG48L2J1dHRvbj5cclxuYCxcclxuICBzdHlsZXM6IFtgLmJ0bi1vdXRsaW5le3dpZHRoOjI4N3B4O2hlaWdodDo1OXB4O2JvcmRlci1yYWRpdXM6MjkuNXB4O2JvcmRlcjoxcHggc29saWQgIzNhM2IzYjtiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2ZvbnQtc2l6ZTouODVlbTtmb250LXdlaWdodDo3MDA7cGFkZGluZzowIDE1cHg7bWFyZ2luLXRvcDoxMHB4O2N1cnNvcjpwb2ludGVyfS5idG4tb3V0bGluZTpob3Zlcntib3JkZXI6MnB4IHNvbGlkICMzYTNiM2J9LmJ0bi1vdXRsaW5lIC5pY29uLWxlZnR7Zm9udC1zaXplOjEuOGVtO3dpZHRoOjMwcHg7dmVydGljYWwtYWxpZ246Ym90dG9tO2xpbmUtaGVpZ2h0OjEwcHh9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE91dGxpbmVCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==