/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { TabComponent } from '../tab/tab.component';
export class TabsComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        /** @type {?} */
        const activeTabs = this.tabs.filter((tab) => tab.active);
        // if there is no active tab set, activate the first
        if (activeTabs.length === 0) {
            this.selectTab(this.tabs.first);
        }
    }
    /**
     * @param {?} tab
     * @return {?}
     */
    selectTab(tab) {
        // deactivate all tabs
        this.tabs.toArray().forEach(tab => tab.active = false);
        // activate the tab the user has clicked on.
        tab.active = true;
    }
}
TabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-tabs',
                template: `<div class="nav nav-tabs">
  <span *ngFor="let tab of tabs" id="{{tab.id}}" (click)="selectTab(tab)" [class.active]="tab.active" class="tab"
        ngClass="{{position}}">
    <span role="button" class="tab-title">{{tab.title}}</span>
  </span>
</div>
<ng-content></ng-content>
`,
                styles: [`.nav-tabs{display:flex;flex-wrap:nowrap;font-size:1em;font-weight:700;text-transform:uppercase;border-bottom:1px solid #e6e7e8}.nav-tabs .tab{width:50%;padding:18px 10px 10px;text-align:center}.nav-tabs .center{flex:1 1;display:flex}.nav-tabs .tab-title{padding:0 7px 5px;cursor:pointer}.nav-tabs .active{border-bottom:solid #ffd200}`]
            },] },
];
/** @nocollapse */
TabsComponent.ctorParameters = () => [];
TabsComponent.propDecorators = {
    position: [{ type: Input }],
    tabs: [{ type: ContentChildren, args: [TabComponent,] }]
};
function TabsComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TabsComponent.prototype.position;
    /** @type {?} */
    TabsComponent.prototype.tabs;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi90YWJzL3RhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQTRCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV0RyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFjbEQsTUFBTTtJQUtKLGlCQUFpQjs7OztJQUVqQixRQUFRO0tBQ1A7Ozs7SUFFRCxrQkFBa0I7O1FBRWhCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7O1FBR3pELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDakM7S0FDRjs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBaUI7O1FBRXpCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQzs7UUFHeEQsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7S0FDbkI7OztZQXRDRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLFFBQVEsRUFBRTs7Ozs7OztDQU9YO2dCQUNDLE1BQU0sRUFBRSxDQUFDLCtVQUErVSxDQUFDO2FBQzFWOzs7Ozt1QkFHRSxLQUFLO21CQUNMLGVBQWUsU0FBQyxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIENvbnRlbnRDaGlsZHJlbiwgSW5wdXQsIE9uSW5pdCwgQWZ0ZXJDb250ZW50SW5pdCwgUXVlcnlMaXN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7VGFiQ29tcG9uZW50fSBmcm9tICcuLi90YWIvdGFiLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10YWJzJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJuYXYgbmF2LXRhYnNcIj5cclxuICA8c3BhbiAqbmdGb3I9XCJsZXQgdGFiIG9mIHRhYnNcIiBpZD1cInt7dGFiLmlkfX1cIiAoY2xpY2spPVwic2VsZWN0VGFiKHRhYilcIiBbY2xhc3MuYWN0aXZlXT1cInRhYi5hY3RpdmVcIiBjbGFzcz1cInRhYlwiXHJcbiAgICAgICAgbmdDbGFzcz1cInt7cG9zaXRpb259fVwiPlxyXG4gICAgPHNwYW4gcm9sZT1cImJ1dHRvblwiIGNsYXNzPVwidGFiLXRpdGxlXCI+e3t0YWIudGl0bGV9fTwvc3Bhbj5cclxuICA8L3NwYW4+XHJcbjwvZGl2PlxyXG48bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5uYXYtdGFic3tkaXNwbGF5OmZsZXg7ZmxleC13cmFwOm5vd3JhcDtmb250LXNpemU6MWVtO2ZvbnQtd2VpZ2h0OjcwMDt0ZXh0LXRyYW5zZm9ybTp1cHBlcmNhc2U7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgI2U2ZTdlOH0ubmF2LXRhYnMgLnRhYnt3aWR0aDo1MCU7cGFkZGluZzoxOHB4IDEwcHggMTBweDt0ZXh0LWFsaWduOmNlbnRlcn0ubmF2LXRhYnMgLmNlbnRlcntmbGV4OjEgMTtkaXNwbGF5OmZsZXh9Lm5hdi10YWJzIC50YWItdGl0bGV7cGFkZGluZzowIDdweCA1cHg7Y3Vyc29yOnBvaW50ZXJ9Lm5hdi10YWJzIC5hY3RpdmV7Ym9yZGVyLWJvdHRvbTpzb2xpZCAjZmZkMjAwfWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlckNvbnRlbnRJbml0ICB7XHJcblxyXG4gIEBJbnB1dCgpIHBvc2l0aW9uOiBzdHJpbmc7XHJcbiAgQENvbnRlbnRDaGlsZHJlbihUYWJDb21wb25lbnQpIHRhYnM6IFF1ZXJ5TGlzdDxUYWJDb21wb25lbnQ+O1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcclxuICAgIC8vIGdldCBhbGwgYWN0aXZlIHRhYnNcclxuICAgIGNvbnN0IGFjdGl2ZVRhYnMgPSB0aGlzLnRhYnMuZmlsdGVyKCh0YWIpID0+IHRhYi5hY3RpdmUpO1xyXG5cclxuICAgIC8vIGlmIHRoZXJlIGlzIG5vIGFjdGl2ZSB0YWIgc2V0LCBhY3RpdmF0ZSB0aGUgZmlyc3RcclxuICAgIGlmIChhY3RpdmVUYWJzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdFRhYih0aGlzLnRhYnMuZmlyc3QpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2VsZWN0VGFiKHRhYjogVGFiQ29tcG9uZW50KSB7XHJcbiAgICAvLyBkZWFjdGl2YXRlIGFsbCB0YWJzXHJcbiAgICB0aGlzLnRhYnMudG9BcnJheSgpLmZvckVhY2goIHRhYiA9PiB0YWIuYWN0aXZlID0gZmFsc2UpO1xyXG5cclxuICAgIC8vIGFjdGl2YXRlIHRoZSB0YWIgdGhlIHVzZXIgaGFzIGNsaWNrZWQgb24uXHJcbiAgICB0YWIuYWN0aXZlID0gdHJ1ZTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==