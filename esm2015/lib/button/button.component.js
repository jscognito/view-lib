/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class ButtonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-button',
                template: `<button class="btn {{ type }}">
  {{ label }}
</button>
`,
                styles: [`.btn{width:144px;height:40px;border-radius:20px;font-size:14px;font-weight:700;border:0;text-transform:uppercase}.btn.primary-gray{background-color:#58595b;color:#fff}.btn.primary-gray:hover{background-color:#363636}.btn.primary-gray:focus{background-color:#58595b}.btn.primary-yellow{background-color:#ffd200;color:#454648}.btn.primary-yellow:hover{background-color:#ffb500}.btn.primary-yellow:focus{background-color:#ffd200}.btn.primary-red{background-color:#fa5e5b;color:#fff}.btn.primary-red:hover{background-color:#d14d4a}.btn.primary-red:focus{background-color:#fa5e5b}.btn.white{background-color:#fff;color:#454648}.btn.white:hover{background-color:transparent;border:1px solid #fff}.btn.white:focus{background-color:#fff;color:#454648}.btn.secondary{border:1px solid #00448d;background-color:#fff;color:#00448d}.btn.secondary:hover{border-width:2px}.btn.secondary:focus{background-color:#e6e7e8}.btn.flat{border-radius:0;background-color:transparent;color:#333}.btn.flat:hover{color:#ffd200}.btn.flat:focus{color:#58595b}`]
            },] },
];
/** @nocollapse */
ButtonComponent.ctorParameters = () => [];
ButtonComponent.propDecorators = {
    type: [{ type: Input }],
    label: [{ type: Input }]
};
function ButtonComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    ButtonComponent.prototype.type;
    /** @type {?} */
    ButtonComponent.prototype.label;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2J1dHRvbi9idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVV6RCxNQUFNO0lBS0osaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBaEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsUUFBUSxFQUFFOzs7Q0FHWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyx1Z0NBQXVnQyxDQUFDO2FBQ2xoQzs7Ozs7bUJBR0UsS0FBSztvQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1idXR0b24nLFxyXG4gIHRlbXBsYXRlOiBgPGJ1dHRvbiBjbGFzcz1cImJ0biB7eyB0eXBlIH19XCI+XHJcbiAge3sgbGFiZWwgfX1cclxuPC9idXR0b24+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5idG57d2lkdGg6MTQ0cHg7aGVpZ2h0OjQwcHg7Ym9yZGVyLXJhZGl1czoyMHB4O2ZvbnQtc2l6ZToxNHB4O2ZvbnQtd2VpZ2h0OjcwMDtib3JkZXI6MDt0ZXh0LXRyYW5zZm9ybTp1cHBlcmNhc2V9LmJ0bi5wcmltYXJ5LWdyYXl7YmFja2dyb3VuZC1jb2xvcjojNTg1OTViO2NvbG9yOiNmZmZ9LmJ0bi5wcmltYXJ5LWdyYXk6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjojMzYzNjM2fS5idG4ucHJpbWFyeS1ncmF5OmZvY3Vze2JhY2tncm91bmQtY29sb3I6IzU4NTk1Yn0uYnRuLnByaW1hcnkteWVsbG93e2JhY2tncm91bmQtY29sb3I6I2ZmZDIwMDtjb2xvcjojNDU0NjQ4fS5idG4ucHJpbWFyeS15ZWxsb3c6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjojZmZiNTAwfS5idG4ucHJpbWFyeS15ZWxsb3c6Zm9jdXN7YmFja2dyb3VuZC1jb2xvcjojZmZkMjAwfS5idG4ucHJpbWFyeS1yZWR7YmFja2dyb3VuZC1jb2xvcjojZmE1ZTViO2NvbG9yOiNmZmZ9LmJ0bi5wcmltYXJ5LXJlZDpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiNkMTRkNGF9LmJ0bi5wcmltYXJ5LXJlZDpmb2N1c3tiYWNrZ3JvdW5kLWNvbG9yOiNmYTVlNWJ9LmJ0bi53aGl0ZXtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Y29sb3I6IzQ1NDY0OH0uYnRuLndoaXRlOmhvdmVye2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym9yZGVyOjFweCBzb2xpZCAjZmZmfS5idG4ud2hpdGU6Zm9jdXN7YmFja2dyb3VuZC1jb2xvcjojZmZmO2NvbG9yOiM0NTQ2NDh9LmJ0bi5zZWNvbmRhcnl7Ym9yZGVyOjFweCBzb2xpZCAjMDA0NDhkO2JhY2tncm91bmQtY29sb3I6I2ZmZjtjb2xvcjojMDA0NDhkfS5idG4uc2Vjb25kYXJ5OmhvdmVye2JvcmRlci13aWR0aDoycHh9LmJ0bi5zZWNvbmRhcnk6Zm9jdXN7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4fS5idG4uZmxhdHtib3JkZXItcmFkaXVzOjA7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtjb2xvcjojMzMzfS5idG4uZmxhdDpob3Zlcntjb2xvcjojZmZkMjAwfS5idG4uZmxhdDpmb2N1c3tjb2xvcjojNTg1OTVifWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=