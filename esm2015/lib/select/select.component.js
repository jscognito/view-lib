/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class SelectComponent {
    constructor() {
        this.valueSelect = 0;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    validateField() {
        (this.valueSelect !== 0) ? this.emptySelect = 'status' : this.emptySelect = '';
    }
}
SelectComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-select',
                template: `<div class="text-field {{ state }}">
  <div class="content">
    <label>
      <select class="{{ emptySelect }}" [(ngModel)]="valueSelect" name="select" required
              (change)="validateField()" [disabled]="(state == 'disabled')">

        <ng-content></ng-content>
      </select>
      <span>Seleccione</span>
      <div class="icon">
        <i class="{{ icon }}"></i>
      </div>
    </label>
  </div>
  <div class="message">
    <p>{{ message }}</p>
  </div>
</div>
`,
                styles: [`.text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content select{position:absolute;top:15px;z-index:2;width:100%;font-size:16px;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px;-moz-appearance:none;-webkit-appearance:none;appearance:none}.text-field .content select:focus{border-bottom:2px solid #ffd200}.text-field .content select:focus+span,.text-field .content select:valid+span{top:6px;cursor:inherit;font-size:13px;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:1;font-size:16px;transition:all .2s ease-in-out;width:100%;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{font-size:18px;position:absolute;z-index:3;right:15px;top:15px}.text-field .message{min-height:20px}.text-field .message p{padding:4px 0 0 15px;font-size:12px;color:#808285;margin:0}`]
            },] },
];
/** @nocollapse */
SelectComponent.ctorParameters = () => [];
SelectComponent.propDecorators = {
    label: [{ type: Input }],
    icon: [{ type: Input }],
    type: [{ type: Input }],
    state: [{ type: Input }],
    message: [{ type: Input }]
};
function SelectComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    SelectComponent.prototype.valueSelect;
    /** @type {?} */
    SelectComponent.prototype.emptySelect;
    /** @type {?} */
    SelectComponent.prototype.label;
    /** @type {?} */
    SelectComponent.prototype.icon;
    /** @type {?} */
    SelectComponent.prototype.type;
    /** @type {?} */
    SelectComponent.prototype.state;
    /** @type {?} */
    SelectComponent.prototype.message;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQXdCdkQsTUFBTTtJQVdKOzJCQVRjLENBQUM7S0FVZDs7OztJQUVELFFBQVE7S0FDUDs7OztJQUVELGFBQWE7UUFDWCxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztLQUNoRjs7O1lBMUNGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0FrQlg7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsMm9EQUEyb0QsQ0FBQzthQUN0cEQ7Ozs7O29CQU1FLEtBQUs7bUJBQ0wsS0FBSzttQkFDTCxLQUFLO29CQUNMLEtBQUs7c0JBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtc2VsZWN0JyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJ0ZXh0LWZpZWxkIHt7IHN0YXRlIH19XCI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgIDxsYWJlbD5cclxuICAgICAgPHNlbGVjdCBjbGFzcz1cInt7IGVtcHR5U2VsZWN0IH19XCIgWyhuZ01vZGVsKV09XCJ2YWx1ZVNlbGVjdFwiIG5hbWU9XCJzZWxlY3RcIiByZXF1aXJlZFxyXG4gICAgICAgICAgICAgIChjaGFuZ2UpPVwidmFsaWRhdGVGaWVsZCgpXCIgW2Rpc2FibGVkXT1cIihzdGF0ZSA9PSAnZGlzYWJsZWQnKVwiPlxyXG5cclxuICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgIDwvc2VsZWN0PlxyXG4gICAgICA8c3Bhbj5TZWxlY2Npb25lPC9zcGFuPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwie3sgaWNvbiB9fVwiPjwvaT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2xhYmVsPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJtZXNzYWdlXCI+XHJcbiAgICA8cD57eyBtZXNzYWdlIH19PC9wPlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLnRleHQtZmllbGR7bWFyZ2luLWJvdHRvbToxMHB4fS50ZXh0LWZpZWxkLmRpc2FibGVke29wYWNpdHk6LjN9LnRleHQtZmllbGQuZGlzYWJsZWQgLmNvbnRlbnQsLnRleHQtZmllbGQuZGlzYWJsZWQgaW5wdXQsLnRleHQtZmllbGQuZGlzYWJsZWQgbGFiZWwsLnRleHQtZmllbGQuZGlzYWJsZWQgc3BhbntjdXJzb3I6bm90LWFsbG93ZWQhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIC5pY29uLC50ZXh0LWZpZWxkLmVycm9yIGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQuZXJyb3IgcCwudGV4dC1maWVsZC5lcnJvciBzcGFue2NvbG9yOiNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIGlucHV0e2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkIC5jb250ZW50e3dpZHRoOjEwMCU7Y29sb3I6IzQ1NDY0ODtiYWNrZ3JvdW5kLWNvbG9yOiNmN2Y3Zjc7Ym9yZGVyLXJhZGl1czo2cHggNnB4IDAgMH0udGV4dC1maWVsZCAuY29udGVudCBsYWJlbHtwb3NpdGlvbjpyZWxhdGl2ZTtkaXNwbGF5OmJsb2NrO3dpZHRoOjEwMCU7bWluLWhlaWdodDo1MHB4fS50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdHtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MTVweDt6LWluZGV4OjI7d2lkdGg6MTAwJTtmb250LXNpemU6MTZweDtib3JkZXI6MDtib3JkZXItYm90dG9tOjFweCBzb2xpZCAjODA4Mjg1O3RyYW5zaXRpb246Ym9yZGVyLWNvbG9yIC4ycyBlYXNlLWluLW91dDtvdXRsaW5lOjA7aGVpZ2h0OjM1cHg7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHg7LW1vei1hcHBlYXJhbmNlOm5vbmU7LXdlYmtpdC1hcHBlYXJhbmNlOm5vbmU7YXBwZWFyYW5jZTpub25lfS50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdDpmb2N1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmZkMjAwfS50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkIC5jb250ZW50IHNlbGVjdDp2YWxpZCtzcGFue3RvcDo2cHg7Y3Vyc29yOmluaGVyaXQ7Zm9udC1zaXplOjEzcHg7Y29sb3I6IzgwODI4NTtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuc3RhdHVze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICMwMDQ0OGR9LnRleHQtZmllbGQgLmNvbnRlbnQgc3Bhbntwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO3RvcDoyMHB4O3otaW5kZXg6MTtmb250LXNpemU6MTZweDt0cmFuc2l0aW9uOmFsbCAuMnMgZWFzZS1pbi1vdXQ7d2lkdGg6MTAwJTtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb257Zm9udC1zaXplOjE4cHg7cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDozO3JpZ2h0OjE1cHg7dG9wOjE1cHh9LnRleHQtZmllbGQgLm1lc3NhZ2V7bWluLWhlaWdodDoyMHB4fS50ZXh0LWZpZWxkIC5tZXNzYWdlIHB7cGFkZGluZzo0cHggMCAwIDE1cHg7Zm9udC1zaXplOjEycHg7Y29sb3I6IzgwODI4NTttYXJnaW46MH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgdmFsdWVTZWxlY3QgPSAwO1xyXG4gIGVtcHR5U2VsZWN0OiBzdHJpbmc7XHJcblxyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHR5cGU6IHN0cmluZztcclxuICBASW5wdXQoKSBzdGF0ZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG4gIHZhbGlkYXRlRmllbGQoKSB7XHJcbiAgICAodGhpcy52YWx1ZVNlbGVjdCAhPT0gMCkgPyB0aGlzLmVtcHR5U2VsZWN0ID0gJ3N0YXR1cycgOiB0aGlzLmVtcHR5U2VsZWN0ID0gJyc7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=