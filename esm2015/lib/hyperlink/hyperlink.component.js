/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class HyperlinkComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
HyperlinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-hyperlink',
                template: `<a href="{{ url }}">
  {{ label }}
</a>
`,
                styles: [`a{font-size:.85em;color:#00448d;text-decoration:underline}a:hover{font-weight:700}`]
            },] },
];
/** @nocollapse */
HyperlinkComponent.ctorParameters = () => [];
HyperlinkComponent.propDecorators = {
    label: [{ type: Input }],
    url: [{ type: Input }]
};
function HyperlinkComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    HyperlinkComponent.prototype.label;
    /** @type {?} */
    HyperlinkComponent.prototype.url;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHlwZXJsaW5rLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2h5cGVybGluay9oeXBlcmxpbmsuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQVV2RCxNQUFNO0lBSUo7S0FDQzs7OztJQUVELFFBQVE7S0FDUDs7O1lBaEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsUUFBUSxFQUFFOzs7Q0FHWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyxvRkFBb0YsQ0FBQzthQUMvRjs7Ozs7b0JBRUUsS0FBSztrQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtaHlwZXJsaW5rJyxcclxuICB0ZW1wbGF0ZTogYDxhIGhyZWY9XCJ7eyB1cmwgfX1cIj5cclxuICB7eyBsYWJlbCB9fVxyXG48L2E+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYGF7Zm9udC1zaXplOi44NWVtO2NvbG9yOiMwMDQ0OGQ7dGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZX1hOmhvdmVye2ZvbnQtd2VpZ2h0OjcwMH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSHlwZXJsaW5rQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHVybDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19