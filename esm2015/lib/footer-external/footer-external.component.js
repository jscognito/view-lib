/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
export class FooterExternalComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FooterExternalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-footer-external',
                template: `<footer>
  <div class="container">
    <div class="row justify-content-md-center text-center">
      <div class="col col-lg-2">
        <a href="" id="info_schedules">Horarios</a>
      </div>
      <div class="col col-lg-2">
        <a href="" id="info_commissions">Comisiones</a>
      </div>
      <div class="col col-lg-2">
        <a href="" id="info_security">Seguridad</a>
      </div>
    </div>
  </div>
</footer>
`,
                styles: [`footer{width:100%;background-color:#e6e7e8;padding:25px 0 0}footer a{color:#454648;font-size:.8em;line-height:34px}footer a:hover{font-weight:700;text-decoration:none}`]
            },] },
];
/** @nocollapse */
FooterExternalComponent.ctorParameters = () => [];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLWV4dGVybmFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Zvb3Rlci1leHRlcm5hbC9mb290ZXItZXh0ZXJuYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBc0JsRCxNQUFNO0lBRUosaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBekJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7OztDQWVYO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHlLQUF5SyxDQUFDO2FBQ3BMIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWZvb3Rlci1leHRlcm5hbCcsXHJcbiAgdGVtcGxhdGU6IGA8Zm9vdGVyPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWluZXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJyb3cganVzdGlmeS1jb250ZW50LW1kLWNlbnRlciB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sIGNvbC1sZy0yXCI+XHJcbiAgICAgICAgPGEgaHJlZj1cIlwiIGlkPVwiaW5mb19zY2hlZHVsZXNcIj5Ib3JhcmlvczwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wgY29sLWxnLTJcIj5cclxuICAgICAgICA8YSBocmVmPVwiXCIgaWQ9XCJpbmZvX2NvbW1pc3Npb25zXCI+Q29taXNpb25lczwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wgY29sLWxnLTJcIj5cclxuICAgICAgICA8YSBocmVmPVwiXCIgaWQ9XCJpbmZvX3NlY3VyaXR5XCI+U2VndXJpZGFkPC9hPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Zvb3Rlcj5cclxuYCxcclxuICBzdHlsZXM6IFtgZm9vdGVye3dpZHRoOjEwMCU7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4O3BhZGRpbmc6MjVweCAwIDB9Zm9vdGVyIGF7Y29sb3I6IzQ1NDY0ODtmb250LXNpemU6LjhlbTtsaW5lLWhlaWdodDozNHB4fWZvb3RlciBhOmhvdmVye2ZvbnQtd2VpZ2h0OjcwMDt0ZXh0LWRlY29yYXRpb246bm9uZX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9vdGVyRXh0ZXJuYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==