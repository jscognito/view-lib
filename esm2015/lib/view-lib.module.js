/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BannerComponent } from './banner/banner.component';
import { ButtonComponent } from './button/button.component';
import { CardComponent } from './card/card.component';
import { CarouselBannerComponent } from './carousel-banner/carousel-banner.component';
import { FooterComponent } from './footer/footer.component';
import { FooterExternalComponent } from './footer-external/footer-external.component';
import { HyperlinkComponent } from './hyperlink/hyperlink.component';
import { LoadingComponent } from './loading/loading.component';
import { ModalComponent } from './modal/modal.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NotificationComponent } from './notification/notification.component';
import { OutlineButtonComponent } from './outline-button/outline-button.component';
import { PasswordFieldComponent } from './password-field/password-field.component';
import { SecondaryLinkComponent } from './secondary-link/secondary-link.component';
import { SelectComponent } from './select/select.component';
import { SubtitleComponent } from './subtitle/subtitle.component';
import { TabComponent } from './tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';
import { TextFieldComponent } from './text-field/text-field.component';
import { TitleComponent } from './title/title.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
export class ViewLibModule {
}
ViewLibModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule
                ],
                declarations: [
                    BannerComponent,
                    ButtonComponent,
                    CardComponent,
                    CarouselBannerComponent,
                    FooterComponent,
                    FooterExternalComponent,
                    HyperlinkComponent,
                    LoadingComponent,
                    ModalComponent,
                    NavbarComponent,
                    NotificationComponent,
                    OutlineButtonComponent,
                    PasswordFieldComponent,
                    SecondaryLinkComponent,
                    SelectComponent,
                    SubtitleComponent,
                    TabComponent,
                    TabsComponent,
                    TextFieldComponent,
                    TitleComponent,
                    TooltipComponent
                ],
                exports: [
                    BannerComponent,
                    ButtonComponent,
                    CardComponent,
                    CarouselBannerComponent,
                    FooterComponent,
                    FooterExternalComponent,
                    HyperlinkComponent,
                    LoadingComponent,
                    ModalComponent,
                    NavbarComponent,
                    NotificationComponent,
                    OutlineButtonComponent,
                    PasswordFieldComponent,
                    SecondaryLinkComponent,
                    SelectComponent,
                    SubtitleComponent,
                    TabComponent,
                    TabsComponent,
                    TextFieldComponent,
                    TitleComponent,
                    TooltipComponent
                ]
            },] },
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1saWIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvdmlldy1saWIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUN0RixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzVELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBc0QvQyxNQUFNOzs7WUFwREwsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLFdBQVc7aUJBQ1o7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLGVBQWU7b0JBQ2YsZUFBZTtvQkFDZixhQUFhO29CQUNiLHVCQUF1QjtvQkFDdkIsZUFBZTtvQkFDZix1QkFBdUI7b0JBQ3ZCLGtCQUFrQjtvQkFDbEIsZ0JBQWdCO29CQUNoQixjQUFjO29CQUNkLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixzQkFBc0I7b0JBQ3RCLHNCQUFzQjtvQkFDdEIsc0JBQXNCO29CQUN0QixlQUFlO29CQUNmLGlCQUFpQjtvQkFDakIsWUFBWTtvQkFDWixhQUFhO29CQUNiLGtCQUFrQjtvQkFDbEIsY0FBYztvQkFDZCxnQkFBZ0I7aUJBQ2pCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxlQUFlO29CQUNmLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYix1QkFBdUI7b0JBQ3ZCLGVBQWU7b0JBQ2YsdUJBQXVCO29CQUN2QixrQkFBa0I7b0JBQ2xCLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFDZCxlQUFlO29CQUNmLHFCQUFxQjtvQkFDckIsc0JBQXNCO29CQUN0QixzQkFBc0I7b0JBQ3RCLHNCQUFzQjtvQkFDdEIsZUFBZTtvQkFDZixpQkFBaUI7b0JBQ2pCLFlBQVk7b0JBQ1osYUFBYTtvQkFDYixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsZ0JBQWdCO2lCQUNqQjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQmFubmVyQ29tcG9uZW50IH0gZnJvbSAnLi9iYW5uZXIvYmFubmVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEJ1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4vYnV0dG9uL2J1dHRvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9jYXJkL2NhcmQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2Fyb3VzZWxCYW5uZXJDb21wb25lbnQgfSBmcm9tICcuL2Nhcm91c2VsLWJhbm5lci9jYXJvdXNlbC1iYW5uZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9vdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9mb290ZXIvZm9vdGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvb3RlckV4dGVybmFsQ29tcG9uZW50IH0gZnJvbSAnLi9mb290ZXItZXh0ZXJuYWwvZm9vdGVyLWV4dGVybmFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEh5cGVybGlua0NvbXBvbmVudCB9IGZyb20gJy4vaHlwZXJsaW5rL2h5cGVybGluay5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2FkaW5nQ29tcG9uZW50IH0gZnJvbSAnLi9sb2FkaW5nL2xvYWRpbmcuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL21vZGFsL21vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5hdmJhckNvbXBvbmVudCB9IGZyb20gJy4vbmF2YmFyL25hdmJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgT3V0bGluZUJ1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4vb3V0bGluZS1idXR0b24vb3V0bGluZS1idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgUGFzc3dvcmRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vcGFzc3dvcmQtZmllbGQvcGFzc3dvcmQtZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2Vjb25kYXJ5TGlua0NvbXBvbmVudCB9IGZyb20gJy4vc2Vjb25kYXJ5LWxpbmsvc2Vjb25kYXJ5LWxpbmsuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnLi9zZWxlY3Qvc2VsZWN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN1YnRpdGxlQ29tcG9uZW50IH0gZnJvbSAnLi9zdWJ0aXRsZS9zdWJ0aXRsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYWJDb21wb25lbnQgfSBmcm9tICcuL3RhYi90YWIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFic0NvbXBvbmVudCB9IGZyb20gJy4vdGFicy90YWJzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRleHRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vdGV4dC1maWVsZC90ZXh0LWZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRpdGxlQ29tcG9uZW50IH0gZnJvbSAnLi90aXRsZS90aXRsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUb29sdGlwQ29tcG9uZW50IH0gZnJvbSAnLi90b29sdGlwL3Rvb2x0aXAuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEJhbm5lckNvbXBvbmVudCxcclxuICAgIEJ1dHRvbkNvbXBvbmVudCxcclxuICAgIENhcmRDb21wb25lbnQsXHJcbiAgICBDYXJvdXNlbEJhbm5lckNvbXBvbmVudCxcclxuICAgIEZvb3RlckNvbXBvbmVudCxcclxuICAgIEZvb3RlckV4dGVybmFsQ29tcG9uZW50LFxyXG4gICAgSHlwZXJsaW5rQ29tcG9uZW50LFxyXG4gICAgTG9hZGluZ0NvbXBvbmVudCxcclxuICAgIE1vZGFsQ29tcG9uZW50LFxyXG4gICAgTmF2YmFyQ29tcG9uZW50LFxyXG4gICAgTm90aWZpY2F0aW9uQ29tcG9uZW50LFxyXG4gICAgT3V0bGluZUJ1dHRvbkNvbXBvbmVudCxcclxuICAgIFBhc3N3b3JkRmllbGRDb21wb25lbnQsXHJcbiAgICBTZWNvbmRhcnlMaW5rQ29tcG9uZW50LFxyXG4gICAgU2VsZWN0Q29tcG9uZW50LFxyXG4gICAgU3VidGl0bGVDb21wb25lbnQsXHJcbiAgICBUYWJDb21wb25lbnQsXHJcbiAgICBUYWJzQ29tcG9uZW50LFxyXG4gICAgVGV4dEZpZWxkQ29tcG9uZW50LFxyXG4gICAgVGl0bGVDb21wb25lbnQsXHJcbiAgICBUb29sdGlwQ29tcG9uZW50XHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBCYW5uZXJDb21wb25lbnQsXHJcbiAgICBCdXR0b25Db21wb25lbnQsXHJcbiAgICBDYXJkQ29tcG9uZW50LFxyXG4gICAgQ2Fyb3VzZWxCYW5uZXJDb21wb25lbnQsXHJcbiAgICBGb290ZXJDb21wb25lbnQsXHJcbiAgICBGb290ZXJFeHRlcm5hbENvbXBvbmVudCxcclxuICAgIEh5cGVybGlua0NvbXBvbmVudCxcclxuICAgIExvYWRpbmdDb21wb25lbnQsXHJcbiAgICBNb2RhbENvbXBvbmVudCxcclxuICAgIE5hdmJhckNvbXBvbmVudCxcclxuICAgIE5vdGlmaWNhdGlvbkNvbXBvbmVudCxcclxuICAgIE91dGxpbmVCdXR0b25Db21wb25lbnQsXHJcbiAgICBQYXNzd29yZEZpZWxkQ29tcG9uZW50LFxyXG4gICAgU2Vjb25kYXJ5TGlua0NvbXBvbmVudCxcclxuICAgIFNlbGVjdENvbXBvbmVudCxcclxuICAgIFN1YnRpdGxlQ29tcG9uZW50LFxyXG4gICAgVGFiQ29tcG9uZW50LFxyXG4gICAgVGFic0NvbXBvbmVudCxcclxuICAgIFRleHRGaWVsZENvbXBvbmVudCxcclxuICAgIFRpdGxlQ29tcG9uZW50LFxyXG4gICAgVG9vbHRpcENvbXBvbmVudFxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFZpZXdMaWJNb2R1bGUgeyB9XHJcbiJdfQ==