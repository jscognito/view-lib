/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
export class CarouselBannerComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CarouselBannerComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-carousel-banner',
                template: `<div class="main-container">
  <div class="carousel-banner">
    <div class="banner-container">
      <a href=""><img class="img-fluid" src="../../../assets/images/bitmap.png"></a>
    </div>
  </div>
</div>
`,
                styles: [`.main-container{width:100%}.main-container .carousel-banner{background-color:#e6e7e8;height:auto}.main-container .carousel-banner .banner-container{max-width:942px;margin:auto;text-align:right}@media (max-width:768px){.carousel-banner{background-color:transparent!important}.carousel-banner .banner-container{max-width:100%;padding:0 26px!important;text-align:center!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.banner-container{padding:0 15px}}`]
            },] },
];
/** @nocollapse */
CarouselBannerComponent.ctorParameters = () => [];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwtYmFubmVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Nhcm91c2VsLWJhbm5lci9jYXJvdXNlbC1iYW5uZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBY2xELE1BQU07SUFFSixpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUFqQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLFFBQVEsRUFBRTs7Ozs7OztDQU9YO2dCQUNDLE1BQU0sRUFBRSxDQUFDLHNnQkFBc2dCLENBQUM7YUFDamhCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWNhcm91c2VsLWJhbm5lcicsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwibWFpbi1jb250YWluZXJcIj5cclxuICA8ZGl2IGNsYXNzPVwiY2Fyb3VzZWwtYmFubmVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiYmFubmVyLWNvbnRhaW5lclwiPlxyXG4gICAgICA8YSBocmVmPVwiXCI+PGltZyBjbGFzcz1cImltZy1mbHVpZFwiIHNyYz1cIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvYml0bWFwLnBuZ1wiPjwvYT5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLm1haW4tY29udGFpbmVye3dpZHRoOjEwMCV9Lm1haW4tY29udGFpbmVyIC5jYXJvdXNlbC1iYW5uZXJ7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4O2hlaWdodDphdXRvfS5tYWluLWNvbnRhaW5lciAuY2Fyb3VzZWwtYmFubmVyIC5iYW5uZXItY29udGFpbmVye21heC13aWR0aDo5NDJweDttYXJnaW46YXV0bzt0ZXh0LWFsaWduOnJpZ2h0fUBtZWRpYSAobWF4LXdpZHRoOjc2OHB4KXsuY2Fyb3VzZWwtYmFubmVye2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQhaW1wb3J0YW50fS5jYXJvdXNlbC1iYW5uZXIgLmJhbm5lci1jb250YWluZXJ7bWF4LXdpZHRoOjEwMCU7cGFkZGluZzowIDI2cHghaW1wb3J0YW50O3RleHQtYWxpZ246Y2VudGVyIWltcG9ydGFudDtib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3h9fUBtZWRpYSAobWF4LXdpZHRoOjk5MnB4KXsuYmFubmVyLWNvbnRhaW5lcntwYWRkaW5nOjAgMTVweH19YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENhcm91c2VsQmFubmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=