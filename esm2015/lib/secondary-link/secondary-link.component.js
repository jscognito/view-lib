/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class SecondaryLinkComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SecondaryLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-secondary-link',
                template: `<div class="secondary-link">
  <i class="icon-left {{ icon }}">
  </i>
  <a href="">{{ labelLink }}</a>
  <i class="fas fa-plus icon-right">
  </i>
</div>
`,
                styles: [`.secondary-link{margin-bottom:30px}.secondary-link .icon-left{font-size:2em;float:left;margin-right:20px;width:35px;text-align:center}.secondary-link a{color:#3a3b3b;font-size:.9em;font-weight:700;line-height:32px}.secondary-link a:hover{color:#ffd200;font-weight:700;text-decoration:none}.secondary-link a:focus{color:#3a3b3b}.secondary-link .icon-right{font-weight:700;font-size:.55em;float:right;line-height:32px}`]
            },] },
];
/** @nocollapse */
SecondaryLinkComponent.ctorParameters = () => [];
SecondaryLinkComponent.propDecorators = {
    labelLink: [{ type: Input }],
    icon: [{ type: Input }],
    id: [{ type: Input }]
};
function SecondaryLinkComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    SecondaryLinkComponent.prototype.labelLink;
    /** @type {?} */
    SecondaryLinkComponent.prototype.icon;
    /** @type {?} */
    SecondaryLinkComponent.prototype.id;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vjb25kYXJ5LWxpbmsuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvc2Vjb25kYXJ5LWxpbmsvc2Vjb25kYXJ5LWxpbmsuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQWN6RCxNQUFNO0lBT0osaUJBQWlCOzs7O0lBRWpCLFFBQVE7S0FDUDs7O1lBdEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixRQUFRLEVBQUU7Ozs7Ozs7Q0FPWDtnQkFDQyxNQUFNLEVBQUUsQ0FBQyxrYUFBa2EsQ0FBQzthQUM3YTs7Ozs7d0JBRUUsS0FBSzttQkFDTCxLQUFLO2lCQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXNlY29uZGFyeS1saW5rJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJzZWNvbmRhcnktbGlua1wiPlxyXG4gIDxpIGNsYXNzPVwiaWNvbi1sZWZ0IHt7IGljb24gfX1cIj5cclxuICA8L2k+XHJcbiAgPGEgaHJlZj1cIlwiPnt7IGxhYmVsTGluayB9fTwvYT5cclxuICA8aSBjbGFzcz1cImZhcyBmYS1wbHVzIGljb24tcmlnaHRcIj5cclxuICA8L2k+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2Auc2Vjb25kYXJ5LWxpbmt7bWFyZ2luLWJvdHRvbTozMHB4fS5zZWNvbmRhcnktbGluayAuaWNvbi1sZWZ0e2ZvbnQtc2l6ZToyZW07ZmxvYXQ6bGVmdDttYXJnaW4tcmlnaHQ6MjBweDt3aWR0aDozNXB4O3RleHQtYWxpZ246Y2VudGVyfS5zZWNvbmRhcnktbGluayBhe2NvbG9yOiMzYTNiM2I7Zm9udC1zaXplOi45ZW07Zm9udC13ZWlnaHQ6NzAwO2xpbmUtaGVpZ2h0OjMycHh9LnNlY29uZGFyeS1saW5rIGE6aG92ZXJ7Y29sb3I6I2ZmZDIwMDtmb250LXdlaWdodDo3MDA7dGV4dC1kZWNvcmF0aW9uOm5vbmV9LnNlY29uZGFyeS1saW5rIGE6Zm9jdXN7Y29sb3I6IzNhM2IzYn0uc2Vjb25kYXJ5LWxpbmsgLmljb24tcmlnaHR7Zm9udC13ZWlnaHQ6NzAwO2ZvbnQtc2l6ZTouNTVlbTtmbG9hdDpyaWdodDtsaW5lLWhlaWdodDozMnB4fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWNvbmRhcnlMaW5rQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBsYWJlbExpbms6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBcclxuICBcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=