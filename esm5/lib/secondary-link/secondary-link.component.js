/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var SecondaryLinkComponent = /** @class */ (function () {
    function SecondaryLinkComponent() {
    }
    /**
     * @return {?}
     */
    SecondaryLinkComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SecondaryLinkComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-secondary-link',
                    template: "<div class=\"secondary-link\">\n  <i class=\"icon-left {{ icon }}\">\n  </i>\n  <a href=\"\">{{ labelLink }}</a>\n  <i class=\"fas fa-plus icon-right\">\n  </i>\n</div>\n",
                    styles: [".secondary-link{margin-bottom:30px}.secondary-link .icon-left{font-size:2em;float:left;margin-right:20px;width:35px;text-align:center}.secondary-link a{color:#3a3b3b;font-size:.9em;font-weight:700;line-height:32px}.secondary-link a:hover{color:#ffd200;font-weight:700;text-decoration:none}.secondary-link a:focus{color:#3a3b3b}.secondary-link .icon-right{font-weight:700;font-size:.55em;float:right;line-height:32px}"]
                },] },
    ];
    /** @nocollapse */
    SecondaryLinkComponent.ctorParameters = function () { return []; };
    SecondaryLinkComponent.propDecorators = {
        labelLink: [{ type: Input }],
        icon: [{ type: Input }],
        id: [{ type: Input }]
    };
    return SecondaryLinkComponent;
}());
export { SecondaryLinkComponent };
function SecondaryLinkComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    SecondaryLinkComponent.prototype.labelLink;
    /** @type {?} */
    SecondaryLinkComponent.prototype.icon;
    /** @type {?} */
    SecondaryLinkComponent.prototype.id;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vjb25kYXJ5LWxpbmsuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvc2Vjb25kYXJ5LWxpbmsvc2Vjb25kYXJ5LWxpbmsuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQzs7SUFxQnZEO0tBQWlCOzs7O0lBRWpCLHlDQUFROzs7SUFBUjtLQUNDOztnQkF0QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLFFBQVEsRUFBRSw0S0FPWDtvQkFDQyxNQUFNLEVBQUUsQ0FBQyxrYUFBa2EsQ0FBQztpQkFDN2E7Ozs7OzRCQUVFLEtBQUs7dUJBQ0wsS0FBSztxQkFDTCxLQUFLOztpQ0FqQlI7O1NBY2Esc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1zZWNvbmRhcnktbGluaycsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwic2Vjb25kYXJ5LWxpbmtcIj5cclxuICA8aSBjbGFzcz1cImljb24tbGVmdCB7eyBpY29uIH19XCI+XHJcbiAgPC9pPlxyXG4gIDxhIGhyZWY9XCJcIj57eyBsYWJlbExpbmsgfX08L2E+XHJcbiAgPGkgY2xhc3M9XCJmYXMgZmEtcGx1cyBpY29uLXJpZ2h0XCI+XHJcbiAgPC9pPlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLnNlY29uZGFyeS1saW5re21hcmdpbi1ib3R0b206MzBweH0uc2Vjb25kYXJ5LWxpbmsgLmljb24tbGVmdHtmb250LXNpemU6MmVtO2Zsb2F0OmxlZnQ7bWFyZ2luLXJpZ2h0OjIwcHg7d2lkdGg6MzVweDt0ZXh0LWFsaWduOmNlbnRlcn0uc2Vjb25kYXJ5LWxpbmsgYXtjb2xvcjojM2EzYjNiO2ZvbnQtc2l6ZTouOWVtO2ZvbnQtd2VpZ2h0OjcwMDtsaW5lLWhlaWdodDozMnB4fS5zZWNvbmRhcnktbGluayBhOmhvdmVye2NvbG9yOiNmZmQyMDA7Zm9udC13ZWlnaHQ6NzAwO3RleHQtZGVjb3JhdGlvbjpub25lfS5zZWNvbmRhcnktbGluayBhOmZvY3Vze2NvbG9yOiMzYTNiM2J9LnNlY29uZGFyeS1saW5rIC5pY29uLXJpZ2h0e2ZvbnQtd2VpZ2h0OjcwMDtmb250LXNpemU6LjU1ZW07ZmxvYXQ6cmlnaHQ7bGluZS1oZWlnaHQ6MzJweH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2Vjb25kYXJ5TGlua0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgbGFiZWxMaW5rOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgXHJcbiAgXHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19