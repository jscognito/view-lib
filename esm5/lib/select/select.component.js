/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var SelectComponent = /** @class */ (function () {
    function SelectComponent() {
        this.valueSelect = 0;
    }
    /**
     * @return {?}
     */
    SelectComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    SelectComponent.prototype.validateField = /**
     * @return {?}
     */
    function () {
        (this.valueSelect !== 0) ? this.emptySelect = 'status' : this.emptySelect = '';
    };
    SelectComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-select',
                    template: "<div class=\"text-field {{ state }}\">\n  <div class=\"content\">\n    <label>\n      <select class=\"{{ emptySelect }}\" [(ngModel)]=\"valueSelect\" name=\"select\" required\n              (change)=\"validateField()\" [disabled]=\"(state == 'disabled')\">\n\n        <ng-content></ng-content>\n      </select>\n      <span>Seleccione</span>\n      <div class=\"icon\">\n        <i class=\"{{ icon }}\"></i>\n      </div>\n    </label>\n  </div>\n  <div class=\"message\">\n    <p>{{ message }}</p>\n  </div>\n</div>\n",
                    styles: [".text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content select{position:absolute;top:15px;z-index:2;width:100%;font-size:16px;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px;-moz-appearance:none;-webkit-appearance:none;appearance:none}.text-field .content select:focus{border-bottom:2px solid #ffd200}.text-field .content select:focus+span,.text-field .content select:valid+span{top:6px;cursor:inherit;font-size:13px;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:1;font-size:16px;transition:all .2s ease-in-out;width:100%;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{font-size:18px;position:absolute;z-index:3;right:15px;top:15px}.text-field .message{min-height:20px}.text-field .message p{padding:4px 0 0 15px;font-size:12px;color:#808285;margin:0}"]
                },] },
    ];
    /** @nocollapse */
    SelectComponent.ctorParameters = function () { return []; };
    SelectComponent.propDecorators = {
        label: [{ type: Input }],
        icon: [{ type: Input }],
        type: [{ type: Input }],
        state: [{ type: Input }],
        message: [{ type: Input }]
    };
    return SelectComponent;
}());
export { SelectComponent };
function SelectComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    SelectComponent.prototype.valueSelect;
    /** @type {?} */
    SelectComponent.prototype.emptySelect;
    /** @type {?} */
    SelectComponent.prototype.label;
    /** @type {?} */
    SelectComponent.prototype.icon;
    /** @type {?} */
    SelectComponent.prototype.type;
    /** @type {?} */
    SelectComponent.prototype.state;
    /** @type {?} */
    SelectComponent.prototype.message;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL3NlbGVjdC9zZWxlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQzs7SUFtQ3JEOzJCQVRjLENBQUM7S0FVZDs7OztJQUVELGtDQUFROzs7SUFBUjtLQUNDOzs7O0lBRUQsdUNBQWE7OztJQUFiO1FBQ0UsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7S0FDaEY7O2dCQTFDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLFFBQVEsRUFBRSx3Z0JBa0JYO29CQUNDLE1BQU0sRUFBRSxDQUFDLDJvREFBMm9ELENBQUM7aUJBQ3RwRDs7Ozs7d0JBTUUsS0FBSzt1QkFDTCxLQUFLO3VCQUNMLEtBQUs7d0JBQ0wsS0FBSzswQkFDTCxLQUFLOzswQkFqQ1I7O1NBd0JhLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXNlbGVjdCcsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwidGV4dC1maWVsZCB7eyBzdGF0ZSB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICA8bGFiZWw+XHJcbiAgICAgIDxzZWxlY3QgY2xhc3M9XCJ7eyBlbXB0eVNlbGVjdCB9fVwiIFsobmdNb2RlbCldPVwidmFsdWVTZWxlY3RcIiBuYW1lPVwic2VsZWN0XCIgcmVxdWlyZWRcclxuICAgICAgICAgICAgICAoY2hhbmdlKT1cInZhbGlkYXRlRmllbGQoKVwiIFtkaXNhYmxlZF09XCIoc3RhdGUgPT0gJ2Rpc2FibGVkJylcIj5cclxuXHJcbiAgICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gICAgICA8L3NlbGVjdD5cclxuICAgICAgPHNwYW4+U2VsZWNjaW9uZTwvc3Bhbj5cclxuICAgICAgPGRpdiBjbGFzcz1cImljb25cIj5cclxuICAgICAgICA8aSBjbGFzcz1cInt7IGljb24gfX1cIj48L2k+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9sYWJlbD5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwibWVzc2FnZVwiPlxyXG4gICAgPHA+e3sgbWVzc2FnZSB9fTwvcD5cclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC50ZXh0LWZpZWxke21hcmdpbi1ib3R0b206MTBweH0udGV4dC1maWVsZC5kaXNhYmxlZHtvcGFjaXR5Oi4zfS50ZXh0LWZpZWxkLmRpc2FibGVkIC5jb250ZW50LC50ZXh0LWZpZWxkLmRpc2FibGVkIGlucHV0LC50ZXh0LWZpZWxkLmRpc2FibGVkIGxhYmVsLC50ZXh0LWZpZWxkLmRpc2FibGVkIHNwYW57Y3Vyc29yOm5vdC1hbGxvd2VkIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciAuaWNvbiwudGV4dC1maWVsZC5lcnJvciBpbnB1dDpmb2N1cytzcGFuLC50ZXh0LWZpZWxkLmVycm9yIHAsLnRleHQtZmllbGQuZXJyb3Igc3Bhbntjb2xvcjojZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZC5lcnJvciBpbnB1dHtib3JkZXItYm90dG9tOjJweCBzb2xpZCAjZmE1ZTViIWltcG9ydGFudH0udGV4dC1maWVsZCAuY29udGVudHt3aWR0aDoxMDAlO2NvbG9yOiM0NTQ2NDg7YmFja2dyb3VuZC1jb2xvcjojZjdmN2Y3O2JvcmRlci1yYWRpdXM6NnB4IDZweCAwIDB9LnRleHQtZmllbGQgLmNvbnRlbnQgbGFiZWx7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt3aWR0aDoxMDAlO21pbi1oZWlnaHQ6NTBweH0udGV4dC1maWVsZCAuY29udGVudCBzZWxlY3R7cG9zaXRpb246YWJzb2x1dGU7dG9wOjE1cHg7ei1pbmRleDoyO3dpZHRoOjEwMCU7Zm9udC1zaXplOjE2cHg7Ym9yZGVyOjA7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgIzgwODI4NTt0cmFuc2l0aW9uOmJvcmRlci1jb2xvciAuMnMgZWFzZS1pbi1vdXQ7b3V0bGluZTowO2hlaWdodDozNXB4O2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4Oy1tb3otYXBwZWFyYW5jZTpub25lOy13ZWJraXQtYXBwZWFyYW5jZTpub25lO2FwcGVhcmFuY2U6bm9uZX0udGV4dC1maWVsZCAuY29udGVudCBzZWxlY3Q6Zm9jdXN7Ym9yZGVyLWJvdHRvbToycHggc29saWQgI2ZmZDIwMH0udGV4dC1maWVsZCAuY29udGVudCBzZWxlY3Q6Zm9jdXMrc3BhbiwudGV4dC1maWVsZCAuY29udGVudCBzZWxlY3Q6dmFsaWQrc3Bhbnt0b3A6NnB4O2N1cnNvcjppbmhlcml0O2ZvbnQtc2l6ZToxM3B4O2NvbG9yOiM4MDgyODU7cGFkZGluZy1sZWZ0OjE1cHh9LnRleHQtZmllbGQgLmNvbnRlbnQgLnN0YXR1c3tib3JkZXItYm90dG9tOjJweCBzb2xpZCAjMDA0NDhkfS50ZXh0LWZpZWxkIC5jb250ZW50IHNwYW57cG9zaXRpb246YWJzb2x1dGU7ZGlzcGxheTpibG9jazt0b3A6MjBweDt6LWluZGV4OjE7Zm9udC1zaXplOjE2cHg7dHJhbnNpdGlvbjphbGwgLjJzIGVhc2UtaW4tb3V0O3dpZHRoOjEwMCU7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IC5pY29ue2ZvbnQtc2l6ZToxOHB4O3Bvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6MztyaWdodDoxNXB4O3RvcDoxNXB4fS50ZXh0LWZpZWxkIC5tZXNzYWdle21pbi1oZWlnaHQ6MjBweH0udGV4dC1maWVsZCAubWVzc2FnZSBwe3BhZGRpbmc6NHB4IDAgMCAxNXB4O2ZvbnQtc2l6ZToxMnB4O2NvbG9yOiM4MDgyODU7bWFyZ2luOjB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlbGVjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHZhbHVlU2VsZWN0ID0gMDtcclxuICBlbXB0eVNlbGVjdDogc3RyaW5nO1xyXG5cclxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgc3RhdGU6IHN0cmluZztcclxuICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZUZpZWxkKCkge1xyXG4gICAgKHRoaXMudmFsdWVTZWxlY3QgIT09IDApID8gdGhpcy5lbXB0eVNlbGVjdCA9ICdzdGF0dXMnIDogdGhpcy5lbXB0eVNlbGVjdCA9ICcnO1xyXG4gIH1cclxuXHJcbn1cclxuIl19