/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { TabComponent } from '../tab/tab.component';
var TabsComponent = /** @class */ (function () {
    function TabsComponent() {
    }
    /**
     * @return {?}
     */
    TabsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    TabsComponent.prototype.ngAfterContentInit = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var activeTabs = this.tabs.filter(function (tab) { return tab.active; });
        // if there is no active tab set, activate the first
        if (activeTabs.length === 0) {
            this.selectTab(this.tabs.first);
        }
    };
    /**
     * @param {?} tab
     * @return {?}
     */
    TabsComponent.prototype.selectTab = /**
     * @param {?} tab
     * @return {?}
     */
    function (tab) {
        // deactivate all tabs
        this.tabs.toArray().forEach(function (tab) { return tab.active = false; });
        // activate the tab the user has clicked on.
        tab.active = true;
    };
    TabsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-tabs',
                    template: "<div class=\"nav nav-tabs\">\n  <span *ngFor=\"let tab of tabs\" id=\"{{tab.id}}\" (click)=\"selectTab(tab)\" [class.active]=\"tab.active\" class=\"tab\"\n        ngClass=\"{{position}}\">\n    <span role=\"button\" class=\"tab-title\">{{tab.title}}</span>\n  </span>\n</div>\n<ng-content></ng-content>\n",
                    styles: [".nav-tabs{display:flex;flex-wrap:nowrap;font-size:1em;font-weight:700;text-transform:uppercase;border-bottom:1px solid #e6e7e8}.nav-tabs .tab{width:50%;padding:18px 10px 10px;text-align:center}.nav-tabs .center{flex:1 1;display:flex}.nav-tabs .tab-title{padding:0 7px 5px;cursor:pointer}.nav-tabs .active{border-bottom:solid #ffd200}"]
                },] },
    ];
    /** @nocollapse */
    TabsComponent.ctorParameters = function () { return []; };
    TabsComponent.propDecorators = {
        position: [{ type: Input }],
        tabs: [{ type: ContentChildren, args: [TabComponent,] }]
    };
    return TabsComponent;
}());
export { TabsComponent };
function TabsComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TabsComponent.prototype.position;
    /** @type {?} */
    TabsComponent.prototype.tabs;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi90YWJzL3RhYnMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQTRCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV0RyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sc0JBQXNCLENBQUM7O0lBbUJoRDtLQUFpQjs7OztJQUVqQixnQ0FBUTs7O0lBQVI7S0FDQzs7OztJQUVELDBDQUFrQjs7O0lBQWxCOztRQUVFLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLE1BQU0sRUFBVixDQUFVLENBQUMsQ0FBQzs7UUFHekQsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNqQztLQUNGOzs7OztJQUVELGlDQUFTOzs7O0lBQVQsVUFBVSxHQUFpQjs7UUFFekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUUsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDOztRQUd4RCxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztLQUNuQjs7Z0JBdENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFLGtUQU9YO29CQUNDLE1BQU0sRUFBRSxDQUFDLCtVQUErVSxDQUFDO2lCQUMxVjs7Ozs7MkJBR0UsS0FBSzt1QkFDTCxlQUFlLFNBQUMsWUFBWTs7d0JBbkIvQjs7U0FnQmEsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBDb250ZW50Q2hpbGRyZW4sIElucHV0LCBPbkluaXQsIEFmdGVyQ29udGVudEluaXQsIFF1ZXJ5TGlzdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge1RhYkNvbXBvbmVudH0gZnJvbSAnLi4vdGFiL3RhYi5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtdGFicycsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwibmF2IG5hdi10YWJzXCI+XHJcbiAgPHNwYW4gKm5nRm9yPVwibGV0IHRhYiBvZiB0YWJzXCIgaWQ9XCJ7e3RhYi5pZH19XCIgKGNsaWNrKT1cInNlbGVjdFRhYih0YWIpXCIgW2NsYXNzLmFjdGl2ZV09XCJ0YWIuYWN0aXZlXCIgY2xhc3M9XCJ0YWJcIlxyXG4gICAgICAgIG5nQ2xhc3M9XCJ7e3Bvc2l0aW9ufX1cIj5cclxuICAgIDxzcGFuIHJvbGU9XCJidXR0b25cIiBjbGFzcz1cInRhYi10aXRsZVwiPnt7dGFiLnRpdGxlfX08L3NwYW4+XHJcbiAgPC9zcGFuPlxyXG48L2Rpdj5cclxuPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG5gLFxyXG4gIHN0eWxlczogW2AubmF2LXRhYnN7ZGlzcGxheTpmbGV4O2ZsZXgtd3JhcDpub3dyYXA7Zm9udC1zaXplOjFlbTtmb250LXdlaWdodDo3MDA7dGV4dC10cmFuc2Zvcm06dXBwZXJjYXNlO2JvcmRlci1ib3R0b206MXB4IHNvbGlkICNlNmU3ZTh9Lm5hdi10YWJzIC50YWJ7d2lkdGg6NTAlO3BhZGRpbmc6MThweCAxMHB4IDEwcHg7dGV4dC1hbGlnbjpjZW50ZXJ9Lm5hdi10YWJzIC5jZW50ZXJ7ZmxleDoxIDE7ZGlzcGxheTpmbGV4fS5uYXYtdGFicyAudGFiLXRpdGxle3BhZGRpbmc6MCA3cHggNXB4O2N1cnNvcjpwb2ludGVyfS5uYXYtdGFicyAuYWN0aXZle2JvcmRlci1ib3R0b206c29saWQgI2ZmZDIwMH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFic0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJDb250ZW50SW5pdCAge1xyXG5cclxuICBASW5wdXQoKSBwb3NpdGlvbjogc3RyaW5nO1xyXG4gIEBDb250ZW50Q2hpbGRyZW4oVGFiQ29tcG9uZW50KSB0YWJzOiBRdWVyeUxpc3Q8VGFiQ29tcG9uZW50PjtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyQ29udGVudEluaXQoKSB7XHJcbiAgICAvLyBnZXQgYWxsIGFjdGl2ZSB0YWJzXHJcbiAgICBjb25zdCBhY3RpdmVUYWJzID0gdGhpcy50YWJzLmZpbHRlcigodGFiKSA9PiB0YWIuYWN0aXZlKTtcclxuXHJcbiAgICAvLyBpZiB0aGVyZSBpcyBubyBhY3RpdmUgdGFiIHNldCwgYWN0aXZhdGUgdGhlIGZpcnN0XHJcbiAgICBpZiAoYWN0aXZlVGFicy5sZW5ndGggPT09IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RUYWIodGhpcy50YWJzLmZpcnN0KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNlbGVjdFRhYih0YWI6IFRhYkNvbXBvbmVudCkge1xyXG4gICAgLy8gZGVhY3RpdmF0ZSBhbGwgdGFic1xyXG4gICAgdGhpcy50YWJzLnRvQXJyYXkoKS5mb3JFYWNoKCB0YWIgPT4gdGFiLmFjdGl2ZSA9IGZhbHNlKTtcclxuXHJcbiAgICAvLyBhY3RpdmF0ZSB0aGUgdGFiIHRoZSB1c2VyIGhhcyBjbGlja2VkIG9uLlxyXG4gICAgdGFiLmFjdGl2ZSA9IHRydWU7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=