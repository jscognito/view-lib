/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
        this.date = new Date();
    }
    /**
     * @return {?}
     */
    NavbarComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    NavbarComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-navbar',
                    template: "<div class=\"navbar\">\n  <nav class=\"main-navbar\">\n    <div class=\"nav-container\">\n      <div class=\"row\">\n        <div class=\"col-sm-6\">\n          <img src=\"../../../assets/images/logoBanistmo.svg\">\n        </div>\n        <div class=\"col-sm-6\">\n          <ng-content></ng-content>\n        </div>\n      </div>\n    </div>\n  </nav>\n  <nav class=\"secondary-navbar\">\n    <div class=\"nav-container\">\n      <div class=\"row\">\n        <div class=\"col-sm-6 welcome-text\">\n          Bienvenido a la Sucursal Virtual Personas\n        </div>\n        <div class=\"col-sm-6 date\">\n          \n        </div>\n      </div>\n    </div>\n  </nav>\n</div>\n",
                    styles: [".navbar{width:100%}.navbar .main-navbar{padding:13px 0;background-color:#fff}.navbar .secondary-navbar{padding:4px 0;background-color:#ffd200;min-height:30px;line-height:25px}.navbar .secondary-navbar .welcome-text{font-weight:700;font-size:.88em}.navbar .secondary-navbar .date{font-size:12px;text-align:right}.navbar .nav-container{max-width:942px;margin:auto}.navbar img{height:34px}@media (max-width:576px){.date,.main-navbar,.secondary-navbar{text-align:center!important}}@media (max-width:768px){.nav-container{max-width:100%;padding:0 26px!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.nav-container{padding:0 15px}}"]
                },] },
    ];
    /** @nocollapse */
    NavbarComponent.ctorParameters = function () { return []; };
    NavbarComponent.propDecorators = {
        content: [{ type: Input }]
    };
    return NavbarComponent;
}());
export { NavbarComponent };
function NavbarComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    NavbarComponent.prototype.date;
    /** @type {?} */
    NavbarComponent.prototype.content;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQzs7SUFzQ3JEO29CQUpPLElBQUksSUFBSSxFQUFFO0tBSUE7Ozs7SUFFakIsa0NBQVE7OztJQUFSO0tBQ0M7O2dCQXZDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLFFBQVEsRUFBRSwwcUJBMEJYO29CQUNDLE1BQU0sRUFBRSxDQUFDLHVyQkFBdXJCLENBQUM7aUJBQ2xzQjs7Ozs7MEJBSUUsS0FBSzs7MEJBcENSOztTQWlDYSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbmF2YmFyJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJuYXZiYXJcIj5cclxuICA8bmF2IGNsYXNzPVwibWFpbi1uYXZiYXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJuYXYtY29udGFpbmVyXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTZcIj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9sb2dvQmFuaXN0bW8uc3ZnXCI+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS02XCI+XHJcbiAgICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9uYXY+XHJcbiAgPG5hdiBjbGFzcz1cInNlY29uZGFyeS1uYXZiYXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJuYXYtY29udGFpbmVyXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTYgd2VsY29tZS10ZXh0XCI+XHJcbiAgICAgICAgICBCaWVudmVuaWRvIGEgbGEgU3VjdXJzYWwgVmlydHVhbCBQZXJzb25hc1xyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNiBkYXRlXCI+XHJcbiAgICAgICAgICBcclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L25hdj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYC5uYXZiYXJ7d2lkdGg6MTAwJX0ubmF2YmFyIC5tYWluLW5hdmJhcntwYWRkaW5nOjEzcHggMDtiYWNrZ3JvdW5kLWNvbG9yOiNmZmZ9Lm5hdmJhciAuc2Vjb25kYXJ5LW5hdmJhcntwYWRkaW5nOjRweCAwO2JhY2tncm91bmQtY29sb3I6I2ZmZDIwMDttaW4taGVpZ2h0OjMwcHg7bGluZS1oZWlnaHQ6MjVweH0ubmF2YmFyIC5zZWNvbmRhcnktbmF2YmFyIC53ZWxjb21lLXRleHR7Zm9udC13ZWlnaHQ6NzAwO2ZvbnQtc2l6ZTouODhlbX0ubmF2YmFyIC5zZWNvbmRhcnktbmF2YmFyIC5kYXRle2ZvbnQtc2l6ZToxMnB4O3RleHQtYWxpZ246cmlnaHR9Lm5hdmJhciAubmF2LWNvbnRhaW5lcnttYXgtd2lkdGg6OTQycHg7bWFyZ2luOmF1dG99Lm5hdmJhciBpbWd7aGVpZ2h0OjM0cHh9QG1lZGlhIChtYXgtd2lkdGg6NTc2cHgpey5kYXRlLC5tYWluLW5hdmJhciwuc2Vjb25kYXJ5LW5hdmJhcnt0ZXh0LWFsaWduOmNlbnRlciFpbXBvcnRhbnR9fUBtZWRpYSAobWF4LXdpZHRoOjc2OHB4KXsubmF2LWNvbnRhaW5lcnttYXgtd2lkdGg6MTAwJTtwYWRkaW5nOjAgMjZweCFpbXBvcnRhbnQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94fX1AbWVkaWEgKG1heC13aWR0aDo5OTJweCl7Lm5hdi1jb250YWluZXJ7cGFkZGluZzowIDE1cHh9fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOYXZiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGRhdGUgPSBuZXcgRGF0ZSgpO1xyXG5cclxuICBASW5wdXQoKSBjb250ZW50OiBhbnk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19