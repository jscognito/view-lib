/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
var CarouselBannerComponent = /** @class */ (function () {
    function CarouselBannerComponent() {
    }
    /**
     * @return {?}
     */
    CarouselBannerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    CarouselBannerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-carousel-banner',
                    template: "<div class=\"main-container\">\n  <div class=\"carousel-banner\">\n    <div class=\"banner-container\">\n      <a href=\"\"><img class=\"img-fluid\" src=\"../../../assets/images/bitmap.png\"></a>\n    </div>\n  </div>\n</div>\n",
                    styles: [".main-container{width:100%}.main-container .carousel-banner{background-color:#e6e7e8;height:auto}.main-container .carousel-banner .banner-container{max-width:942px;margin:auto;text-align:right}@media (max-width:768px){.carousel-banner{background-color:transparent!important}.carousel-banner .banner-container{max-width:100%;padding:0 26px!important;text-align:center!important;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box}}@media (max-width:992px){.banner-container{padding:0 15px}}"]
                },] },
    ];
    /** @nocollapse */
    CarouselBannerComponent.ctorParameters = function () { return []; };
    return CarouselBannerComponent;
}());
export { CarouselBannerComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2Fyb3VzZWwtYmFubmVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Nhcm91c2VsLWJhbm5lci9jYXJvdXNlbC1iYW5uZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQWdCaEQ7S0FBaUI7Ozs7SUFFakIsMENBQVE7OztJQUFSO0tBQ0M7O2dCQWpCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsUUFBUSxFQUFFLHFPQU9YO29CQUNDLE1BQU0sRUFBRSxDQUFDLHNnQkFBc2dCLENBQUM7aUJBQ2poQjs7OztrQ0FiRDs7U0FjYSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtY2Fyb3VzZWwtYmFubmVyJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJtYWluLWNvbnRhaW5lclwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjYXJvdXNlbC1iYW5uZXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJiYW5uZXItY29udGFpbmVyXCI+XHJcbiAgICAgIDxhIGhyZWY9XCJcIj48aW1nIGNsYXNzPVwiaW1nLWZsdWlkXCIgc3JjPVwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9iaXRtYXAucG5nXCI+PC9hPlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AubWFpbi1jb250YWluZXJ7d2lkdGg6MTAwJX0ubWFpbi1jb250YWluZXIgLmNhcm91c2VsLWJhbm5lcntiYWNrZ3JvdW5kLWNvbG9yOiNlNmU3ZTg7aGVpZ2h0OmF1dG99Lm1haW4tY29udGFpbmVyIC5jYXJvdXNlbC1iYW5uZXIgLmJhbm5lci1jb250YWluZXJ7bWF4LXdpZHRoOjk0MnB4O21hcmdpbjphdXRvO3RleHQtYWxpZ246cmlnaHR9QG1lZGlhIChtYXgtd2lkdGg6NzY4cHgpey5jYXJvdXNlbC1iYW5uZXJ7YmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudCFpbXBvcnRhbnR9LmNhcm91c2VsLWJhbm5lciAuYmFubmVyLWNvbnRhaW5lcnttYXgtd2lkdGg6MTAwJTtwYWRkaW5nOjAgMjZweCFpbXBvcnRhbnQ7dGV4dC1hbGlnbjpjZW50ZXIhaW1wb3J0YW50O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveH19QG1lZGlhIChtYXgtd2lkdGg6OTkycHgpey5iYW5uZXItY29udGFpbmVye3BhZGRpbmc6MCAxNXB4fX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2Fyb3VzZWxCYW5uZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==