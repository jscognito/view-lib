/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
var TextFieldComponent = /** @class */ (function () {
    function TextFieldComponent() {
        this.valueUsername = new EventEmitter();
    }
    /**
     * @return {?}
     */
    TextFieldComponent.prototype.getValueUsername = /**
     * @return {?}
     */
    function () {
        this.valueUsername.emit({
            value: this.valueInput,
            state: this.inputStyle
        });
    };
    /**
     * @return {?}
     */
    TextFieldComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    TextFieldComponent.prototype.validateField = /**
     * @return {?}
     */
    function () {
        if (this.valueInput !== null && this.valueInput !== '') {
            this.inputStyle = 'status';
        }
        else {
            this.inputStyle = '';
        }
        this.getValueUsername();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    TextFieldComponent.prototype.onSetStateChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.state = event.state;
        this.inputStyle = event.state;
        if (event.state !== '') {
            this.message = 'No cumple con la longitud mínima';
            this.iconSecondary = 'fal fa-times';
        }
        else {
            this.message = '';
            this.iconSecondary = '';
        }
        if (this.valueInput === '') {
            this.message = '';
            this.state = '';
            this.iconSecondary = '';
        }
        this.getValueUsername();
    };
    TextFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-text-field',
                    template: "<div class=\"text-field {{ state }}\">\n  <div class=\"content\">\n    <label>\n      <input type=\"text\" id=\"{{ id }}\" class=\"{{ inputStyle }}\" [(ngModel)]=\"valueInput\" name=\"text-field\" [required]=\"true\"\n      input-directive [minLength]=8 [maxLength]=16 [pattern]=\"pattern\" (setState)=\"onSetStateChange($event)\" (keyup)=\"validateField()\" [disabled]=\"(state == 'disabled')\">\n      <span>{{ label }}</span>\n      <div class=\"icon\">\n        <i class=\"{{ iconPrimary }}\"></i>\n        <i class=\"{{ iconSecondary }}\"></i>\n      </div>\n    </label>\n  </div>\n  <div class=\"message\">\n    <p>{{ message }}</p>\n  </div>\n</div>\n",
                    styles: [".text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:15px;cursor:pointer}.text-field .content .icon i:nth-child(2){color:#fa5e5b}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}"],
                },] },
    ];
    /** @nocollapse */
    TextFieldComponent.ctorParameters = function () { return []; };
    TextFieldComponent.propDecorators = {
        valueUsername: [{ type: Output }],
        label: [{ type: Input }],
        iconPrimary: [{ type: Input }],
        iconSecondary: [{ type: Input }],
        state: [{ type: Input }],
        message: [{ type: Input }],
        pattern: [{ type: Input }],
        id: [{ type: Input }]
    };
    return TextFieldComponent;
}());
export { TextFieldComponent };
function TextFieldComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TextFieldComponent.prototype.valueInput;
    /** @type {?} */
    TextFieldComponent.prototype.inputStyle;
    /** @type {?} */
    TextFieldComponent.prototype.valueUsername;
    /** @type {?} */
    TextFieldComponent.prototype.label;
    /** @type {?} */
    TextFieldComponent.prototype.iconPrimary;
    /** @type {?} */
    TextFieldComponent.prototype.iconSecondary;
    /** @type {?} */
    TextFieldComponent.prototype.state;
    /** @type {?} */
    TextFieldComponent.prototype.message;
    /** @type {?} */
    TextFieldComponent.prototype.pattern;
    /** @type {?} */
    TextFieldComponent.prototype.id;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi90ZXh0LWZpZWxkL3RleHQtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBVSxZQUFZLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQzs7SUEyQ25DOzZCQWhCaUMsSUFBSSxZQUFZLEVBQUU7S0FpQmxEOzs7O0lBUkQsNkNBQWdCOzs7SUFBaEI7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztZQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVO1NBQ3ZCLENBQUMsQ0FBQztLQUNKOzs7O0lBS0QscUNBQVE7OztJQUFSO0tBQ0M7Ozs7SUFFRCwwQ0FBYTs7O0lBQWI7UUFDRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7U0FDNUI7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7S0FDekI7Ozs7O0lBRUQsNkNBQWdCOzs7O0lBQWhCLFVBQWlCLEtBQUs7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUM5QixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxrQ0FBa0MsQ0FBQztZQUNsRCxJQUFJLENBQUMsYUFBYSxHQUFHLGNBQWMsQ0FBQztTQUNyQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7U0FDekI7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7U0FDekI7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztLQUN6Qjs7Z0JBekVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixRQUFRLEVBQUUscXBCQWdCWDtvQkFDQyxNQUFNLEVBQUUsQ0FBQyw4ckRBQThyRCxDQUFDO2lCQUV6c0Q7Ozs7O2dDQUlFLE1BQU07d0JBQ04sS0FBSzs4QkFDTCxLQUFLO2dDQUNMLEtBQUs7d0JBQ0wsS0FBSzswQkFDTCxLQUFLOzBCQUNMLEtBQUs7cUJBQ0wsS0FBSzs7NkJBbkNSOztTQXlCYSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgRXZlbnRFbWl0dGVyfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtdGV4dC1maWVsZCcsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwidGV4dC1maWVsZCB7eyBzdGF0ZSB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICA8bGFiZWw+XHJcbiAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwie3sgaWQgfX1cIiBjbGFzcz1cInt7IGlucHV0U3R5bGUgfX1cIiBbKG5nTW9kZWwpXT1cInZhbHVlSW5wdXRcIiBuYW1lPVwidGV4dC1maWVsZFwiIFtyZXF1aXJlZF09XCJ0cnVlXCJcclxuICAgICAgaW5wdXQtZGlyZWN0aXZlIFttaW5MZW5ndGhdPTggW21heExlbmd0aF09MTYgW3BhdHRlcm5dPVwicGF0dGVyblwiIChzZXRTdGF0ZSk9XCJvblNldFN0YXRlQ2hhbmdlKCRldmVudClcIiAoa2V5dXApPVwidmFsaWRhdGVGaWVsZCgpXCIgW2Rpc2FibGVkXT1cIihzdGF0ZSA9PSAnZGlzYWJsZWQnKVwiPlxyXG4gICAgICA8c3Bhbj57eyBsYWJlbCB9fTwvc3Bhbj5cclxuICAgICAgPGRpdiBjbGFzcz1cImljb25cIj5cclxuICAgICAgICA8aSBjbGFzcz1cInt7IGljb25QcmltYXJ5IH19XCI+PC9pPlxyXG4gICAgICAgIDxpIGNsYXNzPVwie3sgaWNvblNlY29uZGFyeSB9fVwiPjwvaT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2xhYmVsPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJtZXNzYWdlXCI+XHJcbiAgICA8cD57eyBtZXNzYWdlIH19PC9wPlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBzdHlsZXM6IFtgLnRleHQtZmllbGR7bWFyZ2luLWJvdHRvbToxMHB4fS50ZXh0LWZpZWxkLmRpc2FibGVke29wYWNpdHk6LjN9LnRleHQtZmllbGQuZGlzYWJsZWQgLmNvbnRlbnQsLnRleHQtZmllbGQuZGlzYWJsZWQgaW5wdXQsLnRleHQtZmllbGQuZGlzYWJsZWQgbGFiZWwsLnRleHQtZmllbGQuZGlzYWJsZWQgc3BhbntjdXJzb3I6bm90LWFsbG93ZWQhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQuZXJyb3IgcCwudGV4dC1maWVsZC5lcnJvciBzcGFue2NvbG9yOiNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIGlucHV0e2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkIC5jb250ZW50e3dpZHRoOjEwMCU7Y29sb3I6IzQ1NDY0ODtiYWNrZ3JvdW5kLWNvbG9yOiNmN2Y3Zjc7Ym9yZGVyLXJhZGl1czo2cHggNnB4IDAgMH0udGV4dC1maWVsZCAuY29udGVudCBsYWJlbHtwb3NpdGlvbjpyZWxhdGl2ZTtkaXNwbGF5OmJsb2NrO3dpZHRoOjEwMCU7bWluLWhlaWdodDo1MHB4fS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0e3Bvc2l0aW9uOmFic29sdXRlO3RvcDoxNXB4O3otaW5kZXg6MTt3aWR0aDoxMDAlO2ZvbnQtc2l6ZToxZW07Ym9yZGVyOjA7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgIzgwODI4NTt0cmFuc2l0aW9uOmJvcmRlci1jb2xvciAuMnMgZWFzZS1pbi1vdXQ7b3V0bGluZTowO2hlaWdodDozNXB4O2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OmZvY3Vze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmZmQyMDB9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6Zm9jdXMrc3BhbiwudGV4dC1maWVsZCAuY29udGVudCBpbnB1dDp2YWxpZCtzcGFue3RvcDo2cHg7Y3Vyc29yOmluaGVyaXQ7Zm9udC1zaXplOi44ZW07Y29sb3I6IzgwODI4NTtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuc3RhdHVze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICMwMDQ0OGR9LnRleHQtZmllbGQgLmNvbnRlbnQgc3Bhbntwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO3RvcDoyMHB4O3otaW5kZXg6Mjtmb250LXNpemU6MWVtO3RyYW5zaXRpb246YWxsIC4ycyBlYXNlLWluLW91dDt3aWR0aDoxMDAlO2N1cnNvcjp0ZXh0O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuaWNvbnt6LWluZGV4OjM7bWFyZ2luOjE1cHggMCAwO3JpZ2h0OjA7cG9zaXRpb246YWJzb2x1dGV9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb24gaXtmb250LXNpemU6MS4yZW07bGV0dGVyLXNwYWNpbmc6MTVweDtjdXJzb3I6cG9pbnRlcn0udGV4dC1maWVsZCAuY29udGVudCAuaWNvbiBpOm50aC1jaGlsZCgyKXtjb2xvcjojZmE1ZTVifS50ZXh0LWZpZWxkIC5tZXNzYWdle21pbi1oZWlnaHQ6MjBweH0udGV4dC1maWVsZCAubWVzc2FnZSBwe3BhZGRpbmc6MCAwIDAgMTVweDtmb250LXNpemU6LjhlbTtjb2xvcjojODA4Mjg1O21hcmdpbjotM3B4IDAgMH1gXSxcclxuXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUZXh0RmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHB1YmxpYyB2YWx1ZUlucHV0O1xyXG4gIGlucHV0U3R5bGU6IHN0cmluZztcclxuICBAT3V0cHV0KCkgcHVibGljIHZhbHVlVXNlcm5hbWUgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uUHJpbWFyeTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb25TZWNvbmRhcnk6IHN0cmluZztcclxuICBASW5wdXQoKSBzdGF0ZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuICBASW5wdXQoKSBwYXR0ZXJuOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuXHJcbiAgZ2V0VmFsdWVVc2VybmFtZSgpIHtcclxuICAgIHRoaXMudmFsdWVVc2VybmFtZS5lbWl0KHtcclxuICAgICAgdmFsdWU6IHRoaXMudmFsdWVJbnB1dCxcclxuICAgICAgc3RhdGU6IHRoaXMuaW5wdXRTdHlsZVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbiAgdmFsaWRhdGVGaWVsZCgpIHtcclxuICAgIGlmICh0aGlzLnZhbHVlSW5wdXQgIT09IG51bGwgJiYgdGhpcy52YWx1ZUlucHV0ICE9PSAnJykge1xyXG4gICAgICB0aGlzLmlucHV0U3R5bGUgPSAnc3RhdHVzJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuaW5wdXRTdHlsZSA9ICcnO1xyXG4gICAgfVxyXG4gICAgdGhpcy5nZXRWYWx1ZVVzZXJuYW1lKCk7XHJcbiAgfVxyXG5cclxuICBvblNldFN0YXRlQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICB0aGlzLnN0YXRlID0gZXZlbnQuc3RhdGU7XHJcbiAgICB0aGlzLmlucHV0U3R5bGUgPSBldmVudC5zdGF0ZTtcclxuICAgIGlmIChldmVudC5zdGF0ZSAhPT0gJycpIHtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJ05vIGN1bXBsZSBjb24gbGEgbG9uZ2l0dWQgbcOtbmltYSc7XHJcbiAgICAgIHRoaXMuaWNvblNlY29uZGFyeSA9ICdmYWwgZmEtdGltZXMnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICAgIHRoaXMuaWNvblNlY29uZGFyeSA9ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnZhbHVlSW5wdXQgPT09ICcnKSB7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9ICcnO1xyXG4gICAgICB0aGlzLnN0YXRlID0gJyc7XHJcbiAgICAgIHRoaXMuaWNvblNlY29uZGFyeSA9ICcnO1xyXG4gICAgfVxyXG4gICAgdGhpcy5nZXRWYWx1ZVVzZXJuYW1lKCk7XHJcbiAgfVxyXG5cclxufVxyXG5cclxuXHJcbiJdfQ==