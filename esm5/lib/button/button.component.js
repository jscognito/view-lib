/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var ButtonComponent = /** @class */ (function () {
    function ButtonComponent() {
    }
    /**
     * @return {?}
     */
    ButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-button',
                    template: "<button class=\"btn {{ type }}\">\n  {{ label }}\n</button>\n",
                    styles: [".btn{width:144px;height:40px;border-radius:20px;font-size:14px;font-weight:700;border:0;text-transform:uppercase}.btn.primary-gray{background-color:#58595b;color:#fff}.btn.primary-gray:hover{background-color:#363636}.btn.primary-gray:focus{background-color:#58595b}.btn.primary-yellow{background-color:#ffd200;color:#454648}.btn.primary-yellow:hover{background-color:#ffb500}.btn.primary-yellow:focus{background-color:#ffd200}.btn.primary-red{background-color:#fa5e5b;color:#fff}.btn.primary-red:hover{background-color:#d14d4a}.btn.primary-red:focus{background-color:#fa5e5b}.btn.white{background-color:#fff;color:#454648}.btn.white:hover{background-color:transparent;border:1px solid #fff}.btn.white:focus{background-color:#fff;color:#454648}.btn.secondary{border:1px solid #00448d;background-color:#fff;color:#00448d}.btn.secondary:hover{border-width:2px}.btn.secondary:focus{background-color:#e6e7e8}.btn.flat{border-radius:0;background-color:transparent;color:#333}.btn.flat:hover{color:#ffd200}.btn.flat:focus{color:#58595b}"]
                },] },
    ];
    /** @nocollapse */
    ButtonComponent.ctorParameters = function () { return []; };
    ButtonComponent.propDecorators = {
        type: [{ type: Input }],
        label: [{ type: Input }]
    };
    return ButtonComponent;
}());
export { ButtonComponent };
function ButtonComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    ButtonComponent.prototype.type;
    /** @type {?} */
    ButtonComponent.prototype.label;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2J1dHRvbi9idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQzs7SUFldkQ7S0FBaUI7Ozs7SUFFakIsa0NBQVE7OztJQUFSO0tBQ0M7O2dCQWhCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLFFBQVEsRUFBRSwrREFHWDtvQkFDQyxNQUFNLEVBQUUsQ0FBQyx1Z0NBQXVnQyxDQUFDO2lCQUNsaEM7Ozs7O3VCQUdFLEtBQUs7d0JBQ0wsS0FBSzs7MEJBYlI7O1NBVWEsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtYnV0dG9uJyxcclxuICB0ZW1wbGF0ZTogYDxidXR0b24gY2xhc3M9XCJidG4ge3sgdHlwZSB9fVwiPlxyXG4gIHt7IGxhYmVsIH19XHJcbjwvYnV0dG9uPlxyXG5gLFxyXG4gIHN0eWxlczogW2AuYnRue3dpZHRoOjE0NHB4O2hlaWdodDo0MHB4O2JvcmRlci1yYWRpdXM6MjBweDtmb250LXNpemU6MTRweDtmb250LXdlaWdodDo3MDA7Ym9yZGVyOjA7dGV4dC10cmFuc2Zvcm06dXBwZXJjYXNlfS5idG4ucHJpbWFyeS1ncmF5e2JhY2tncm91bmQtY29sb3I6IzU4NTk1Yjtjb2xvcjojZmZmfS5idG4ucHJpbWFyeS1ncmF5OmhvdmVye2JhY2tncm91bmQtY29sb3I6IzM2MzYzNn0uYnRuLnByaW1hcnktZ3JheTpmb2N1c3tiYWNrZ3JvdW5kLWNvbG9yOiM1ODU5NWJ9LmJ0bi5wcmltYXJ5LXllbGxvd3tiYWNrZ3JvdW5kLWNvbG9yOiNmZmQyMDA7Y29sb3I6IzQ1NDY0OH0uYnRuLnByaW1hcnkteWVsbG93OmhvdmVye2JhY2tncm91bmQtY29sb3I6I2ZmYjUwMH0uYnRuLnByaW1hcnkteWVsbG93OmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2ZmZDIwMH0uYnRuLnByaW1hcnktcmVke2JhY2tncm91bmQtY29sb3I6I2ZhNWU1Yjtjb2xvcjojZmZmfS5idG4ucHJpbWFyeS1yZWQ6aG92ZXJ7YmFja2dyb3VuZC1jb2xvcjojZDE0ZDRhfS5idG4ucHJpbWFyeS1yZWQ6Zm9jdXN7YmFja2dyb3VuZC1jb2xvcjojZmE1ZTVifS5idG4ud2hpdGV7YmFja2dyb3VuZC1jb2xvcjojZmZmO2NvbG9yOiM0NTQ2NDh9LmJ0bi53aGl0ZTpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O2JvcmRlcjoxcHggc29saWQgI2ZmZn0uYnRuLndoaXRlOmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2ZmZjtjb2xvcjojNDU0NjQ4fS5idG4uc2Vjb25kYXJ5e2JvcmRlcjoxcHggc29saWQgIzAwNDQ4ZDtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Y29sb3I6IzAwNDQ4ZH0uYnRuLnNlY29uZGFyeTpob3Zlcntib3JkZXItd2lkdGg6MnB4fS5idG4uc2Vjb25kYXJ5OmZvY3Vze2JhY2tncm91bmQtY29sb3I6I2U2ZTdlOH0uYnRuLmZsYXR7Ym9yZGVyLXJhZGl1czowO2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Y29sb3I6IzMzM30uYnRuLmZsYXQ6aG92ZXJ7Y29sb3I6I2ZmZDIwMH0uYnRuLmZsYXQ6Zm9jdXN7Y29sb3I6IzU4NTk1Yn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19