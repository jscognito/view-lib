/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    /**
     * @return {?}
     */
    FooterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    FooterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-footer',
                    template: "<footer>\n  <div class=\"contact-information text-center\">\n    <div>\n      <i class=\"footer-icon fal fa-phone\"></i>\n      Sucursal Telef\u00F3nica : 507-306-4700\n    </div>\n    <div>Copyright Banistmo SA. 2018</div>\n  </div>\n</footer>\n\n\n\n",
                    styles: ["footer{width:100%;background-color:#e6e7e8;padding:25px 0}footer .contact-information{font-size:.8em}footer .contact-information i{-webkit-transform:scaleX(-1);transform:scaleX(-1)}"]
                },] },
    ];
    /** @nocollapse */
    FooterComponent.ctorParameters = function () { return []; };
    return FooterComponent;
}());
export { FooterComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFpQixNQUFNLGVBQWUsQ0FBQzs7SUFxQnZEO0tBQWlCOzs7O0lBRWpCLGtDQUFROzs7SUFBUjtLQUNDOztnQkF0QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0QixRQUFRLEVBQUUsOFBBWVg7b0JBQ0MsTUFBTSxFQUFFLENBQUMsdUxBQXVMLENBQUM7aUJBQ2xNOzs7OzBCQWxCRDs7U0FtQmEsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtZm9vdGVyJyxcclxuICB0ZW1wbGF0ZTogYDxmb290ZXI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRhY3QtaW5mb3JtYXRpb24gdGV4dC1jZW50ZXJcIj5cclxuICAgIDxkaXY+XHJcbiAgICAgIDxpIGNsYXNzPVwiZm9vdGVyLWljb24gZmFsIGZhLXBob25lXCI+PC9pPlxyXG4gICAgICBTdWN1cnNhbCBUZWxlZsOzbmljYSA6IDUwNy0zMDYtNDcwMFxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2PkNvcHlyaWdodCBCYW5pc3RtbyBTQS4gMjAxODwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Zvb3Rlcj5cclxuXHJcblxyXG5cclxuYCxcclxuICBzdHlsZXM6IFtgZm9vdGVye3dpZHRoOjEwMCU7YmFja2dyb3VuZC1jb2xvcjojZTZlN2U4O3BhZGRpbmc6MjVweCAwfWZvb3RlciAuY29udGFjdC1pbmZvcm1hdGlvbntmb250LXNpemU6LjhlbX1mb290ZXIgLmNvbnRhY3QtaW5mb3JtYXRpb24gaXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZVgoLTEpO3RyYW5zZm9ybTpzY2FsZVgoLTEpfWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb290ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==