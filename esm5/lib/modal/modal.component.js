/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var ModalComponent = /** @class */ (function () {
    function ModalComponent() {
    }
    /**
     * @return {?}
     */
    ModalComponent.prototype.openModal = /**
     * @return {?}
     */
    function () {
        this.display = 'block';
        document.body.style.overflow = 'hidden';
    };
    /**
     * @return {?}
     */
    ModalComponent.prototype.closeModal = /**
     * @return {?}
     */
    function () {
        this.display = 'none';
        document.body.style.overflow = 'auto';
    };
    ModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-modal',
                    template: "<div class=\"modal-container\" [style.display]=display id=\"{{ id }}\">\n  <div class=\"centered\">\n    <div class=\"modal-content\">\n      <div class=\"row modal-header align-items-center\">\n        <div class=\"col-12 text-center\">\n          <h2>{{ titleModal }}</h2>\n        </div>\n        <i routerLink=\"/login\" class=\"fal fa-times close\" (click)=\"closeModal()\" id=\"btn_close\"></i>\n      </div>\n      <div class=\"modal-body text-center\">\n        <i class=\"icon-exit\"></i>\n        <p class=\"session-message\">{{ messageContent }}</p>\n        <div class=\"row justify-content-center actions\">\n          <div class=\"col-5\">\n            <app-button id=\"btn_accept\" (click)=\"closeModal()\" routerLink=\"/login\" type=\"primary-gray\" label=\"{{ labelActionBtn }}\"></app-button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n",
                    styles: ["body{overflow:hidden}.modal-container{display:none;position:fixed;z-index:100;left:0;top:0;width:100%;height:100%;-webkit-backdrop-filter:blur(3px);backdrop-filter:blur(3px);background-color:rgba(247,247,247,.8)}.modal-container .centered{display:table;margin:12% auto}.modal-content{min-width:500px;border-radius:8px;box-shadow:0 2px 6px 0 rgba(0,0,0,.07);background-color:#fff;border:1px solid #f1f1f1;align-items:center}.modal-content .modal-header{width:100%;padding:15px;border-bottom:1px solid rgba(151,151,151,.15);color:#454648;margin:0;position:relative}.modal-content .modal-header .close{position:absolute;right:15px;cursor:pointer;font-size:1.5em;color:#58595b;opacity:.5}.modal-content .modal-header h2{font-size:1.12em;font-weight:700;margin-bottom:0}.modal-content .modal-body{padding:30px 0}.modal-content .modal-body .icon-exit{padding-bottom:30px;content:url(../../../assets/icons/exit.svg)}.modal-content .modal-body .session-message{padding-bottom:30px}@media (max-width:576px){.icon{position:absolute;right:10px;top:-32px}.modal-content{min-width:320px!important}.centered{margin:35% auto}}"]
                },] },
    ];
    ModalComponent.propDecorators = {
        id: [{ type: Input }],
        titleModal: [{ type: Input }],
        messageContent: [{ type: Input }],
        labelActionBtn: [{ type: Input }],
        iconModalBtn: [{ type: Input }]
    };
    return ModalComponent;
}());
export { ModalComponent };
function ModalComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    ModalComponent.prototype.id;
    /** @type {?} */
    ModalComponent.prototype.titleModal;
    /** @type {?} */
    ModalComponent.prototype.messageContent;
    /** @type {?} */
    ModalComponent.prototype.labelActionBtn;
    /** @type {?} */
    ModalComponent.prototype.iconModalBtn;
    /** @type {?} */
    ModalComponent.prototype.display;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvbW9kYWwvbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQzs7Ozs7OztJQXNDL0Msa0NBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztLQUN6Qzs7OztJQUVELG1DQUFVOzs7SUFBVjtRQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ3RCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7S0FDdkM7O2dCQTVDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLFFBQVEsRUFBRSw0M0JBcUJYO29CQUNDLE1BQU0sRUFBRSxDQUFDLHlsQ0FBeWxDLENBQUM7aUJBQ3BtQzs7O3FCQUdFLEtBQUs7NkJBQ0wsS0FBSztpQ0FDTCxLQUFLO2lDQUNMLEtBQUs7K0JBQ0wsS0FBSzs7eUJBbENSOztTQTRCYSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbW9kYWwnLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRhaW5lclwiIFtzdHlsZS5kaXNwbGF5XT1kaXNwbGF5IGlkPVwie3sgaWQgfX1cIj5cclxuICA8ZGl2IGNsYXNzPVwiY2VudGVyZWRcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJyb3cgbW9kYWwtaGVhZGVyIGFsaWduLWl0ZW1zLWNlbnRlclwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTIgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgICAgIDxoMj57eyB0aXRsZU1vZGFsIH19PC9oMj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aSByb3V0ZXJMaW5rPVwiL2xvZ2luXCIgY2xhc3M9XCJmYWwgZmEtdGltZXMgY2xvc2VcIiAoY2xpY2spPVwiY2xvc2VNb2RhbCgpXCIgaWQ9XCJidG5fY2xvc2VcIj48L2k+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keSB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgIDxpIGNsYXNzPVwiaWNvbi1leGl0XCI+PC9pPlxyXG4gICAgICAgIDxwIGNsYXNzPVwic2Vzc2lvbi1tZXNzYWdlXCI+e3sgbWVzc2FnZUNvbnRlbnQgfX08L3A+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyIGFjdGlvbnNcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtNVwiPlxyXG4gICAgICAgICAgICA8YXBwLWJ1dHRvbiBpZD1cImJ0bl9hY2NlcHRcIiAoY2xpY2spPVwiY2xvc2VNb2RhbCgpXCIgcm91dGVyTGluaz1cIi9sb2dpblwiIHR5cGU9XCJwcmltYXJ5LWdyYXlcIiBsYWJlbD1cInt7IGxhYmVsQWN0aW9uQnRuIH19XCI+PC9hcHAtYnV0dG9uPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2Bib2R5e292ZXJmbG93OmhpZGRlbn0ubW9kYWwtY29udGFpbmVye2Rpc3BsYXk6bm9uZTtwb3NpdGlvbjpmaXhlZDt6LWluZGV4OjEwMDtsZWZ0OjA7dG9wOjA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTstd2Via2l0LWJhY2tkcm9wLWZpbHRlcjpibHVyKDNweCk7YmFja2Ryb3AtZmlsdGVyOmJsdXIoM3B4KTtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMjQ3LDI0NywyNDcsLjgpfS5tb2RhbC1jb250YWluZXIgLmNlbnRlcmVke2Rpc3BsYXk6dGFibGU7bWFyZ2luOjEyJSBhdXRvfS5tb2RhbC1jb250ZW50e21pbi13aWR0aDo1MDBweDtib3JkZXItcmFkaXVzOjhweDtib3gtc2hhZG93OjAgMnB4IDZweCAwIHJnYmEoMCwwLDAsLjA3KTtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7Ym9yZGVyOjFweCBzb2xpZCAjZjFmMWYxO2FsaWduLWl0ZW1zOmNlbnRlcn0ubW9kYWwtY29udGVudCAubW9kYWwtaGVhZGVye3dpZHRoOjEwMCU7cGFkZGluZzoxNXB4O2JvcmRlci1ib3R0b206MXB4IHNvbGlkIHJnYmEoMTUxLDE1MSwxNTEsLjE1KTtjb2xvcjojNDU0NjQ4O21hcmdpbjowO3Bvc2l0aW9uOnJlbGF0aXZlfS5tb2RhbC1jb250ZW50IC5tb2RhbC1oZWFkZXIgLmNsb3Nle3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjE1cHg7Y3Vyc29yOnBvaW50ZXI7Zm9udC1zaXplOjEuNWVtO2NvbG9yOiM1ODU5NWI7b3BhY2l0eTouNX0ubW9kYWwtY29udGVudCAubW9kYWwtaGVhZGVyIGgye2ZvbnQtc2l6ZToxLjEyZW07Zm9udC13ZWlnaHQ6NzAwO21hcmdpbi1ib3R0b206MH0ubW9kYWwtY29udGVudCAubW9kYWwtYm9keXtwYWRkaW5nOjMwcHggMH0ubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuaWNvbi1leGl0e3BhZGRpbmctYm90dG9tOjMwcHg7Y29udGVudDp1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb25zL2V4aXQuc3ZnKX0ubW9kYWwtY29udGVudCAubW9kYWwtYm9keSAuc2Vzc2lvbi1tZXNzYWdle3BhZGRpbmctYm90dG9tOjMwcHh9QG1lZGlhIChtYXgtd2lkdGg6NTc2cHgpey5pY29ue3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjEwcHg7dG9wOi0zMnB4fS5tb2RhbC1jb250ZW50e21pbi13aWR0aDozMjBweCFpbXBvcnRhbnR9LmNlbnRlcmVke21hcmdpbjozNSUgYXV0b319YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE1vZGFsQ29tcG9uZW50IHtcclxuXHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBASW5wdXQoKSB0aXRsZU1vZGFsOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgbWVzc2FnZUNvbnRlbnQ6IHN0cmluZztcclxuICBASW5wdXQoKSBsYWJlbEFjdGlvbkJ0bjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb25Nb2RhbEJ0bjogc3RyaW5nO1xyXG5cclxuICBkaXNwbGF5OiBzdHJpbmc7XHJcblxyXG4gIG9wZW5Nb2RhbCgpIHtcclxuICAgIHRoaXMuZGlzcGxheSA9ICdibG9jayc7XHJcbiAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XHJcbiAgfVxyXG5cclxuICBjbG9zZU1vZGFsKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdhdXRvJztcclxuICB9XHJcblxyXG59XHJcbiJdfQ==