/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
var FooterExternalComponent = /** @class */ (function () {
    function FooterExternalComponent() {
    }
    /**
     * @return {?}
     */
    FooterExternalComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    FooterExternalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-footer-external',
                    template: "<footer>\n  <div class=\"container\">\n    <div class=\"row justify-content-md-center text-center\">\n      <div class=\"col col-lg-2\">\n        <a href=\"\" id=\"info_schedules\">Horarios</a>\n      </div>\n      <div class=\"col col-lg-2\">\n        <a href=\"\" id=\"info_commissions\">Comisiones</a>\n      </div>\n      <div class=\"col col-lg-2\">\n        <a href=\"\" id=\"info_security\">Seguridad</a>\n      </div>\n    </div>\n  </div>\n</footer>\n",
                    styles: ["footer{width:100%;background-color:#e6e7e8;padding:25px 0 0}footer a{color:#454648;font-size:.8em;line-height:34px}footer a:hover{font-weight:700;text-decoration:none}"]
                },] },
    ];
    /** @nocollapse */
    FooterExternalComponent.ctorParameters = function () { return []; };
    return FooterExternalComponent;
}());
export { FooterExternalComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLWV4dGVybmFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Zvb3Rlci1leHRlcm5hbC9mb290ZXItZXh0ZXJuYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQXdCaEQ7S0FBaUI7Ozs7SUFFakIsMENBQVE7OztJQUFSO0tBQ0M7O2dCQXpCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsUUFBUSxFQUFFLDhjQWVYO29CQUNDLE1BQU0sRUFBRSxDQUFDLHlLQUF5SyxDQUFDO2lCQUNwTDs7OztrQ0FyQkQ7O1NBc0JhLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1mb290ZXItZXh0ZXJuYWwnLFxyXG4gIHRlbXBsYXRlOiBgPGZvb3Rlcj5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicm93IGp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbCBjb2wtbGctMlwiPlxyXG4gICAgICAgIDxhIGhyZWY9XCJcIiBpZD1cImluZm9fc2NoZWR1bGVzXCI+SG9yYXJpb3M8L2E+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sIGNvbC1sZy0yXCI+XHJcbiAgICAgICAgPGEgaHJlZj1cIlwiIGlkPVwiaW5mb19jb21taXNzaW9uc1wiPkNvbWlzaW9uZXM8L2E+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sIGNvbC1sZy0yXCI+XHJcbiAgICAgICAgPGEgaHJlZj1cIlwiIGlkPVwiaW5mb19zZWN1cml0eVwiPlNlZ3VyaWRhZDwvYT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC9mb290ZXI+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYGZvb3Rlcnt3aWR0aDoxMDAlO2JhY2tncm91bmQtY29sb3I6I2U2ZTdlODtwYWRkaW5nOjI1cHggMCAwfWZvb3RlciBhe2NvbG9yOiM0NTQ2NDg7Zm9udC1zaXplOi44ZW07bGluZS1oZWlnaHQ6MzRweH1mb290ZXIgYTpob3Zlcntmb250LXdlaWdodDo3MDA7dGV4dC1kZWNvcmF0aW9uOm5vbmV9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvb3RlckV4dGVybmFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=