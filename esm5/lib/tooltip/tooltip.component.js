/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var TooltipComponent = /** @class */ (function () {
    function TooltipComponent() {
    }
    /**
     * @return {?}
     */
    TooltipComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    TooltipComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-tooltip',
                    template: "<a  class=\"tooltip\" title=\"{{ title}}\">\n  <i class=\"{{ icon }}\"></i>\n</a>",
                    styles: [".tooltip{display:inline;position:relative}.tooltip:hover:after{background:rgba(0,0,0,.8);border-radius:5px;bottom:26px;color:#fff;content:attr(title);left:-600%;padding:5px 15px;position:absolute;z-index:98;width:220px}.tooltip:hover:before{border:solid;border-color:#333 transparent;border-width:6px 6px 0;bottom:20px;content:\"\";left:0;position:absolute;z-index:99}.tooltip i{font-size:1.2em;letter-spacing:15px;cursor:pointer}"]
                },] },
    ];
    /** @nocollapse */
    TooltipComponent.ctorParameters = function () { return []; };
    TooltipComponent.propDecorators = {
        title: [{ type: Input }],
        icon: [{ type: Input }]
    };
    return TooltipComponent;
}());
export { TooltipComponent };
function TooltipComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TooltipComponent.prototype.title;
    /** @type {?} */
    TooltipComponent.prototype.icon;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aWV3LWxpYi8iLCJzb3VyY2VzIjpbImxpYi90b29sdGlwL3Rvb2x0aXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQzs7SUFjdkQ7S0FBaUI7Ozs7SUFFakIsbUNBQVE7OztJQUFSO0tBQ0M7O2dCQWZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsUUFBUSxFQUFFLG1GQUVQO29CQUNILE1BQU0sRUFBRSxDQUFDLGdiQUE4YSxDQUFDO2lCQUN6Yjs7Ozs7d0JBR0UsS0FBSzt1QkFDTCxLQUFLOzsyQkFaUjs7U0FTYSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRvb2x0aXAnLFxyXG4gIHRlbXBsYXRlOiBgPGEgIGNsYXNzPVwidG9vbHRpcFwiIHRpdGxlPVwie3sgdGl0bGV9fVwiPlxyXG4gIDxpIGNsYXNzPVwie3sgaWNvbiB9fVwiPjwvaT5cclxuPC9hPmAsXHJcbiAgc3R5bGVzOiBbYC50b29sdGlwe2Rpc3BsYXk6aW5saW5lO3Bvc2l0aW9uOnJlbGF0aXZlfS50b29sdGlwOmhvdmVyOmFmdGVye2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuOCk7Ym9yZGVyLXJhZGl1czo1cHg7Ym90dG9tOjI2cHg7Y29sb3I6I2ZmZjtjb250ZW50OmF0dHIodGl0bGUpO2xlZnQ6LTYwMCU7cGFkZGluZzo1cHggMTVweDtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4Ojk4O3dpZHRoOjIyMHB4fS50b29sdGlwOmhvdmVyOmJlZm9yZXtib3JkZXI6c29saWQ7Ym9yZGVyLWNvbG9yOiMzMzMgdHJhbnNwYXJlbnQ7Ym9yZGVyLXdpZHRoOjZweCA2cHggMDtib3R0b206MjBweDtjb250ZW50OlwiXCI7bGVmdDowO3Bvc2l0aW9uOmFic29sdXRlO3otaW5kZXg6OTl9LnRvb2x0aXAgaXtmb250LXNpemU6MS4yZW07bGV0dGVyLXNwYWNpbmc6MTVweDtjdXJzb3I6cG9pbnRlcn1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9vbHRpcENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHRpdGxlOnN0cmluZztcclxuICBASW5wdXQoKSBpY29uOnN0cmluZztcclxuICBcclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==