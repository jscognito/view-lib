/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var HyperlinkComponent = /** @class */ (function () {
    function HyperlinkComponent() {
    }
    /**
     * @return {?}
     */
    HyperlinkComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    HyperlinkComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-hyperlink',
                    template: "<a href=\"{{ url }}\">\n  {{ label }}\n</a>\n",
                    styles: ["a{font-size:.85em;color:#00448d;text-decoration:underline}a:hover{font-weight:700}"]
                },] },
    ];
    /** @nocollapse */
    HyperlinkComponent.ctorParameters = function () { return []; };
    HyperlinkComponent.propDecorators = {
        label: [{ type: Input }],
        url: [{ type: Input }]
    };
    return HyperlinkComponent;
}());
export { HyperlinkComponent };
function HyperlinkComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    HyperlinkComponent.prototype.label;
    /** @type {?} */
    HyperlinkComponent.prototype.url;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHlwZXJsaW5rLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2h5cGVybGluay9oeXBlcmxpbmsuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQzs7SUFjckQ7S0FDQzs7OztJQUVELHFDQUFROzs7SUFBUjtLQUNDOztnQkFoQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxlQUFlO29CQUN6QixRQUFRLEVBQUUsK0NBR1g7b0JBQ0MsTUFBTSxFQUFFLENBQUMsb0ZBQW9GLENBQUM7aUJBQy9GOzs7Ozt3QkFFRSxLQUFLO3NCQUNMLEtBQUs7OzZCQVpSOztTQVVhLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWh5cGVybGluaycsXHJcbiAgdGVtcGxhdGU6IGA8YSBocmVmPVwie3sgdXJsIH19XCI+XHJcbiAge3sgbGFiZWwgfX1cclxuPC9hPlxyXG5gLFxyXG4gIHN0eWxlczogW2Bhe2ZvbnQtc2l6ZTouODVlbTtjb2xvcjojMDA0NDhkO3RleHQtZGVjb3JhdGlvbjp1bmRlcmxpbmV9YTpob3Zlcntmb250LXdlaWdodDo3MDB9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEh5cGVybGlua0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuICBASW5wdXQoKSB1cmw6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==