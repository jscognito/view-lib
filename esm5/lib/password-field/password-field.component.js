/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
var PasswordFieldComponent = /** @class */ (function () {
    function PasswordFieldComponent() {
        this.valuePassword = new EventEmitter();
        this.class = '';
    }
    /**
     * @return {?}
     */
    PasswordFieldComponent.prototype.getValuePassword = /**
     * @return {?}
     */
    function () {
        this.valuePassword.emit(this.valueInputPassword);
    };
    /**
     * @return {?}
     */
    PasswordFieldComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.type = 'password';
    };
    /**
     * @return {?}
     */
    PasswordFieldComponent.prototype.validateField = /**
     * @return {?}
     */
    function () {
        this.getValuePassword();
        (this.valueInputPassword !== null && this.valueInputPassword !== '') ? this.emptyInput = 'status' : this.emptyInput = '';
    };
    /**
     * @return {?}
     */
    PasswordFieldComponent.prototype.clearField = /**
     * @return {?}
     */
    function () {
        this.valueInputPassword = '';
        this.state = '';
        this.message = '';
    };
    /**
     * @param {?} event
     * @return {?}
     */
    PasswordFieldComponent.prototype.onSetStateChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.state = event.state;
        if (event.state !== '') {
            this.message = 'No cumple con la longitud mínima';
        }
        else {
            this.message = '';
        }
    };
    PasswordFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-password-field',
                    template: "<div class=\"text-field {{ state }}\">\n  <div class=\"content\">\n    <label>\n      <input type=\"{{ type }}\" id=\"{{ id }}\" class=\"{{ emptyInput }}\" [(ngModel)]=\"valueInputPassword\" name=\"text-field\" required (change)=\"validateField()\"\n        autocomplete=\"off\" onpaste=\"return false\" onCopy=\"return false\" input-directive [minLength]=8 [maxLength]=16 [pattern]=\"pattern\"\n        (setState)=\"onSetStateChange($event)\">\n      <span>{{ label }}</span>\n      <div class=\"icon\">\n        <i [class]=\"'fal fa-eye' + class\" (mousedown)=\"class='-slash';type='text'\" (mouseup)=\"class='';type='password'\" ></i>\n        <i *ngIf=\"state !== 'error'\" class=\"fal fa-info-circle\"></i>\n        <i *ngIf=\"state === 'error'\" class=\"fal fa-times error\" (click)=\"clearField()\"></i>\n      </div>\n    </label>\n  </div>\n  <div class=\"message\">\n    <p>{{ message }}</p>\n  </div>\n</div>\n",
                    styles: [".text-field{margin-bottom:10px}.text-field.disabled{opacity:.3}.text-field.disabled .content,.text-field.disabled input,.text-field.disabled label,.text-field.disabled span{cursor:not-allowed!important}.text-field.error .icon .error,.text-field.error input:focus+span,.text-field.error p,.text-field.error span{color:#fa5e5b!important}.text-field.error input{border-bottom:2px solid #fa5e5b!important}.text-field .content{width:100%;color:#454648;background-color:#f7f7f7;border-radius:6px 6px 0 0}.text-field .content label{position:relative;display:block;width:100%;min-height:50px}.text-field .content input{position:absolute;top:15px;z-index:1;width:100%;font-size:1em;border:0;border-bottom:1px solid #808285;transition:border-color .2s ease-in-out;outline:0;height:35px;background-color:transparent;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content input:focus{border-bottom:2px solid #ffd200}.text-field .content input:focus+span,.text-field .content input:valid+span{top:6px;cursor:inherit;font-size:.8em;color:#808285;padding-left:15px}.text-field .content .status{border-bottom:2px solid #00448d}.text-field .content span{position:absolute;display:block;top:20px;z-index:2;font-size:1em;transition:all .2s ease-in-out;width:100%;cursor:text;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding-left:15px}.text-field .content .icon{z-index:3;margin:15px 0 0;right:0;position:absolute}.text-field .content .icon i{font-size:1.2em;letter-spacing:8px;cursor:pointer}.text-field .message{min-height:20px}.text-field .message p{padding:0 0 0 15px;font-size:.8em;color:#808285;margin:-3px 0 0}"]
                },] },
    ];
    /** @nocollapse */
    PasswordFieldComponent.ctorParameters = function () { return []; };
    PasswordFieldComponent.propDecorators = {
        valuePassword: [{ type: Output }],
        label: [{ type: Input }],
        state: [{ type: Input }],
        message: [{ type: Input }],
        type: [{ type: Input }],
        pattern: [{ type: Input }],
        icon: [{ type: Input }],
        id: [{ type: Input }]
    };
    return PasswordFieldComponent;
}());
export { PasswordFieldComponent };
function PasswordFieldComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    PasswordFieldComponent.prototype.valueInputPassword;
    /** @type {?} */
    PasswordFieldComponent.prototype.emptyInput;
    /** @type {?} */
    PasswordFieldComponent.prototype.valuePassword;
    /** @type {?} */
    PasswordFieldComponent.prototype.label;
    /** @type {?} */
    PasswordFieldComponent.prototype.state;
    /** @type {?} */
    PasswordFieldComponent.prototype.message;
    /** @type {?} */
    PasswordFieldComponent.prototype.type;
    /** @type {?} */
    PasswordFieldComponent.prototype.pattern;
    /** @type {?} */
    PasswordFieldComponent.prototype.icon;
    /** @type {?} */
    PasswordFieldComponent.prototype.id;
    /** @type {?} */
    PasswordFieldComponent.prototype.class;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFzc3dvcmQtZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvcGFzc3dvcmQtZmllbGQvcGFzc3dvcmQtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFDLE1BQU0sZUFBZSxDQUFDOztJQXdDNUU7NkJBWGlDLElBQUksWUFBWSxFQUFFO3FCQVNwQyxFQUFFO0tBSWhCOzs7O0lBQ0QsaURBQWdCOzs7SUFBaEI7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztLQUNsRDs7OztJQUVELHlDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO0tBRXhCOzs7O0lBRUQsOENBQWE7OztJQUFiO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDekIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO0tBQ3pIOzs7O0lBQ0QsMkNBQVU7OztJQUFWO1FBQ0UsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztLQUNuQjs7Ozs7SUFFRCxpREFBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBSztRQUNwQixJQUFJLENBQUMsS0FBSyxHQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDdkIsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFLENBQUMsQ0FBQSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsa0NBQWtDLENBQUE7U0FDbEQ7UUFBQyxJQUFJLENBQUEsQ0FBQztZQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1NBQ25CO0tBQ0Y7O2dCQW5FRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsUUFBUSxFQUFFLDI1QkFrQlg7b0JBQ0MsTUFBTSxFQUFFLENBQUMsb3FEQUFvcUQsQ0FBQztpQkFDL3FEOzs7OztnQ0FLRSxNQUFNO3dCQUNOLEtBQUs7d0JBQ0wsS0FBSzswQkFDTCxLQUFLO3VCQUNMLEtBQUs7MEJBQ0wsS0FBSzt1QkFDTCxLQUFLO3FCQUNMLEtBQUs7O2lDQXBDUjs7U0F5QmEsc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1wYXNzd29yZC1maWVsZCcsXHJcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwidGV4dC1maWVsZCB7eyBzdGF0ZSB9fVwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XHJcbiAgICA8bGFiZWw+XHJcbiAgICAgIDxpbnB1dCB0eXBlPVwie3sgdHlwZSB9fVwiIGlkPVwie3sgaWQgfX1cIiBjbGFzcz1cInt7IGVtcHR5SW5wdXQgfX1cIiBbKG5nTW9kZWwpXT1cInZhbHVlSW5wdXRQYXNzd29yZFwiIG5hbWU9XCJ0ZXh0LWZpZWxkXCIgcmVxdWlyZWQgKGNoYW5nZSk9XCJ2YWxpZGF0ZUZpZWxkKClcIlxyXG4gICAgICAgIGF1dG9jb21wbGV0ZT1cIm9mZlwiIG9ucGFzdGU9XCJyZXR1cm4gZmFsc2VcIiBvbkNvcHk9XCJyZXR1cm4gZmFsc2VcIiBpbnB1dC1kaXJlY3RpdmUgW21pbkxlbmd0aF09OCBbbWF4TGVuZ3RoXT0xNiBbcGF0dGVybl09XCJwYXR0ZXJuXCJcclxuICAgICAgICAoc2V0U3RhdGUpPVwib25TZXRTdGF0ZUNoYW5nZSgkZXZlbnQpXCI+XHJcbiAgICAgIDxzcGFuPnt7IGxhYmVsIH19PC9zcGFuPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaWNvblwiPlxyXG4gICAgICAgIDxpIFtjbGFzc109XCInZmFsIGZhLWV5ZScgKyBjbGFzc1wiIChtb3VzZWRvd24pPVwiY2xhc3M9Jy1zbGFzaCc7dHlwZT0ndGV4dCdcIiAobW91c2V1cCk9XCJjbGFzcz0nJzt0eXBlPSdwYXNzd29yZCdcIiA+PC9pPlxyXG4gICAgICAgIDxpICpuZ0lmPVwic3RhdGUgIT09ICdlcnJvcidcIiBjbGFzcz1cImZhbCBmYS1pbmZvLWNpcmNsZVwiPjwvaT5cclxuICAgICAgICA8aSAqbmdJZj1cInN0YXRlID09PSAnZXJyb3InXCIgY2xhc3M9XCJmYWwgZmEtdGltZXMgZXJyb3JcIiAoY2xpY2spPVwiY2xlYXJGaWVsZCgpXCI+PC9pPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvbGFiZWw+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cIm1lc3NhZ2VcIj5cclxuICAgIDxwPnt7IG1lc3NhZ2UgfX08L3A+XHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AudGV4dC1maWVsZHttYXJnaW4tYm90dG9tOjEwcHh9LnRleHQtZmllbGQuZGlzYWJsZWR7b3BhY2l0eTouM30udGV4dC1maWVsZC5kaXNhYmxlZCAuY29udGVudCwudGV4dC1maWVsZC5kaXNhYmxlZCBpbnB1dCwudGV4dC1maWVsZC5kaXNhYmxlZCBsYWJlbCwudGV4dC1maWVsZC5kaXNhYmxlZCBzcGFue2N1cnNvcjpub3QtYWxsb3dlZCFpbXBvcnRhbnR9LnRleHQtZmllbGQuZXJyb3IgLmljb24gLmVycm9yLC50ZXh0LWZpZWxkLmVycm9yIGlucHV0OmZvY3VzK3NwYW4sLnRleHQtZmllbGQuZXJyb3IgcCwudGV4dC1maWVsZC5lcnJvciBzcGFue2NvbG9yOiNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkLmVycm9yIGlucHV0e2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmYTVlNWIhaW1wb3J0YW50fS50ZXh0LWZpZWxkIC5jb250ZW50e3dpZHRoOjEwMCU7Y29sb3I6IzQ1NDY0ODtiYWNrZ3JvdW5kLWNvbG9yOiNmN2Y3Zjc7Ym9yZGVyLXJhZGl1czo2cHggNnB4IDAgMH0udGV4dC1maWVsZCAuY29udGVudCBsYWJlbHtwb3NpdGlvbjpyZWxhdGl2ZTtkaXNwbGF5OmJsb2NrO3dpZHRoOjEwMCU7bWluLWhlaWdodDo1MHB4fS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0e3Bvc2l0aW9uOmFic29sdXRlO3RvcDoxNXB4O3otaW5kZXg6MTt3aWR0aDoxMDAlO2ZvbnQtc2l6ZToxZW07Ym9yZGVyOjA7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgIzgwODI4NTt0cmFuc2l0aW9uOmJvcmRlci1jb2xvciAuMnMgZWFzZS1pbi1vdXQ7b3V0bGluZTowO2hlaWdodDozNXB4O2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Ym94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94Oy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94O3BhZGRpbmctbGVmdDoxNXB4fS50ZXh0LWZpZWxkIC5jb250ZW50IGlucHV0OmZvY3Vze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICNmZmQyMDB9LnRleHQtZmllbGQgLmNvbnRlbnQgaW5wdXQ6Zm9jdXMrc3BhbiwudGV4dC1maWVsZCAuY29udGVudCBpbnB1dDp2YWxpZCtzcGFue3RvcDo2cHg7Y3Vyc29yOmluaGVyaXQ7Zm9udC1zaXplOi44ZW07Y29sb3I6IzgwODI4NTtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuc3RhdHVze2JvcmRlci1ib3R0b206MnB4IHNvbGlkICMwMDQ0OGR9LnRleHQtZmllbGQgLmNvbnRlbnQgc3Bhbntwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO3RvcDoyMHB4O3otaW5kZXg6Mjtmb250LXNpemU6MWVtO3RyYW5zaXRpb246YWxsIC4ycyBlYXNlLWluLW91dDt3aWR0aDoxMDAlO2N1cnNvcjp0ZXh0O2JveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDtwYWRkaW5nLWxlZnQ6MTVweH0udGV4dC1maWVsZCAuY29udGVudCAuaWNvbnt6LWluZGV4OjM7bWFyZ2luOjE1cHggMCAwO3JpZ2h0OjA7cG9zaXRpb246YWJzb2x1dGV9LnRleHQtZmllbGQgLmNvbnRlbnQgLmljb24gaXtmb250LXNpemU6MS4yZW07bGV0dGVyLXNwYWNpbmc6OHB4O2N1cnNvcjpwb2ludGVyfS50ZXh0LWZpZWxkIC5tZXNzYWdle21pbi1oZWlnaHQ6MjBweH0udGV4dC1maWVsZCAubWVzc2FnZSBwe3BhZGRpbmc6MCAwIDAgMTVweDtmb250LXNpemU6LjhlbTtjb2xvcjojODA4Mjg1O21hcmdpbjotM3B4IDAgMH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFzc3dvcmRGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHZhbHVlSW5wdXRQYXNzd29yZDogYW55O1xyXG4gIGVtcHR5SW5wdXQ6IHN0cmluZztcclxuICBAT3V0cHV0KCkgcHVibGljIHZhbHVlUGFzc3dvcmQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuICBASW5wdXQoKSBzdGF0ZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgcGF0dGVybjogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG5cclxuICBwdWJsaWMgY2xhc3MgPSAnJztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcblxyXG4gIH1cclxuICBnZXRWYWx1ZVBhc3N3b3JkKCkge1xyXG4gICAgdGhpcy52YWx1ZVBhc3N3b3JkLmVtaXQodGhpcy52YWx1ZUlucHV0UGFzc3dvcmQpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnR5cGUgPSAncGFzc3dvcmQnO1xyXG5cclxuICB9XHJcblxyXG4gIHZhbGlkYXRlRmllbGQoKSB7XHJcbiAgICB0aGlzLmdldFZhbHVlUGFzc3dvcmQoKTtcclxuICAgKHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkICE9PSBudWxsICYmIHRoaXMudmFsdWVJbnB1dFBhc3N3b3JkICE9PSAnJykgPyB0aGlzLmVtcHR5SW5wdXQgPSAnc3RhdHVzJyA6IHRoaXMuZW1wdHlJbnB1dCA9ICcnO1xyXG4gIH1cclxuICBjbGVhckZpZWxkKCkge1xyXG4gICAgdGhpcy52YWx1ZUlucHV0UGFzc3dvcmQgPSAnJztcclxuICAgIHRoaXMuc3RhdGUgPSAnJztcclxuICAgIHRoaXMubWVzc2FnZSA9ICcnO1xyXG4gIH1cclxuXHJcbiAgb25TZXRTdGF0ZUNoYW5nZShldmVudCl7XHJcbiAgICB0aGlzLnN0YXRlPWV2ZW50LnN0YXRlO1xyXG4gICAgaWYoZXZlbnQuc3RhdGUgIT09ICcnKXtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJ05vIGN1bXBsZSBjb24gbGEgbG9uZ2l0dWQgbcOtbmltYSdcclxuICAgIH0gZWxzZXtcclxuICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=