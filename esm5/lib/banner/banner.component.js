/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
var BannerComponent = /** @class */ (function () {
    function BannerComponent() {
    }
    /**
     * @return {?}
     */
    BannerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    BannerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-banner',
                    template: "<div class=\"banner\">\n  <ng-content></ng-content>\n</div>\n",
                    styles: [".banner{width:auto;height:auto;border-radius:6px;background-color:#fff;border:1.5px solid #ffd200;padding:15px;overflow:hidden}"]
                },] },
    ];
    /** @nocollapse */
    BannerComponent.ctorParameters = function () { return []; };
    return BannerComponent;
}());
export { BannerComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFubmVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL2Jhbm5lci9iYW5uZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQVloRDtLQUFpQjs7OztJQUVqQixrQ0FBUTs7O0lBQVI7S0FDQzs7Z0JBYkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0QixRQUFRLEVBQUUsK0RBR1g7b0JBQ0MsTUFBTSxFQUFFLENBQUMsaUlBQWlJLENBQUM7aUJBQzVJOzs7OzBCQVREOztTQVVhLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtYmFubmVyJyxcclxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJiYW5uZXJcIj5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvZGl2PlxyXG5gLFxyXG4gIHN0eWxlczogW2AuYmFubmVye3dpZHRoOmF1dG87aGVpZ2h0OmF1dG87Ym9yZGVyLXJhZGl1czo2cHg7YmFja2dyb3VuZC1jb2xvcjojZmZmO2JvcmRlcjoxLjVweCBzb2xpZCAjZmZkMjAwO3BhZGRpbmc6MTVweDtvdmVyZmxvdzpoaWRkZW59YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEJhbm5lckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19