/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
var SubtitleComponent = /** @class */ (function () {
    function SubtitleComponent() {
    }
    /**
     * @return {?}
     */
    SubtitleComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SubtitleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-subtitle',
                    template: "<h2>\n  <ng-content></ng-content>\n</h2>\n",
                    styles: ["h2{font-size:17px;font-weight:700;line-height:1.35}"]
                },] },
    ];
    /** @nocollapse */
    SubtitleComponent.ctorParameters = function () { return []; };
    return SubtitleComponent;
}());
export { SubtitleComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VidGl0bGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvc3VidGl0bGUvc3VidGl0bGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQVloRDtLQUFpQjs7OztJQUVqQixvQ0FBUTs7O0lBQVI7S0FDQzs7Z0JBYkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4QixRQUFRLEVBQUUsNENBR1g7b0JBQ0MsTUFBTSxFQUFFLENBQUMscURBQXFELENBQUM7aUJBQ2hFOzs7OzRCQVREOztTQVVhLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1zdWJ0aXRsZScsXHJcbiAgdGVtcGxhdGU6IGA8aDI+XHJcbiAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG48L2gyPlxyXG5gLFxyXG4gIHN0eWxlczogW2BoMntmb250LXNpemU6MTdweDtmb250LXdlaWdodDo3MDA7bGluZS1oZWlnaHQ6MS4zNX1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU3VidGl0bGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==