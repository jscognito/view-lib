/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component } from '@angular/core';
var TitleComponent = /** @class */ (function () {
    function TitleComponent() {
    }
    /**
     * @return {?}
     */
    TitleComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    TitleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-title',
                    template: "<h1>\n  <ng-content></ng-content>\n</h1>\n",
                    styles: ["h1{font-size:24px;font-weight:700;line-height:1.17}"]
                },] },
    ];
    /** @nocollapse */
    TitleComponent.ctorParameters = function () { return []; };
    return TitleComponent;
}());
export { TitleComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGl0bGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvdGl0bGUvdGl0bGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFTLE1BQU0sZUFBZSxDQUFDOztJQVk5QztLQUFpQjs7OztJQUVqQixpQ0FBUTs7O0lBQVI7S0FDQzs7Z0JBYkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxXQUFXO29CQUNyQixRQUFRLEVBQUUsNENBR1g7b0JBQ0MsTUFBTSxFQUFFLENBQUMscURBQXFELENBQUM7aUJBQ2hFOzs7O3lCQVREOztTQVVhLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLXRpdGxlJyxcclxuICB0ZW1wbGF0ZTogYDxoMT5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvaDE+XHJcbmAsXHJcbiAgc3R5bGVzOiBbYGgxe2ZvbnQtc2l6ZToyNHB4O2ZvbnQtd2VpZ2h0OjcwMDtsaW5lLWhlaWdodDoxLjE3fWBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUaXRsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19