/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var OutlineButtonComponent = /** @class */ (function () {
    function OutlineButtonComponent() {
    }
    /**
     * @return {?}
     */
    OutlineButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    OutlineButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-outline-button',
                    template: "<button class=\"btn-outline\" id=\"{{ id }}\">\n  <i class=\"icon-left {{ icon }}\"></i>\n  <ng-content></ng-content>\n</button>\n",
                    styles: [".btn-outline{width:287px;height:59px;border-radius:29.5px;border:1px solid #3a3b3b;background-color:transparent;font-size:.85em;font-weight:700;padding:0 15px;margin-top:10px;cursor:pointer}.btn-outline:hover{border:2px solid #3a3b3b}.btn-outline .icon-left{font-size:1.8em;width:30px;vertical-align:bottom;line-height:10px}"]
                },] },
    ];
    /** @nocollapse */
    OutlineButtonComponent.ctorParameters = function () { return []; };
    OutlineButtonComponent.propDecorators = {
        id: [{ type: Input }],
        icon: [{ type: Input }]
    };
    return OutlineButtonComponent;
}());
export { OutlineButtonComponent };
function OutlineButtonComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    OutlineButtonComponent.prototype.id;
    /** @type {?} */
    OutlineButtonComponent.prototype.icon;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3V0bGluZS1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlldy1saWIvIiwic291cmNlcyI6WyJsaWIvb3V0bGluZS1idXR0b24vb3V0bGluZS1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQzs7SUFlckQ7S0FBaUI7Ozs7SUFFakIseUNBQVE7OztJQUFSO0tBQ0M7O2dCQWhCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsUUFBUSxFQUFFLG9JQUlYO29CQUNDLE1BQU0sRUFBRSxDQUFDLHNVQUFzVSxDQUFDO2lCQUNqVjs7Ozs7cUJBRUUsS0FBSzt1QkFDTCxLQUFLOztpQ0FiUjs7U0FXYSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1vdXRsaW5lLWJ1dHRvbicsXHJcbiAgdGVtcGxhdGU6IGA8YnV0dG9uIGNsYXNzPVwiYnRuLW91dGxpbmVcIiBpZD1cInt7IGlkIH19XCI+XHJcbiAgPGkgY2xhc3M9XCJpY29uLWxlZnQge3sgaWNvbiB9fVwiPjwvaT5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvYnV0dG9uPlxyXG5gLFxyXG4gIHN0eWxlczogW2AuYnRuLW91dGxpbmV7d2lkdGg6Mjg3cHg7aGVpZ2h0OjU5cHg7Ym9yZGVyLXJhZGl1czoyOS41cHg7Ym9yZGVyOjFweCBzb2xpZCAjM2EzYjNiO2JhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7Zm9udC1zaXplOi44NWVtO2ZvbnQtd2VpZ2h0OjcwMDtwYWRkaW5nOjAgMTVweDttYXJnaW4tdG9wOjEwcHg7Y3Vyc29yOnBvaW50ZXJ9LmJ0bi1vdXRsaW5lOmhvdmVye2JvcmRlcjoycHggc29saWQgIzNhM2IzYn0uYnRuLW91dGxpbmUgLmljb24tbGVmdHtmb250LXNpemU6MS44ZW07d2lkdGg6MzBweDt2ZXJ0aWNhbC1hbGlnbjpib3R0b207bGluZS1oZWlnaHQ6MTBweH1gXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgT3V0bGluZUJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19