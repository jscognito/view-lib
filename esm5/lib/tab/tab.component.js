/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from '@angular/core';
var TabComponent = /** @class */ (function () {
    function TabComponent() {
        this.active = false;
    }
    /**
     * @return {?}
     */
    TabComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    TabComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-tab',
                    template: "<div [hidden]=\"!active\" class=\"pane\">\n  <ng-content></ng-content>\n</div>",
                    styles: [".pane{padding:30px 20px;justify-content:center}"]
                },] },
    ];
    /** @nocollapse */
    TabComponent.ctorParameters = function () { return []; };
    TabComponent.propDecorators = {
        title: [{ type: Input }],
        id: [{ type: Input }],
        active: [{ type: Input }]
    };
    return TabComponent;
}());
export { TabComponent };
function TabComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    TabComponent.prototype.title;
    /** @type {?} */
    TabComponent.prototype.id;
    /** @type {?} */
    TabComponent.prototype.active;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpZXctbGliLyIsInNvdXJjZXMiOlsibGliL3RhYi90YWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQzs7SUFnQnZEO3NCQUZrQixLQUFLO0tBRU47Ozs7SUFFakIsK0JBQVE7OztJQUFSO0tBQ0M7O2dCQWpCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFFBQVEsRUFBRSxnRkFFTDtvQkFDTCxNQUFNLEVBQUUsQ0FBQyxpREFBaUQsQ0FBQztpQkFDNUQ7Ozs7O3dCQUlFLEtBQUs7cUJBQ0wsS0FBSzt5QkFDTCxLQUFLOzt1QkFkUjs7U0FTYSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC10YWInLFxyXG4gIHRlbXBsYXRlOiBgPGRpdiBbaGlkZGVuXT1cIiFhY3RpdmVcIiBjbGFzcz1cInBhbmVcIj5cclxuICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbjwvZGl2PmAsXHJcbiAgc3R5bGVzOiBbYC5wYW5le3BhZGRpbmc6MzBweCAyMHB4O2p1c3RpZnktY29udGVudDpjZW50ZXJ9YF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cclxuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgYWN0aXZlID0gZmFsc2U7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gIH1cclxuXHJcbn1cclxuIl19