import { OnInit } from '@angular/core';
export declare class NotificationComponent implements OnInit {
    type: string;
    icon: string;
    message: string;
    constructor();
    ngOnInit(): void;
}
