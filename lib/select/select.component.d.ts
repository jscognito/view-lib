import { OnInit } from '@angular/core';
export declare class SelectComponent implements OnInit {
    valueSelect: number;
    emptySelect: string;
    label: string;
    icon: string;
    type: string;
    state: string;
    message: string;
    constructor();
    ngOnInit(): void;
    validateField(): void;
}
