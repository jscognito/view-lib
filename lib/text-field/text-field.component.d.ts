import { OnInit, EventEmitter } from '@angular/core';
export declare class TextFieldComponent implements OnInit {
    valueInput: any;
    inputStyle: string;
    valueUsername: EventEmitter<{}>;
    label: string;
    iconPrimary: string;
    iconSecondary: string;
    state: string;
    message: string;
    pattern: string;
    id: string;
    getValueUsername(): void;
    constructor();
    ngOnInit(): void;
    validateField(): void;
    onSetStateChange(event: any): void;
}
