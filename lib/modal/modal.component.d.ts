export declare class ModalComponent {
    id: string;
    titleModal: string;
    messageContent: string;
    labelActionBtn: string;
    iconModalBtn: string;
    display: string;
    openModal(): void;
    closeModal(): void;
}
