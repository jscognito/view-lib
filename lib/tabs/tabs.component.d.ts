import { OnInit, AfterContentInit, QueryList } from '@angular/core';
import { TabComponent } from '../tab/tab.component';
export declare class TabsComponent implements OnInit, AfterContentInit {
    position: string;
    tabs: QueryList<TabComponent>;
    constructor();
    ngOnInit(): void;
    ngAfterContentInit(): void;
    selectTab(tab: TabComponent): void;
}
