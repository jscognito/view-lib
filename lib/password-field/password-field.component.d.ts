import { OnInit, EventEmitter } from '@angular/core';
export declare class PasswordFieldComponent implements OnInit {
    valueInputPassword: any;
    emptyInput: string;
    valuePassword: EventEmitter<{}>;
    label: string;
    state: string;
    message: string;
    type: string;
    pattern: string;
    icon: string;
    id: string;
    class: string;
    constructor();
    getValuePassword(): void;
    ngOnInit(): void;
    validateField(): void;
    clearField(): void;
    onSetStateChange(event: any): void;
}
